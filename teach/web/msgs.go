package web

const (
	msgErrInternalErrorRetry     = "an internal error happened, please retry request"
	msgErrRequireLogin           = "you need to be logged in to perform this action"
	msgErrAlreadyActiveChallenge = "you already have an active challenge, please stop it before starting a new"
	msgErrNoWorkers              = "we are currently at capacity, please try again later"
)

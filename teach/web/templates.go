package web

import (
	"bytes"
	"embed"
	"errors"
	"html/template"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/yuin/goldmark"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

const (
	templatesBasePath = "templates/"
	templatesExt      = ".tmpl"
)

var (
	//go:embed templates
	fsTemplates embed.FS
)

// templates that can be used in the templates
type tmplChallenges struct {
	ChallengeMap map[string][]models.Challenge // all the challenges available
	Completed    map[string]bool               // maps a challenge id to a boolean, indicating if the user has completed the challenge

	Tags []models.ChallengeTag

	FilterTags      map[string]bool
	FilterWithLabs  bool
	FilterWithFiles bool
}

type tmplChallengesView struct {
	UserChallenge  models.UserChallenge
	Challenge      models.Challenge
	ChallengeState models.ChallengeState
	DoRefresh      bool

	TotalFlags     int
	CompletedFlags int

	LoggedIn bool
	Active   bool

	TeachworkerIP string

	// challenge started, challenge end
	ChallengeStarted time.Time
	ChallengeEnd     time.Time
}

type tmplUserChallengesCompletedstruct struct {
	Challenges []models.Challenge
	// map[challenge id]time completed
	CompletedAt map[string]time.Time
}

func (w *Web) getEndpointFallback(name, fallback string, pairs ...string) string {
	url, err := w.getEndpoint(name, pairs...)
	if err != nil {
		log.Error().Err(err).Str("name", name).Msg("failed to get endpoint")
		return fallback
	}
	return url
}

func (w *Web) templateGet(name string) *template.Template {
	// 404
	if _, ok := w.Templates[name]; !ok {
		log.Error().Str("name", name).Msg("Trying to get a template that does not exists, returning a 404 page")
		return w.Templates["404.tmpl"]
	}

	// return tmpl
	return w.Templates[name]
}

func (w *Web) templateParse(name, path string) {
	// default path to name
	if path == "" {
		path = name
	}

	if _, ok := w.Templates[name]; ok {
		log.Panic().Msg("Trying to ready a template which has already been prepared")
		return
	}

	funcMap := template.FuncMap{
		"endpoint":   w.getEndpoint,
		"formatTime": templateFormatTime,
		"markdown":   parseMarkdown,
	}

	w.Templates[name] = template.Must(template.New(name).Funcs(funcMap).ParseFS(fsTemplates, templatesBasePath+path+templatesExt, templatesBasePath+"base"+templatesExt))
}

// functions that can be used inside the templates
func templateFormatTime(date time.Time) string {
	return date.Format("2006-01-02 15:04")
}

func (w *Web) getEndpoint(name string, pairs ...string) (string, error) {
	route := w.Router.Get(name)
	if route == nil {
		log.Error().Str("route", name).Msg("cannot find the provided route")
		return "", errors.New("template.endpoint: cannot find the specified route")
	}

	url, err := route.URL(pairs...)
	if err != nil {
		return "", err
	}

	return url.String(), err
}

func parseMarkdown(md string) (template.HTML, error) {
	var buf bytes.Buffer
	if err := goldmark.Convert([]byte(md), &buf); err != nil {
		return "", err
	}

	return template.HTML(buf.String()), nil
}

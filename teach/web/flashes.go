package web

import (
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/teach/web/mw"
)

type FlashLevel uint8

const (
	FlashLevelInfo FlashLevel = iota
	FlashLevelWarning
	FlashLevelDanger
	FlashLevelSuccess
)

func (fl FlashLevel) String() string {
	switch fl {
	case FlashLevelInfo:
		return "info"
	case FlashLevelWarning:
		return "warning"
	case FlashLevelDanger:
		return "danger"
	case FlashLevelSuccess:
		return "success"
	}

	log.Error().Msg("FlashLevel.string: could not get string, defaulting to info")
	return "info"
}

type FlashMessage struct {
	Level FlashLevel
	Msg   string
}

func (w *Web) AddFlash(rw http.ResponseWriter, r *http.Request, msg FlashMessage) {
	ses := mw.SessionFromContext(r.Context())

	ses.AddFlash(msg)

	if err := ses.Save(r, rw); err != nil {
		log.Error().Err(err).Msg("Failed to add flash while saving")
	}
}

func (w *Web) Flashes(rw http.ResponseWriter, r *http.Request) []FlashMessage {
	ses := mw.SessionFromContext(r.Context())

	flashes := ses.Flashes()

	var flashMsgs []FlashMessage
	for _, flashMsg := range flashes {
		v, ok := flashMsg.(FlashMessage)
		if !ok {
			continue
		}

		flashMsgs = append(flashMsgs, v)
	}

	if err := ses.Save(r, rw); err != nil {
		log.Error().Err(err).Msg("Failed to read flashes while saving")
	}

	return flashMsgs
}

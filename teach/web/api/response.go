package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
)

var (
	ErrInternalServerError = errors.New("internal server error")
	ErrNotFound            = errors.New("couldn't find requested item")
	ErrResourceExists      = errors.New("resource with that ID already exists")
)

type APIError struct {
	Msg string `json:"msg"`
	// TODO(eyJhb): potential future for error codes, instead of string compare
	Code int `json:"code"`
}

type APIRes struct {
	Collection interface{} `json:"collection"`
	Error      APIError    `json:"error"`
	Code       int         `json:"code"`
}
type ah func(http.ResponseWriter, *http.Request) (interface{}, int, error)

func (fn ah) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	col, code, err := fn(w, r)

	// log error if it is a internal error
	if code == 500 {
		log.Printf("Internal server error on API request: %+v", err)
	}

	// make our response ready
	res := APIRes{Error: APIError{}}

	// all error related stuff
	// TODO(eyJhb): should have multiple error codes...
	// goes in hand with above TODO
	res.Code = code
	res.Error.Code = code

	// if err != nil, lets return that error
	if err != nil {
		res.Error.Msg = err.Error()
	} else {
		// else we just give generic errors
		switch code {
		case 404:
			res.Error.Msg = ErrNotFound.Error()
		}
	}

	// if collection != nil
	if col != nil {
		res.Collection = col
	}

	// always specify we are using json
	w.Header().Add("Content-Type", "application/json")

	// write status-code after other
	// headers have been added
	w.WriteHeader(code)

	// write our response (body)
	json.NewEncoder(w).Encode(res)
}

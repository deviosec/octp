package api

import (
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/deviosec/octp/internal/registryauth"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

type Server struct {
	Worm               *worm.WORM
	RegistryTokenMaker *registryauth.Maker
}

// TODO(eyJhb) this is defined in teach web, and should ONLY be defined one place...
// copy paste is a ugly hack
func subrouter(origRouter *mux.Router, path string, fn func(r *mux.Router)) {
	fn(origRouter.NewRoute().PathPrefix(path).Subrouter())
}

// add our routes/middleware to our router
func (s *Server) Setup(r *mux.Router) {
	subrouter(r, "/users", func(r *mux.Router) {
		r.Handle("", ah(s.HandleUsersList)).Methods("GET")
	})
	subrouter(r, "/configs", func(r *mux.Router) {
		r.Handle("/{key}", ah(s.HandleConfigsGet)).Methods("GET")
	})
	subrouter(r, "/challenges", func(r *mux.Router) {
		r.Handle("", ah(s.HandleChallengesList)).Methods("GET")
		r.Handle("/{id}", ah(s.HandleChallengesPut)).Methods("PUT")
		subrouter(r, "/{id}", func(r *mux.Router) {
			r.Handle("", ah(s.HandleChallengesGet)).Methods("GET")
			r.Handle("", ah(s.HandleChallengesDelete)).Methods("DELETE")
		})
	})
	subrouter(r, "/workers", func(r *mux.Router) {
		r.Handle("", ah(s.HandleWorkerList)).Methods("GET")
	})

	subrouter(r, "/registry", func(r *mux.Router) {
		r.Handle("/token", ah(s.HandleRegistryToken)).Methods("POST")
	})

	// default not found handler
	r.NotFoundHandler = ah(func(rw http.ResponseWriter, r *http.Request) (interface{}, int, error) {
		return nil, 404, errors.New("endpoint not found")
	})
}

func (s *Server) HandleUsersList(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	var users []models.User
	if err := s.Worm.Select(&users).Run(r.Context()); err != nil {
		return nil, 500, err
	}

	return users, 200, nil
}

func (s *Server) HandleConfigsGet(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	key := mux.Vars(r)["key"]

	var confValue models.Config
	if err := s.Worm.Select(&confValue).Where(worm.Eq{"key": key}).Run(r.Context()); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, http.StatusNotFound, nil
		}

		return nil, http.StatusInternalServerError, err
	}

	return confValue, 200, nil
}

func (s *Server) HandleChallengesPut(rw http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	id := mux.Vars(r)["id"]

	var c models.Challenge
	if err := json.NewDecoder(r.Body).Decode(&c); err != nil {
		return nil, 400, err
	}

	if err := c.Validate(); err != nil {
		return nil, http.StatusBadRequest, err
	}

	tx, err := s.Worm.Begin(r.Context())
	if err != nil {
		return nil, 500, err
	}
	defer tx.Rollback()

	// check if challenge already exists
	exists := true
	if err := tx.Select(&models.Challenge{}).Where(worm.Eq{"id": id}).Run(r.Context()); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			exists = false
		} else {
			return nil, 500, err
		}
	}

	// update the id so it correspons with the correct one
	c.ID = id

	// update or insert challenge into database
	var stmt worm.StatementQuery
	if exists {
		stmt = tx.Update(&c)
	} else {
		stmt = tx.Insert(&c)
	}

	if err := stmt.Run(r.Context()); err != nil {
		return nil, 500, err
	}

	// delete current challenge tags, and insert them again
	// moving tags into a slice, so we can batch insert them
	var newTags []models.ChallengeTag
	for _, tag := range c.Tags {
		tag.ChallengeID = c.ID
		newTags = append(newTags, tag)
	}

	if err := tx.Delete(&models.ChallengeTag{}).Where(worm.Eq{"challenge_id": c.ID}).Run(r.Context()); err != nil {
		return nil, 500, err
	}

	if err := tx.Insert(&newTags).Run(r.Context()); err != nil {
		return nil, 500, err
	}

	// commit changes
	if err := tx.Commit(); err != nil {
		return nil, 500, err
	}

	return c, 204, nil
}

func (s *Server) HandleChallengesGet(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	id := mux.Vars(r)["id"]

	var c models.Challenge
	if err := s.Worm.Select(&c).Where(worm.Eq{"id": id}).Run(r.Context()); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, 404, nil
		}
		return nil, 500, err
	}

	return c, 200, nil
}

func (s *Server) HandleChallengesList(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	var challenges []models.Challenge
	if err := s.Worm.Select(&challenges).Run(r.Context()); err != nil {
		return nil, 500, err
	}

	return challenges, 200, nil
}

func (s *Server) HandleChallengesDelete(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	id := mux.Vars(r)["id"]

	if err := s.Worm.Delete(&models.Challenge{}).Where(worm.Eq{"id": id}).Run(r.Context()); err != nil {
		return nil, 500, err
	}

	return nil, 200, nil
}

func (s *Server) HandleWorkerList(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	var workers []models.TeachWorker

	if err := s.Worm.Select(&workers).Run(r.Context()); err != nil {
		return nil, 500, err
	}

	return workers, 200, nil
}

type ReqRegistryToken struct {
	ID      string
	Actions []string
	Images  []string
	Exp     time.Duration
}

func (s *Server) HandleRegistryToken(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	// if registry authentication is not enabled, we just return a static token
	if s.RegistryTokenMaker == nil {
		return "auth-not-enabled", http.StatusOK, nil
	}

	var req ReqRegistryToken

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, http.StatusBadRequest, err
	}

	// limit exp to 12 hours
	if req.Exp > 12*time.Hour {
		return nil, http.StatusBadRequest, errors.New("experation cannot be over 12 hours")
	}

	token, err := s.RegistryTokenMaker.GenerateToken(req.ID, req.Actions, req.Images, req.Exp)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return token, http.StatusCreated, nil
}

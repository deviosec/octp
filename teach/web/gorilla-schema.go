package web

import (
	"net/http"

	"github.com/gorilla/schema"
	"github.com/rs/zerolog/log"
)

// global schema decoder, which has a built in cache
// to store basic struct information
var schemaDecoder = schema.NewDecoder()

func populateStruct(r *http.Request, data interface{}) {
	if err := r.ParseForm(); err != nil {
		log.Error().Err(err).Msg("populateStruct: failed to ParseForm()")
		return
	}

	if err := schemaDecoder.Decode(data, r.PostForm); err != nil {
		log.Error().Err(err).Msg("populateStruct: failed to Decode using gorilla Schema")
		return
	}
}

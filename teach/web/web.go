package web

import (
	"encoding/hex"
	"fmt"
	"html/template"
	"net"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/authentication"
	"gitlab.com/deviosec/octp/internal/fileserver"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/manager"
	"gitlab.com/deviosec/octp/teach/web/api"
	"gitlab.com/deviosec/octp/teach/web/mw"
)

const (
	FileserverPath = "teach-fileserver"
)

var (
	rateLimitRealHeader = "X-Real-IP"
	rateLimitIPv6Mask   = net.CIDRMask(56, 128)
)

type Web struct {
	Worm        *worm.WORM
	AuthService authentication.AuthService
	Manager     *manager.Manager

	APIServer   *api.Server
	FileService *fileserver.Fileserver
	Router      *mux.Router

	// sessions managenment
	Sessions    sessions.Store
	SessionsDir string
	SessionKeys []string

	// fileservice
	FileserviceDir     string
	FileserviceMaxSize int64
	FileserviceMaxMem  int64

	// binaries dir
	BinariesPath string

	// rate limits
	RateLimitChallengeStart mw.RateLimit
	RateLimitChallengeSolve mw.RateLimit
	RateLimitSignIn         mw.RateLimit
	RateLimitSignUp         mw.RateLimit

	// bind information
	Bind string
	Port int

	Templates map[string]*template.Template
}

func (w *Web) New() {
	w.Router = mux.NewRouter()

	// if we have no sessions, create it
	if w.Sessions == nil {
		if len(w.SessionKeys) == 0 {
			log.Fatal().Msg("session_key cannot be empty,")
		}

		if err := os.MkdirAll(w.SessionsDir, 0750); err != nil {
			log.Fatal().Err(err).Msg("failed to create sessions dir")
		}

		var keys [][]byte
		for _, key := range w.SessionKeys {
			decodedKey, err := hex.DecodeString(key)
			if err != nil {
				log.Fatal().Err(err).Msg("failed to decode session key")
			}

			keys = append(keys, decodedKey)
		}

		w.Sessions = sessions.NewFilesystemStore(w.SessionsDir, keys...)
	}

	// if no fileservice, create it
	if w.FileService == nil {
		if err := os.MkdirAll(w.FileserviceDir, 0750); err != nil {
			log.Fatal().Err(err).Msg("web.New: failed to create dir for fileserver")
			return
		}

		if w.FileserviceMaxSize > 0 && w.FileserviceDir != "" {
			fs, err := fileserver.New(w.FileserviceDir, w.FileserviceMaxSize, w.FileserviceMaxMem)
			if err != nil {
				log.Fatal().Err(err).Msg("web.New: failed to create fileserver")
				return
			}

			w.FileService = fs
		} else {
			log.Info().Msg("Not enabling fileservice as 'maxfilesize' is <=0, or 'path' is empty")
		}
	}

	// if templates is a nil map, init it and add templates
	if w.Templates == nil {
		w.Templates = make(map[string]*template.Template)

		w.templateParse("", "index")
		w.templateParse("index", "")
		w.templateParse("about", "")
		w.templateParse("challenges", "")
		w.templateParse("challenge", "")
		w.templateParse("signup", "")
		w.templateParse("signin", "")

		w.templateParse("user-settings", "")
		w.templateParse("user-completed-challenges", "")
		w.templateParse("scoreboard", "")
	}

	// setup our routes
	w.Routes()
}

func (w *Web) Start() error {
	log.Info().
		Int("port", w.Port).
		Str("bind", w.Bind).
		Msg("listening teach server")

	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", w.Bind, w.Port), w.Router); err != nil {
		return err
	}

	return nil
}

func (w *Web) attachHandler(fn handlerFunc) handler {
	return handler{fn: fn, fnFlashes: w.Flashes, fnAddFlash: w.AddFlash}
}

// subrouter is a helper function for mimicking how go-chi does
// https://pkg.go.dev/github.com/go-chi/chi
func subrouter(origRouter *mux.Router, path string, fn func(r *mux.Router)) {
	fn(origRouter.NewRoute().PathPrefix(path).Subrouter())
}

func (w *Web) Routes() {
	// make require login middleware available, ie.
	// setup all the vars here, so it can easily be used
	// in routers.
	fnChallengesEndpoint := func() string { return w.getEndpointFallback("signin", "/signin") }
	fnSigninEndpoint := func() string { return w.getEndpointFallback("signin", "/signin") }
	fnAddFlash := func(rw http.ResponseWriter, r *http.Request, msg string) {
		w.AddFlash(rw, r, FlashMessage{FlashLevelInfo, msg})
	}

	// declare rate limit options
	rateLimitOpts := mw.RateLimitOptions{
		RealIPHader:    rateLimitRealHeader,
		IPv6Mask:       rateLimitIPv6Mask,
		FnAddFlash:     fnAddFlash,
		FnRedirectBack: redirectBack,
	}

	// initialise middlewares
	mwRequireLogin := mw.RequireLogin(fnSigninEndpoint, fnAddFlash)
	mwRequireChallenge := mw.RequireChallenge(fnChallengesEndpoint, fnAddFlash)

	subrouter(w.Router, "/", func(r *mux.Router) {
		// middlewares to use!
		r.Use(mw.APIAuthentication(&mw.APIAuthenticationOptions{
			AuthService: w.AuthService,
			Routes:      authRoutes(),
		}))
		// session before user, as session
		// is to check if we have a user
		r.Use(mw.AddSessionCtx(&mw.AddSessionStoreOptions{
			SessionStore: w.Sessions,
		}))
		r.Use(mw.AddUserCtx(&mw.AddUserCtxOptions{
			Worm: w.Worm,
		}))

		// setup rate limit for endpoints
		r.Use(mw.NewRateLimiterRouteMap(
			rateLimitOpts,
			map[string]mw.RateLimit{
				"signup":          w.RateLimitSignUp,
				"signin":          w.RateLimitSignIn,
				"challenge_start": w.RateLimitChallengeStart,
				"challenge_solve": w.RateLimitChallengeSolve,
			},
		))

		// setup binaries endpoint
		r.HandleFunc("/binaries/{file}", w.HandlerBinaries)

		r.Handle("/scoreboard", w.attachHandler(w.HandlerScoreboard)).Name("scoreboard").Methods("GET")

		// base endpoints
		r.Handle("/", w.attachHandler(w.HandlerIndex)).Name("index").Methods("GET")
		r.Handle("/about", w.attachHandler(w.HandlerAbout)).Name("about").Methods("GET")
		subrouter(r, "/user", func(r *mux.Router) {
			r.Handle("/signup", w.attachHandler(w.HandlerSignup)).Name("signup").Methods("GET", "POST")
			r.Handle("/signin", w.attachHandler(w.HandlerSignin)).Name("signin").Methods("GET", "POST")
			r.Handle("/signout", w.attachHandler(w.HandlerSignout)).Name("signout").Methods("POST")

			subrouter(r, "/", func(r *mux.Router) {
				r.Use(mwRequireLogin)
				r.Handle("/settings", w.attachHandler(w.HandlerUserSettings)).Name("user_settings").Methods("GET", "POST")
				r.Handle("/challenges_completed", w.attachHandler(w.HandlerUserCompletedChallenges)).Name("user_completed_challenges").Methods("GET", "POST")
			})
		})

		// add our api routes
		subrouter(r, "/api", func(r *mux.Router) {
			w.APIServer.Setup(r)
		})

		// all fileservice endpoints here
		if w.FileService != nil {
			subrouter(r, "/files", func(r *mux.Router) {
				r.Handle("", fileserver.HandleFunc(w.FileService.HandleUpload)).Methods("POST")
				r.Handle("", fileserver.HandleFunc(w.FileService.HandleGetAll)).Methods("GET")
				r.Handle("/download/{id}", fileserver.HandleFunc(w.FileService.HandleDownload)).Methods("GET")
				r.Handle("/view/{id}", fileserver.HandleFunc(w.FileService.HandleView)).Name("file").Methods("GET")
				r.Handle("/{id}", fileserver.HandleFunc(w.FileService.HandleGet)).Methods("GET")
				r.Handle("/{id}", fileserver.HandleFunc(w.FileService.HandleDelete)).Methods("DELETE")
			})
		}

		// challenges endpoint
		subrouter(r, "/challenges", func(r *mux.Router) {
			r.Handle("", w.attachHandler(w.HandlerChallengesList)).Name("challenges").Methods("GET")

			subrouter(r, "/", func(r *mux.Router) {
				r.Use(mwRequireLogin)
				r.Handle("/active", w.attachHandler(w.HandlerChallengesActive)).Name("challenge_active").Methods("GET")
			})

			subrouter(r, "/{id}", func(r *mux.Router) {
				r.Use(mw.AddChallengeCtx(&mw.AddChallengeCtxOptions{
					Worm: w.Worm,
				}))
				r.Use(mwRequireChallenge)

				r.Handle("/view", w.attachHandler(w.HandlerChallengesView)).Name("challenge").Methods("GET")

				subrouter(r, "/", func(r *mux.Router) {
					r.Use(mwRequireLogin)

					r.Handle("/start", w.attachHandler(w.HandlerChallengesStart)).Name("challenge_start").Methods("POST")
					r.Handle("/stop", w.attachHandler(w.HandlerChallengesStop)).Name("challenge_stop").Methods("POST")
					r.Handle("/solve", w.attachHandler(w.HandlerChallengesSolve)).Name("challenge_solve").Methods("POST")
				})
			})
		})

		// is the server alive ?
		r.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("pong"))
		})
	})
}

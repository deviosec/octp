{{define "title"}}{{.Challenge.Title}}{{end}}

{{define "extrahead" }}
    {{ if .DoRefresh }}
        <meta http-equiv="refresh" content="1" />
    {{ end }}
{{end}}

{{define "content"}}
{{$chal := .Challenge}}
{{$uchal := .UserChallenge}}
{{$completed := and (gt .CompletedFlags 0) (eq .CompletedFlags .TotalFlags)}}
<div class="bg-light pb-2">
    <div class="container">
        <div class="pt-5"></div>
        <h1>{{$chal.Title}}</h1>
        <div class="row">
            <div class="col">
                <p class="challenge-description">{{markdown $chal.Description}}</p>
                {{if .Active}}
                    <p class="text-muted">
                        You can connect to the challenge using the IP and port(s) listed to the right.
                        For more information see <a href="#">here</a>.
                    </p>
                {{end}}
            </div>
            <div class="col">
                <p class="my-0">Information</p>
                <hr class="mt-0 mb-1">
                <div class="row">
                    <div class="col-6">
                        <span>Category</span>
                    </div>
                    <div class="col">
                        <span>{{$chal.Category}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <span>Difficulty</span>
                    </div>
                    <div class="col">
                        <span>{{$chal.Difficulty}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <span>Completed</span>
                    </div>
                    <div class="col">
                        <p class="my-0{{if $completed}} text-success{{else}} text-danger{{end}}">{{if $completed}}Yes{{else}}No{{end}}</span>
                    </div>
                </div>

                {{ if $chal.Lab.Virtuals }}
                    <p class="mb-0 mt-3">Controls</p>
                    <hr class="mt-0 mb-1">
                    <div class="row">
                        <div class="col-6">
                            <span>Status</span>
                        </div>
                        <div class="col">
                            <span>
                                {{if .Active}} {{.ChallengeState.String}} {{else}}Not Started{{end}}
                            </span>
                        </div>
                    </div>
                    {{if .Active}}
                        <div class="row">
                            <div class="col-6">
                                <span>IP</span>
                            </div>
                            <div class="col">
                                <span>{{.TeachworkerIP}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span>Ports</span>
                            </div>
                            <div class="col">
                                <span>
                                    {{ range $virt := $chal.Lab.Virtuals }}
                                        {{ $virtports := (index $uchal.Ports $virt.Name) }}
                                        {{ range $portname, $port := $virt.Ports }}
                                            <b>{{index $virtports $portname }}</b>
                                        {{ end }}
                                    {{ end }}
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span>Started on</span>
                            </div>
                            <div class="col">
                                <span>{{formatTime .ChallengeStarted}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span>Will turn off at</span>
                            </div>
                            <div class="col">
                                <span>{{formatTime .ChallengeEnd}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center pt-2">
                                <form method="post" action="{{endpoint "challenge_stop" "id" $chal.ID}}">
                                    <button type="submit" class="btn btn-primary">Stop Challenge</button>
                                </form>
                            </div>
                        </div>
                    {{else}}
                        <div class="row">
                            <div class="col text-center pt-2">
                                <form method="post" action="{{endpoint "challenge_start" "id" $chal.ID}}">
                                    <button type="submit" class="btn btn-primary">Start Challenge</button>
                                </form>
                            </div>
                        </div>
                    {{end}}
                {{end}}
            </div>
        </div>

        {{if $chal.Files}}
            <h4>Files</h4>
            <hr class="mt-0 mb-1">
            {{range $file := $chal.Files}}
                <div class="row">
                    <div class="col-2">
                        <a href="{{$file.Link}}">{{$file.Filename}}</a>
                    </div>
                    <div class="col">
                        <span class="float-right">sha256-{{$file.Sha256}}</span>
                    </div>
                </div>
            {{end}}
        {{end}}

        <!-- <div class="pt-1"></div> -->
        <h4 class="pt-2">Flags</h4>
        <hr class="mt-0 mb-1">
        {{if $completed}}
            <p>You have taken all the flags, congratulations!</p>
        {{else}}
            <form method="POST" action="{{endpoint "challenge_solve" "id" $chal.ID}}">
                <div class="form-inline">
                    <input type="text" class="col-10 form-control" name="flag" placeholder="CTF{...}" />
                    <div class="col text-center">
                        <button type="submit" class="btn btn-primary my-1">Submit Flag</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-9 col-sm-12">
                        <span>You have found {{.CompletedFlags}} flag(s), there are more flags for you to find!</span>
                    </div>
                </div>
            </form>


        <div class="py-5"></div>
        <h2>Need help getting started?</h2>
        <p>
	    Maecenas pellentesque arcu id nunc lacinia sollicitudin. Ut sapien justo, commodo in lacus in, dignissim ornare orci. Donec dignissim mauris magna, et finibus nisi gravida non. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus mattis tortor a commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed imperdiet dui mollis, elementum risus eget, malesuada nulla.
        </p>
        {{end}}

    </div>
</div>
{{end}}

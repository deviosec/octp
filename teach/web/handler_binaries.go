package web

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"

	"github.com/gorilla/mux"
)

var (
	reBinaryName = regexp.MustCompile(`^[a-z0-9\-]+$`)

	ErrInvailedBinaryName = errors.New("the binary name is invaled")
)

func (w *Web) HandlerBinaries(rw http.ResponseWriter, r *http.Request) {
	// get the filename requested
	requestedFile := mux.Vars(r)["file"]

	// ensure that requested file matches binary name
	if matched := reBinaryName.Match([]byte(requestedFile)); !matched {
		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte(ErrInvailedBinaryName.Error()))
		return
	}

	fullPath := fmt.Sprintf("%s/%s", w.BinariesPath, requestedFile)
	http.ServeFile(rw, r, fullPath)
}

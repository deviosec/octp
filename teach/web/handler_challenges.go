package web

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"sort"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/manager"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/web/mw"
)

var (
	ErrNotSignedIn         = errors.New("you need to be signed in to perform this action")
	ErrNoChallengeProvided = errors.New("no challenge was provided")
	ErrNoFlagProvided      = errors.New("no flag was provided")
)

func (w *Web) HandlerChallengesList(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	tmpl := w.templateGet("challenges")

	// parse form
	if err := r.ParseForm(); err != nil {
		return nil, nil, http.StatusBadRequest, err
	}

	// setup filter
	tags := r.Form["tags"]

	filterStruct := challengesFilter{
		Tags:      tags,
		WithLabs:  r.Form.Has("with_labs"),
		WithFiles: r.Form.Has("with_files"),
	}

	// get all challenges
	challenges, err := w.filterChallenges(r.Context(), filterStruct)
	if err != nil {
		err := fmt.Errorf("HandlerChallengesList: failed to get challenges list error: %w", err)
		return nil, nil, 500, err
	}

	// get all tags
	var allTags []models.ChallengeTag
	if err := w.Worm.Select(&allTags).Distinct("tag").Run(r.Context()); err != nil {
		err := fmt.Errorf("HandlerChallengesList: failed to get all tags: %w", err)
		return tmpl, nil, 500, err
	}

	// make map[category][]challenge
	challengeMap := make(map[string][]models.Challenge)
	for _, chal := range challenges {
		challengeMap[chal.Category] = append(challengeMap[chal.Category], chal)
	}

	// sort all challenges by difficulty
	for _, challenges := range challengeMap {
		// sort by title then by difficulty
		sort.SliceStable(challenges, func(i, j int) bool { return challenges[i].Title < challenges[j].Title })
		sort.SliceStable(challenges, func(i, j int) bool { return challenges[i].Difficulty < challenges[j].Difficulty })
	}

	// setup completed challenges
	completedChallenges := make(map[string]bool)
	if user := mw.UserFromContext(r.Context()); user != nil {
		for completedChallengeID := range user.CompletedChallenges {
			completedChallenges[completedChallengeID] = true
		}
	}

	// convert filterTags to map
	filterTags := make(map[string]bool)
	for _, tag := range tags {
		filterTags[tag] = true
	}

	// input into our template
	tmplData := tmplChallenges{
		ChallengeMap: challengeMap,
		Completed:    completedChallenges,
		Tags:         allTags,

		// add filter options
		FilterTags:      filterTags,
		FilterWithLabs:  filterStruct.WithLabs,
		FilterWithFiles: filterStruct.WithFiles,
	}

	return tmpl, tmplData, 200, nil
}

func (w *Web) HandlerChallengesActive(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	// get user context
	user := mw.UserFromContext(r.Context())

	// get our active challenge
	_, activeChallenge, active, err := w.Manager.ActiveChallenge(r.Context(), user.ID.String())
	if err != nil {
		err = fmt.Errorf("HandlerChallengesActive: failed to get active challenge: %w", err)
		w.AddFlash(rw, r, FlashMessage{FlashLevelDanger, "Failed to get active challenge, please try again."})

		// redirect to previous URL
		redirectBack(rw, r)
		return nil, nil, 0, err
	}

	// if we have no active challenge, just redirect them back
	// from where they came to the challenges endpoint
	if !active {
		w.AddFlash(rw, r, FlashMessage{FlashLevelWarning, "You curreently have no active challenge."})

		redirectBack(rw, r)
		return nil, nil, 0, nil
	}

	http.Redirect(rw, r, w.getEndpointFallback("challenge", "/", "id", activeChallenge.ChallengeID), http.StatusSeeOther)
	return nil, nil, 0, nil
}

// HandlerChallengesView
// Scenarios
//	1. Not logged in user viewing (cannot submit flags)
//	2. Logged in user (can submit flags)
//	 2.1. Currently active (virtual)
//   2.2. Not active
func (w *Web) HandlerChallengesView(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	challengeID := mux.Vars(r)["id"]

	// init our tmplData
	tmplData := tmplChallengesView{}

	// get challenge
	var challenge models.Challenge
	if err := w.Worm.Select(&challenge).Where(worm.And{
		worm.Eq{"id": challengeID},
		worm.Eq{"active": true},
	}).Run(r.Context()); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return w.templateGet("challenges"), nil, http.StatusBadRequest, ErrNoChallengeProvided
		}

		err := fmt.Errorf("HandlerChallengesView: failed to get challenge with id '%s': %w", challengeID, err)
		return w.templateGet("challenges"), nil, http.StatusInternalServerError, err
	}

	// set the challenge - if the user is logged
	// in, then we will replace tmplData.UserChallenge
	// to the currently active one
	tmplData.Challenge = challenge

	// see if we can get a user
	user := mw.UserFromContext(r.Context())

	// user is not logged in, show the challenge
	if user == nil {
		return w.templateGet("challenge"), tmplData, http.StatusOK, nil
	}

	// otherwise user is logged in, populate further information

	// at this point, we do have a valid user, else they would have
	// gotten a error
	tmplData.LoggedIn = true

	// check if this challenge is a challenge they are actively running
	teachWorker, activeChallenge, active, err := w.Manager.ActiveChallenge(r.Context(), user.ID.String())
	if err != nil {
		err := fmt.Errorf("HandlerChallengesView: Failed to get active challenge, error: %w", err)
		return nil, nil, http.StatusInternalServerError, err
	}

	// set userchallenge
	tmplData.UserChallenge = activeChallenge

	if active && activeChallenge.ChallengeID == challenge.ID {
		tmplData.Active = true
		tmplData.TeachworkerIP = teachWorker.IP

		// setup the time information, based on the users timezone
		loc, err := time.LoadLocation(user.TimeZone)
		if err != nil {
			log.Error().
				Err(err).
				Str("id", user.ID.String()).
				Str("timezone", user.TimeZone).
				Msg("failed to load user timezone, using default time")
			loc = time.Local
		}
		tmplData.ChallengeStarted = activeChallenge.CreatedAt.In(loc)
		tmplData.ChallengeEnd = activeChallenge.CreatedAt.Add(30 * time.Minute).In(loc)

		// get the current state of the challenge
		// and refresh the page until the challenge state is not running
		tmplData.ChallengeState = w.Manager.ChallengeState(activeChallenge.ID.String())
		tmplData.DoRefresh = tmplData.ChallengeState != models.ChallengeStateWorkerRunning

		// update all the challenge description things
		description := tmplData.Challenge.Description

		// update the challenge description
		for _, virt := range tmplData.Challenge.Lab.Virtuals {
			for portName, port := range virt.Ports {
				description = strings.ReplaceAll(description, fmt.Sprintf("##PORT_%s##", portName), fmt.Sprintf("%d", port.Host))
			}
		}

		// replace the host IP
		description = strings.ReplaceAll(description, "##SERVERIP##", teachWorker.IP)
		tmplData.Challenge.Description = description
	}

	// find total flags + completed
	var totalFlags, completedFlags int
	for _, flag := range challenge.Flags {
		totalFlags += 1
		for _, fa := range user.FlagAttempts {
			if !fa.Correct || fa.ChallengeID != challenge.ID {
				continue
			}

			if flag.Name == fa.Name {
				completedFlags += 1
				break
			}
		}
	}

	tmplData.TotalFlags = totalFlags
	tmplData.CompletedFlags = completedFlags

	return w.templateGet("challenge"), tmplData, http.StatusOK, nil
}

func (w *Web) HandlerChallengesStart(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	defer func() { redirectBack(rw, r) }()

	user := mw.UserFromContext(r.Context())
	chall := mw.ChallengeFromContext(r.Context())

	log.Info().Str("user_id", user.ID.String()).Interface("challenge_id", chall.ID).Msg("challenge_start")

	if _, _, err := w.Manager.StartChallenge(r.Context(), user.ID.String(), chall.ID); err != nil {
		if err == manager.ErrAlreadyActiveChallenge {
			return nil, nil, 200, errors.New(msgErrAlreadyActiveChallenge)
		} else if err == manager.ErrNoWorkersAvailable || err == manager.ErrNoViableWorkersAvailable {
			return nil, nil, 200, errors.New(msgErrNoWorkers)
		}

		return nil, nil, 200, errors.New(msgErrInternalErrorRetry)
	}

	return nil, nil, 0, nil
}

func (w *Web) HandlerChallengesStop(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	defer func() { redirectBack(rw, r) }()

	user := mw.UserFromContext(r.Context())
	chall := mw.ChallengeFromContext(r.Context())
	log.Info().Str("user_id", user.ID.String()).Interface("challenge_id", chall.ID).Msg("challenge_stop")

	if err := w.Manager.StopChallenge(r.Context(), user.ID.String()); err != nil {
		err = fmt.Errorf("HandlerChallengesStop: Could not stop the challenge for user '%s', got error: %v", user.ID.String(), err)
		return nil, nil, 500, err
	}

	return nil, nil, 0, nil
}

func (w *Web) HandlerChallengesSolve(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	defer func() { redirectBack(rw, r) }()

	if err := r.ParseForm(); err != nil {
		err = fmt.Errorf("HandlerChallengesSolve: Failed to parse form: %w", err)
		return nil, nil, 500, err
	}

	// get user
	user := mw.UserFromContext(r.Context())
	chall := mw.ChallengeFromContext(r.Context())

	// get flag value
	flag := r.Form.Get("flag")
	if flag == "" {
		return nil, nil, http.StatusBadRequest, ErrNoFlagProvided
	}

	// if flag is above 1024 chars, give out a error
	if len(flag) > 1024 {
		w.AddFlash(rw, r, FlashMessage{FlashLevelWarning, "The provided flag was too long, and is thereby invalid"})
		return nil, nil, 0, nil
	}

	// loop over all flags in active challenge, and see if it matches
	var correctFlagKey *string
	for _, chalFlag := range chall.Flags {
		if chalFlag.Flag == flag {
			correctFlagKey = &chalFlag.Name
			break
		}
	}

	// init flag attempt struct
	flagAttempt := models.FlagAttempt{
		UserID:      user.ID,
		ChallengeID: chall.ID,
		Value:       flag,
	}

	// not a correct flag
	if correctFlagKey == nil {
		w.AddFlash(rw, r, FlashMessage{FlashLevelDanger, "Incorrect flag!"})
	} else {
		// no reason to save a known flag
		flagAttempt.Value = ""
		flagAttempt.Correct = true
		flagAttempt.Name = *correctFlagKey

		w.AddFlash(rw, r, FlashMessage{FlashLevelSuccess, "Correct flag!"})
	}

	log.Info().Str("user_id", user.ID.String()).Interface("flag", flagAttempt).Msg("flag attempt")

	// begin transaction for WORM
	tx, err := w.Worm.Begin(r.Context())
	if err != nil {
		return nil, nil, http.StatusInternalServerError, err
	}
	defer tx.Rollback()

	// add it to our flag attempts
	if err := tx.Insert(&flagAttempt).Run(r.Context()); err != nil {
		err := fmt.Errorf("HandlerChallengesSolve: could not update user '%s': %w", user.ID.String(), err)
		return nil, nil, http.StatusInternalServerError, err
	}

	// check if challenge is completed
	challengeCompleted, err := checkChallengeCompleted(r.Context(), tx, *user, *chall)
	if err != nil {
		return nil, nil, http.StatusInternalServerError, err
	}

	if challengeCompleted {
		if err := markChallengeCompleted(r.Context(), tx, *user, *chall); err != nil {
			return nil, nil, http.StatusInternalServerError, err
		}
	}

	tx.Commit()

	return nil, nil, 0, nil
}

func checkChallengeCompleted(ctx context.Context, tx *worm.WORM, user models.User, challenge models.Challenge) (bool, error) {
	// get all users flag attempts
	var flagAttempts []models.FlagAttempt
	err := tx.Select(&flagAttempts).Where(
		worm.And{
			worm.And{
				worm.Eq{"correct": true},
				worm.Eq{"challenge_id": challenge.ID},
			},
			worm.Eq{"user_id": user.ID},
		},
	).Distinct("name").Run(ctx)
	if err != nil {
		return false, err
	}

	// set all the correct flags that the user has submitted
	// in a map, so that it can later be checked against the challenge
	correctUserFlags := make(map[string]struct{})
	for _, flagAttempt := range flagAttempts {
		correctUserFlags[flagAttempt.Name] = struct{}{}
	}

	var missingFlag bool
	for _, challengeFlag := range challenge.Flags {
		if _, ok := correctUserFlags[challengeFlag.Name]; !ok {
			missingFlag = true
		}
	}

	return !missingFlag, nil
}

func markChallengeCompleted(ctx context.Context, tx *worm.WORM, user models.User, challenge models.Challenge) error {
	comChall := models.CompletedChallenge{
		UserID:      user.ID,
		ChallengeID: challenge.ID,
	}

	return tx.Insert(&comChall).Run(ctx)
}

package web

import (
	"net/http"
	"strings"

	"gitlab.com/deviosec/octp/internal/authentication"
	"gitlab.com/deviosec/octp/teach/pkgs/permissions"
)

func authRoutes() map[string]func(r, m string, ti authentication.TokenInfo) bool {
	routes := make(map[string]func(string, string, authentication.TokenInfo) bool)

	routes["/api/*"] = func(r, m string, ti authentication.TokenInfo) bool {
		return ti.Perm >= permissions.Admin
	}

	// fileservice authentication
	routes["/files/*"] = func(r, m string, ti authentication.TokenInfo) bool {
		if ti.Perm >= permissions.Admin {
			return true
		}

		if m == http.MethodGet && (strings.HasPrefix(r, "/files/view/") || strings.HasPrefix(r, "/files/download/")) {
			return true
		}

		return false
	}

	// default route last, so we are not explicit.
	// could be omitted, as if no routes are found it
	// is forbidden
	// routes["/*"] = func(r, m string, ti authentication.TokenInfo) bool {
	// 	return false
	// }

	return routes
}

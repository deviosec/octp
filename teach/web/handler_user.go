package web

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"sort"
	"time"

	sq "github.com/Masterminds/squirrel"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/simplecrypto"
	"gitlab.com/deviosec/octp/internal/tz"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/web/mw"
)

func (w *Web) HandlerSignout(rw http.ResponseWriter, r *http.Request) (tmpl *template.Template, data interface{}, statusCode int, err error) {
	defer func() { redirectBack(rw, r) }()

	ses := mw.SessionFromContext(r.Context())

	delete(ses.Values, mw.SessionKeyUserID{})

	if err := ses.Save(r, rw); err != nil {
		return nil, nil, 500, err
	}

	return
}

// user signup
type userSignupForm struct {
	Username  string
	Email     string
	TimeZone  string
	Password1 string
	Password2 string

	TimeZones []string
}

func (f *userSignupForm) Validate() error {
	errs, _ := validation.ValidateStruct(f,
		validation.Field(&f.Username, validation.Required, is.PrintableASCII),
		validation.Field(&f.Email, is.EmailFormat),
		validation.Field(&f.Password1, validation.Required),
		validation.Field(&f.Password2, validation.Required),
	).(validation.Errors)

	if errs == nil {
		errs = validation.Errors{}
	}

	if f.Password1 != f.Password2 {
		errs["password"] = validation.NewError("validation_not_same", "passwords must match")
	}

	if !tz.Exists(f.TimeZone) {
		errs["timezone"] = validation.NewError("invalid_timezone", "selected timezone is invalid")
	}

	if len(errs) != 0 {
		return errs
	}

	return nil
}

func (w *Web) HandlerSignup(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	// default template
	tmpl := w.templateGet("signup")

	if user := mw.UserFromContext(r.Context()); user != nil {
		return tmpl, nil, 200, errors.New("unable to create new user when signed in")
	}

	// declare our formData, so that
	// it can be used for both GET
	// (fill in fields on failure)
	// and POST (create user)
	var formData userSignupForm
	if r.Method == http.MethodPost {
		populateStruct(r, &formData)
		data := formData
		data.TimeZones = tz.TimeZones

		if err := formData.Validate(); err != nil {
			return tmpl, data, 200, err
		}
	}

	if r.Method == http.MethodGet {
		// init data to give to user signup
		data := userSignupForm{
			TimeZones: tz.TimeZones,
		}

		return tmpl, data, 200, nil
	}

	// check if user already exists
	if _, err := w.validateUser(r.Context(), formData.Username, nil); err != ErrNoUserFound {
		if err == ErrDatabaseErr {
			return tmpl, nil, 500, err
		}

		w.AddFlash(rw, r, FlashMessage{FlashLevelDanger, "A user with that username already exists"})
		return tmpl, nil, 200, nil
	}

	hashedPassword, err := simplecrypto.PasswordHash([]byte(formData.Password1))
	if err != nil {
		return tmpl, nil, 500, err
	}

	user := models.User{
		Username:       formData.Username,
		Email:          formData.Email,
		HashedPassword: hashedPassword,

		TimeZone: formData.TimeZone,
	}

	if err := w.Worm.Insert(&user).Run(r.Context()); err != nil {
		log.Error().Err(err).Msg("failed to create user")
		w.AddFlash(rw, r, FlashMessage{FlashLevelDanger, "Failed to create user, please try again"})
		return tmpl, nil, 200, nil
	}

	w.AddFlash(rw, r, FlashMessage{FlashLevelSuccess, "successfully created user"})

	ses := mw.SessionFromContext(r.Context())
	ses.Values[mw.SessionKeyUserID{}] = user.ID.String()
	ses.Save(r, rw)

	// set the statusCode, so that the return
	// handler knows it is a redirect
	http.Redirect(rw, r, w.getEndpointFallback("index", "/"), http.StatusSeeOther)
	return tmpl, nil, http.StatusSeeOther, nil
}

// user signin
type userSigninForm struct {
	Username string
	Password string
}

func (f *userSigninForm) Validate() error {
	return nil
}

func (w *Web) HandlerSignin(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	// defaults (see HandlerSignup)
	tmpl := w.templateGet("signin")

	var formData userSigninForm
	if r.Method == http.MethodPost {
		populateStruct(r, &formData)
		data := formData

		if err := formData.Validate(); err != nil {
			return tmpl, data, 200, err
		}
	}

	if r.Method == http.MethodGet {
		return tmpl, nil, 200, nil
	}

	user, err := w.validateUser(r.Context(), formData.Username, &formData.Password)
	if err != nil {
		if err == ErrNoUserFound {
			w.AddFlash(rw, r, FlashMessage{FlashLevelWarning, "Incorrect username or password"})
			return tmpl, nil, 200, nil
		}

		return tmpl, nil, 500, err
	}

	if user == nil {
		w.AddFlash(rw, r, FlashMessage{FlashLevelWarning, "Incorrect username or password"})
		return tmpl, nil, 200, nil
	}

	// set userid for session
	ses := mw.SessionFromContext(r.Context())
	ses.Values[mw.SessionKeyUserID{}] = user.ID.String()
	ses.Save(r, rw)

	// redirect to front page
	http.Redirect(rw, r, w.getEndpointFallback("index", "/"), http.StatusSeeOther)
	return tmpl, nil, http.StatusSeeOther, nil
}

// user settings
type userSettingsForm struct {
	Email                 string
	TimeZone              string
	NewPassword1          string
	NewPassword2          string
	PasswordConfirm       string
	ParticipateScoreboard bool

	// data for template
	TimeZones []string
}

func (f *userSettingsForm) Validate() error {
	errs, _ := validation.ValidateStruct(f,
		validation.Field(&f.Email, is.EmailFormat),
		validation.Field(&f.PasswordConfirm, validation.Required),
	).(validation.Errors)

	if errs == nil {
		errs = validation.Errors{}
	}

	if f.NewPassword1 != f.NewPassword2 {
		errs["new password"] = validation.NewError("validation_not_same", "passwords must match")
	}

	if !tz.Exists(f.TimeZone) {
		errs["timezone"] = validation.NewError("invalid_timezone", "selected timezone is invalid")
	}

	if len(errs) != 0 {
		return errs
	}

	return nil
}

func (w *Web) HandlerUserSettings(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	// default template
	tmpl := w.templateGet("user-settings")

	// extract current user
	user := mw.UserFromContext(r.Context())
	if user == nil {
		w.AddFlash(rw, r, FlashMessage{FlashLevelWarning, "Not logged in"})
		return tmpl, nil, 200, nil
	}

	// declare our formData, so that
	// it can be used for both GET
	// (fill in fields on failure)
	// and POST (create user)
	data := userSettingsForm{
		TimeZone:  user.TimeZone,
		TimeZones: tz.TimeZones,
	}

	if r.Method == http.MethodPost {
		populateStruct(r, &data)

		if err := data.Validate(); err != nil {
			return tmpl, data, 200, err
		}
	}

	if r.Method == http.MethodGet {
		// TODO(eyJhb): find a better way to do this, instead of manually
		// having to add eatch field here
		// set initial information about our user
		data.Email = user.Email
		data.ParticipateScoreboard = user.ParticipateScoreboard

		return tmpl, data, 200, nil
	}

	// ensure that password confirms already saved password
	ok, err := simplecrypto.ValidatePasswordHash(data.PasswordConfirm, user.HashedPassword)
	if err != nil {
		return tmpl, data, 500, err
	}

	if !ok {
		w.AddFlash(rw, r, FlashMessage{FlashLevelWarning, "Wrong confirmation password"})
		return tmpl, data, 200, nil
	}

	// TODO(eyJhb): changing password should logout the user
	if data.NewPassword1 != "" {
		user.HashedPassword, err = simplecrypto.PasswordHash([]byte(data.NewPassword1))
		if err != nil {
			return tmpl, data, 500, err
		}
	}

	// set fields here
	user.TimeZone = data.TimeZone
	user.Email = data.Email
	user.ParticipateScoreboard = data.ParticipateScoreboard
	user.TimeZone = data.TimeZone

	if err := w.Worm.Update(user).Run(r.Context()); err != nil {
		w.AddFlash(rw, r, FlashMessage{FlashLevelDanger, "Failed to update settings, please try again"})
		log.Error().Err(err).Msg("failed to update user settings")
		return tmpl, data, 200, nil
	}

	w.AddFlash(rw, r, FlashMessage{FlashLevelSuccess, "successfully updated settings"})

	return tmpl, data, 200, nil
}

func (w *Web) HandlerUserCompletedChallenges(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	// default template
	tmpl := w.templateGet("user-completed-challenges")

	// extract our user
	user := mw.UserFromContext(r.Context())

	// model query to use to select the appropiate challenges
	modelChallengeCompletedQuery := sq.Select("challenge_id").From("completed_challenges").Where(sq.Eq{"user_id": user.ID}).Distinct()
	sql, params, err := modelChallengeCompletedQuery.ToSql()
	if err != nil {
		return nil, nil, http.StatusInternalServerError, err
	}
	whereStmt := worm.And{worm.Eq{"active": true}, sq.Expr(fmt.Sprintf("id in (%s)", sql), params...)}

	var challenges []models.Challenge
	if err := w.Worm.Select(&challenges).Where(whereStmt).Run(r.Context()); err != nil {
		return tmpl, nil, http.StatusInternalServerError, err
	}

	// sort the challenges based on completed at
	sort.SliceStable(challenges, func(i, j int) bool {
		return user.CompletedChallenges[challenges[i].ID].CreatedAt.After(user.CompletedChallenges[challenges[j].ID].CreatedAt)
	})

	// make completedAt
	completedAt := make(map[string]time.Time)
	for challengeID, completedChallenge := range user.CompletedChallenges {
		completedAt[challengeID] = completedChallenge.CreatedAt
	}

	data := tmplUserChallengesCompletedstruct{
		Challenges:  challenges,
		CompletedAt: completedAt,
	}

	return tmpl, data, 200, nil
}

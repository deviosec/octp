package mw

import (
	"context"
	"database/sql"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

type ContextKeyChallengeInfo struct{}

type AddChallengeCtxOptions struct {
	Worm *worm.WORM
}

func AddChallengeCtx(options *AddChallengeCtxOptions) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(rw http.ResponseWriter, r *http.Request) {
			// get the challenge id, and ensure there is one
			// otherwise the request is ignored
			vars := mux.Vars(r)
			challengeID, ok := vars["id"]
			if !ok {
				next.ServeHTTP(rw, r)
				return
			}

			var challenge models.Challenge
			if err := options.Worm.Select(&challenge).Where(worm.Eq{"id": challengeID}).Run(r.Context()); err != nil {
				if !errors.Is(err, sql.ErrNoRows) {
					log.Error().Err(err).Msg("AddChallengeCtx: failed to get challenge, calling next anyways")
				}
				next.ServeHTTP(rw, r)
				return
			}

			// store challenge in context
			r = r.WithContext(context.WithValue(r.Context(), ContextKeyChallengeInfo{}, &challenge))
			next.ServeHTTP(rw, r)
		}

		return http.HandlerFunc(fn)
	}
}

func ChallengeFromContext(ctx context.Context) *models.Challenge {
	// look in UserFromContext for explanation
	challenge, _ := ctx.Value(ContextKeyChallengeInfo{}).(*models.Challenge)
	return challenge
}

package mw

import (
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/authentication"
)

type APIAuthenticationOptions struct {
	AuthService authentication.AuthService
	Routes      map[string]func(r, m string, ti authentication.TokenInfo) bool
}

// simple auth middleware
func APIAuthentication(options *APIAuthenticationOptions) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(rw http.ResponseWriter, r *http.Request) {
			path := r.URL.Path
			method := r.Method

			for route, fn := range options.Routes {
				if !authMatchURL(route, path) {
					continue
				}

				var ti authentication.TokenInfo
				apiKeyHeader := r.Header.Get("Authorization")
				if strings.HasPrefix(apiKeyHeader, "Bearer ") {
					token := apiKeyHeader[len("Bearer "):]

					// validate token
					tmpTi, err := options.AuthService.Validate(r.Context(), token)
					if err != nil && err != authentication.ErrInvalidToken {
						log.Error().Err(err).Msg("api.auth: failed to validatee token")
						http.Error(rw, "internal server error", http.StatusInternalServerError)
						return
					}

					if err == authentication.ErrInvalidToken {
						log.Error().Err(err).Msg("api.auth: the provided token does not exists on the server")
						http.Error(rw, authentication.ErrInvalidToken.Error(), http.StatusUnauthorized)
						return
					}

					ti = tmpTi
				}

				if fn(path, method, ti) {
					next.ServeHTTP(rw, r)
					return
				}

				// assume that it did not pass, forbidden
				http.Error(rw, "Forbidden", http.StatusForbidden)
				return
			}

			// there was no route match for this one, just continue on to the next
			next.ServeHTTP(rw, r)
		}

		return http.HandlerFunc(fn)
	}
}

func authMatchURL(authRoute, actualRoute string) bool {
	if authRoute[len(authRoute)-1] == '*' {
		if strings.HasPrefix(actualRoute, authRoute[:len(authRoute)-2]) {
			return true
		}
		// not wildcard, exact match
	} else if actualRoute == authRoute {
		return true
	}

	return false
}

package mw

import "net/http"

func RequireChallenge(fnChallengesEndpoint func() string, fnAddFlash func(http.ResponseWriter, *http.Request, string)) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			// if there is a valid challenge for request, then just serve next request
			if chall := ChallengeFromContext(r.Context()); chall != nil {
				next.ServeHTTP(rw, r)
				return
			}

			// no valid challenge, bring the user back to
			fnAddFlash(rw, r, "The challenge does not exists, please go to challenge and try again")
			http.Redirect(rw, r, fnChallengesEndpoint(), http.StatusSeeOther)
		})
	}
}

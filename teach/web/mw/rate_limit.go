package mw

import (
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/ulule/limiter/v3"
	"github.com/ulule/limiter/v3/drivers/middleware/stdlib"
	"github.com/ulule/limiter/v3/drivers/store/memory"
)

// basic reusable options
type RateLimitOptions struct {
	RealIPHader string
	IPv6Mask    net.IPMask

	// use by handler functions
	FnAddFlash     func(http.ResponseWriter, *http.Request, string)
	FnRedirectBack func(http.ResponseWriter, *http.Request)
}

type RateLimit struct {
	Duration time.Duration
	Limit    int64
	Methods  []string
}

type rateLimitHandlerOptions struct {
	fnAddFlash     func(http.ResponseWriter, *http.Request, string)
	fnRedirectBack func(http.ResponseWriter, *http.Request)
}

func NewRateLimiterRouteMap(opts RateLimitOptions, rateOpts map[string]RateLimit) mux.MiddlewareFunc {
	// init all rate limitters
	limits := make(map[string]mux.MiddlewareFunc)
	routeMethods := make(map[string]map[string]bool)
	for routeName, rateOpt := range rateOpts {
		limits[routeName] = NewRateLimiter(opts, rateOpt)

		// setup methods
		routeMethods[routeName] = make(map[string]bool)
		for _, method := range rateOpt.Methods {
			routeMethods[routeName][method] = true
		}
	}

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			// get current route
			router := mux.CurrentRoute(r)
			if router == nil {
				next.ServeHTTP(rw, r)
				return
			}

			// extract route name
			routeName := router.GetName()

			// check which methods this should react on
			// if route name does not exists in map,
			// then it is not a route that is rate limitted
			methods, ok := routeMethods[routeName]
			if !ok {
				next.ServeHTTP(rw, r)
				return
			}

			// if there is at least one method that should
			// be rate limitted, and the method matches
			// then do rate limitting
			if len(methods) > 0 && !methods[r.Method] {
				next.ServeHTTP(rw, r)
				return
			}

			rateFunc, ok := limits[routeName]
			if !ok {
				next.ServeHTTP(rw, r)
				return
			}

			rateFunc(next).ServeHTTP(rw, r)
		})
	}
}

func NewRateLimiter(opts RateLimitOptions, rateOpts RateLimit) mux.MiddlewareFunc {
	store := memory.NewStore()

	ratelmt := limiter.New(
		store,
		limiter.Rate{Period: rateOpts.Duration, Limit: rateOpts.Limit},
		limiter.WithClientIPHeader(opts.RealIPHader),
		limiter.WithIPv6Mask(opts.IPv6Mask),
	)

	handlerOptions := rateLimitHandlerOptions{
		fnAddFlash:     opts.FnAddFlash,
		fnRedirectBack: opts.FnRedirectBack,
	}

	return stdlib.NewMiddleware(
		ratelmt,
		stdlib.WithKeyGetter(rateLimitKeyGetter(ratelmt)),
		stdlib.WithLimitReachedHandler(rateLimitReachedHandler(handlerOptions, ratelmt)),
		stdlib.WithErrorHandler(rateLimitErrorHandler(handlerOptions)),
	).Handler
}

// rateLimitErrorHandler is called when any internal error happens in the rate limitter.
// a generic error will be returned to the user, and the actual error will get logged
func rateLimitErrorHandler(opts rateLimitHandlerOptions) stdlib.ErrorHandler {
	return func(rw http.ResponseWriter, r *http.Request, err error) {
		log.Error().Err(err).Msg("rate limitter had error enforcing ratelimit")
		opts.fnAddFlash(rw, r, "Unknown error trying to enforce rate limit, please try again later")
		opts.fnRedirectBack(rw, r)
	}
}

// rateLimitReachedHandler is called whenever a user gets rate limited,
// where the user will get an error message saying they have been rate limited
// and they should retry in X secounds. if the time they have been rate limited
// can not be calculated, just give a generic rate limit message
func rateLimitReachedHandler(opts rateLimitHandlerOptions, limitter *limiter.Limiter) stdlib.LimitReachedHandler {
	return func(rw http.ResponseWriter, r *http.Request) {
		rctx, err := limitter.Get(r.Context(), rateLimitGetKey(limitter, r))

		// if any error occured, log it and give a basic try again later,
		// otherwise calculate the actual time before the rate limit expires
		if err != nil {
			opts.fnAddFlash(rw, r, "Rate limit reached, please try again later")
		} else {
			resetTime := time.Unix(rctx.Reset, 0)
			remainingTime := time.Until(resetTime).Seconds()
			opts.fnAddFlash(rw, r, fmt.Sprintf("Rate limit reached, please try again in %d secounds", int(remainingTime)))
		}

		opts.fnRedirectBack(rw, r)
	}
}

func rateLimitKeyGetter(limiter *limiter.Limiter) func(r *http.Request) string {
	return func(r *http.Request) string {
		return rateLimitGetKey(limiter, r)
	}
}

// rateLimitGetKey will use the user id if signed in
// otherwise if not signed in, the IP will be returned
func rateLimitGetKey(limiter *limiter.Limiter, r *http.Request) string {
	user := UserFromContext(r.Context())
	if user != nil {
		return user.ID.String()
	}

	return limiter.GetIPKey(r)
}

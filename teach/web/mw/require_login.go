package mw

import "net/http"

// RequireLogin requires a user to be attached to each context, if not it will redirect to `signin` and flash a message.
// If the middleware should not add a flash, then `fnAddFlash` should just be an empty function.
// The reason for using a function for getting the signin endpoint, is that the middleware might be attached before the endpoint
// has actually been created, which will result in a error.
func RequireLogin(fnSigninEndpoint func() string, fnAddFlash func(http.ResponseWriter, *http.Request, string)) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			if user := UserFromContext(r.Context()); user == nil {
				fnAddFlash(rw, r, "You need to be signed in to see this page")

				http.Redirect(rw, r, fnSigninEndpoint(), http.StatusSeeOther)
				return
			}

			next.ServeHTTP(rw, r)
		})
	}
}

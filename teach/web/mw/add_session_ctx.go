package mw

import (
	"context"
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/rs/zerolog/log"
)

type ContextKeySession struct{}

const (
	SessionKey = "session"
)

type AddSessionStoreOptions struct {
	SessionStore sessions.Store
}

func AddSessionCtx(options *AddSessionStoreOptions) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(rw http.ResponseWriter, r *http.Request) {
			ses, err := options.SessionStore.Get(r, SessionKey)
			if err != nil {
				log.Error().Err(err).Msg("mw: failed to get session")
			}

			r = r.WithContext(context.WithValue(r.Context(), ContextKeySession{}, ses))
			next.ServeHTTP(rw, r)
		}

		return http.HandlerFunc(fn)
	}
}

func SessionFromContext(ctx context.Context) *sessions.Session {
	// we discard our OK value, as we do not need it,
	// but only so we do not panic! As if we cannot
	// get any value, then we get a nil pointer, which
	// should never happen
	session, _ := ctx.Value(ContextKeySession{}).(*sessions.Session)
	return session
}

package mw

import (
	"context"
	"database/sql"
	"errors"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

type ContextKeyUserInfo struct{}
type SessionKeyUserID struct{}

type AddUserCtxOptions struct {
	Worm *worm.WORM
}

func AddUserCtx(options *AddUserCtxOptions) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(rw http.ResponseWriter, r *http.Request) {
			// get userid
			ses := SessionFromContext(r.Context())
			userid := ses.Values[SessionKeyUserID{}]

			// if empty userid, then user is most
			// likely not logged in
			if userid == "" {
				next.ServeHTTP(rw, r)
				return
			}

			// we have a token, try to validate this and then
			// add a value in our context that indicates this
			var user models.User
			if err := options.Worm.Select(&user).Where(worm.Eq{"id": userid}).Run(r.Context()); err != nil {
				// if no rows were found, then just assume not logged in and call next
				if !errors.Is(err, sql.ErrNoRows) {
					log.Error().Err(err).Msg("AddUserCtx: failed to get user, calling next anyways")
				}

				next.ServeHTTP(rw, r)
				return
			}

			// store user in context
			r = r.WithContext(context.WithValue(r.Context(), ContextKeyUserInfo{}, &user))

			next.ServeHTTP(rw, r)
		}

		return http.HandlerFunc(fn)
	}
}

func UserFromContext(ctx context.Context) *models.User {
	// we discard our OK value, as we do not need it,
	// but only so we do not panic! As if we cannot
	// get any value, then we get a nil pointer, and
	// we will use that to determine if we have
	// any information on the user
	user, _ := ctx.Value(ContextKeyUserInfo{}).(*models.User)
	return user
}

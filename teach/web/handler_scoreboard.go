package web

import (
	"fmt"
	"html/template"
	"net/http"
	"sort"

	sq "github.com/Masterminds/squirrel"
	uuid "github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

const (
	scoreboardLimitUsers = 100
)

func (w *Web) HandlerScoreboard(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	// default template
	tmpl := w.templateGet("scoreboard")

	// struct to use for template
	type tmplUserScore struct {
		User  models.User
		Score uint64
	}

	// users with scoreboard enabled
	var users map[uuid.UUID]models.User
	if err := w.Worm.Select(&users).Where(worm.Eq{"participate_scoreboard": true}).Limit(scoreboardLimitUsers).Run(r.Context()); err != nil {
		log.Error().Err(err).Msg("failed to get participate users")
		return tmpl, nil, http.StatusInternalServerError, err
	}

	// get a slice of user IDs
	var userIDs []uuid.UUID
	for userID := range users {
		userIDs = append(userIDs, userID)
	}

	// model query to use to select the appropiate challenges
	modelChallengeCompletedQuery := sq.Select("challenge_id").From("completed_challenges").Where(sq.Eq{"user_id": userIDs}).Distinct()
	sql, params, err := modelChallengeCompletedQuery.ToSql()
	if err != nil {
		return nil, nil, http.StatusInternalServerError, err
	}
	whereStmt := worm.And{worm.Eq{"active": true}, sq.Expr(fmt.Sprintf("id in (%s)", sql), params...)}

	// all challenges
	var challenges map[string]models.Challenge
	if err := w.Worm.Select(&challenges).Where(whereStmt).Run(r.Context()); err != nil {
		log.Error().Err(err).Msg("failed to get challenges")
		return tmpl, nil, http.StatusInternalServerError, err
	}

	// calculate a score for each user
	var userRanking []tmplUserScore
	for userid, user := range users {
		var score uint64
		for challengeid := range user.CompletedChallenges {
			score += uint64(challenges[challengeid].Difficulty)
		}

		userRanking = append(userRanking, tmplUserScore{
			User:  users[userid],
			Score: score,
		})
	}

	// sort users by score
	sort.SliceStable(userRanking, func(i, j int) bool { return userRanking[i].Score > userRanking[j].Score })

	return tmpl, userRanking, 200, nil
}

package web

import (
	"html/template"
	"net/http"
)

func (w *Web) HandlerIndex(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	return w.templateGet(""), nil, http.StatusOK, nil
}

func (w *Web) HandlerAbout(rw http.ResponseWriter, r *http.Request) (*template.Template, interface{}, int, error) {
	return w.templateGet("about"), nil, http.StatusOK, nil
}

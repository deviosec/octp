package web

import (
	"context"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

type challengesFilter struct {
	Tags      []string
	WithLabs  bool
	WithFiles bool
}

func (w *Web) filterChallenges(ctx context.Context, f challengesFilter) (map[string]models.Challenge, error) {
	var challenges map[string]models.Challenge

	// init our where variable, which will be used
	// to add all our filters to.
	var where sq.Sqlizer

	// initially set that only active challenges
	// will be returned
	where = worm.Eq{"active": true}

	// if any tags are given, then add the filter
	if len(f.Tags) > 0 {
		// init a base query, which distinctly selects all challenge ids with the specified tags
		modelTagsQuery := sq.Select("challenge_id").From("challenge_tags").Where(sq.Eq{"tag": f.Tags}).Distinct()
		sql, params, err := modelTagsQuery.ToSql()
		if err != nil {
			return nil, err
		}

		// add this to our `where` part
		where = worm.And{where, sq.Expr(fmt.Sprintf("id in (%s)", sql), params...)}
	}

	if f.WithLabs {
		where = worm.And{where, worm.Gt{"LENGTH(lab)": 50}}
	}

	if f.WithFiles {
		where = worm.And{where, worm.Gt{"LENGTH(files)": 10}}
	}

	// execute query
	if err := w.Worm.Select(&challenges).Where(where).Run(ctx); err != nil {
		return nil, err
	}

	return challenges, nil
}

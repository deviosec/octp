package web

import (
	"html/template"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/web/mw"
)

type handler struct {
	// function - handle http function
	fn handlerFunc

	fnAddFlash func(rw http.ResponseWriter, r *http.Request, msg FlashMessage)
	fnFlashes  func(rw http.ResponseWriter, r *http.Request) []FlashMessage
}

type handlerFunc func(http.ResponseWriter, *http.Request) (template *template.Template, data interface{}, statusCode int, err error)

func (h handler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	// template data structure, for our requests
	type tmplDataStructure struct {
		Errors []FlashMessage
		User   *models.User
		Data   interface{}
	}

	// execute the handler
	tmpl, data, statusCode, err := h.fn(rw, r)

	// 500 means internal error, which we do not pass on
	if statusCode == 500 {
		log.Error().Err(err).Msg("ServeHTTP: internal server error")
		err = nil
	}

	// add err as a flash
	if err != nil {
		log.Error().Err(err).Msg("response handler got error")
		h.fnAddFlash(rw, r, FlashMessage{FlashLevelDanger, err.Error()})
	}

	// if no template set, then do nothing
	if tmpl == nil && statusCode != 0 {
		log.Info().Msg("ServeHTTP: called with nil template, and statusCode was not 0")
		return
	}

	// setup our data to the template
	tmplData := tmplDataStructure{
		Data: data,
	}

	if tmpl != nil && (statusCode == http.StatusOK || statusCode == http.StatusUnauthorized || statusCode == http.StatusBadRequest) {
		tmplData.Errors = h.fnFlashes(rw, r)
	}

	// try to get any user data from our context
	user := mw.UserFromContext(r.Context())
	if user != nil {
		tmplData.User = user
	}

	// statusCode `0` means that we should not write the StatusCode out
	if statusCode != 0 {
		rw.WriteHeader(statusCode)
	}

	// if err := w.templateGet(name).ExecuteTemplate(rw, "base", data); err != nil {
	if tmpl != nil {
		if err := tmpl.ExecuteTemplate(rw, "base", tmplData); err != nil {
			log.Printf("ServeHTTP: failed to execute template '%s', got error: %v", tmpl.Name(), err)
		}
	}
}

package web

import (
	"context"
	"database/sql"
	"errors"
	"net/http"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/simplecrypto"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

var (
	ErrNoUserFound = errors.New("no user found")
	ErrDatabaseErr = errors.New("unxepcted database error")
)

// ValidateUser validates the user based on the username and password provided
// It verifies the information against the stored values in the database.
// ValidateUser will lowercase the username, and check if any lowercase equvilant
// already exists in the database. This is done to ensure, that there isn't a uppercase
// and a lowercase username.
func (w *Web) validateUser(context context.Context, username string, password *string) (*models.User, error) {
	// check user exists
	var user models.User
	lowerUsername := strings.ToLower(username)

	// compare the lowercased version of the username, with a LOWER() version in the database
	if err := w.Worm.Select(&user).Where(worm.Eq{"LOWER(username)": lowerUsername}).Run(context); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoUserFound
		}

		log.Error().Err(err).Msg("failed to find user")
		return nil, ErrDatabaseErr
	}
	// if passowrd is set to nil it does not have to be validated
	if password == nil {
		return nil, nil
	}

	// check password is correct against stored hashed value
	ok, err := simplecrypto.ValidatePasswordHash(*password, user.HashedPassword)
	if err != nil {
		return nil, err
	}

	// unauthorised
	if !ok {
		return nil, nil
	}

	return &user, nil
}

// redirectBack redirects the user to the location of the `referer` header.
// If the referer header is empty, then defaults to redirect back to `/`
func redirectBack(rw http.ResponseWriter, r *http.Request) {
	// get refer header, so that we can send them back,
	// if no refer header is there, then we just send them to `/`
	referer := r.Header.Get("referer")

	if referer == "" {
		referer = "/"
	}

	http.Redirect(rw, r, referer, http.StatusSeeOther)
}

package web

import (
	"encoding/gob"

	"gitlab.com/deviosec/octp/teach/web/mw"
)

// magic function, that runs when the package is imported
// used to register structs to be used for session
func init() {
	gob.Register(FlashMessage{})
	gob.Register(mw.SessionKeyUserID{})
}

package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/deviosec/octp/teach/daemon"
)

var (
	flagConfig = flag.String("config", "", "path to configuration file, if empty reads from default location")
)

func main() {
	// parse flags
	flag.Parse()

	// seed our random number generator at each start
	rand.Seed(time.Now().UnixNano())

	// configure viper
	viper.SetConfigName("teach")
	viper.SetEnvPrefix("octp_teach")
	viper.AutomaticEnv()

	// prefer flag configuration, over normal paths
	if flagConfig != nil && *flagConfig != "" {
		viper.SetConfigFile(*flagConfig)
	} else {
		// get user configuration dir, if flag is not specified,
		// and add that path to our lookup paths
		userConfigDir, err := os.UserConfigDir()
		if err != nil {
			log.Fatal().Err(err).Msg("teach: failed to get userConfigDir")
		}

		octpConfigPath := fmt.Sprintf("%s/octp", userConfigDir)
		viper.AddConfigPath(octpConfigPath)
	}

	// read config file
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("teach: failed to read configuration file")
	}

	var conf daemon.Config
	if err := viper.Unmarshal(&conf); err != nil {
		log.Fatal().Err(err).Msg("teach: failed to unmarshal configuration file")
	}

	// validate config and set defaults
	if err := conf.ValidateAndDefaults(); err != nil {
		log.Fatal().Err(err).Msg("failed to validate config")
	}

	d := daemon.Daemon{
		Config: conf,
	}

	if err := d.Start(); err != nil {
		log.Fatal().Err(err).Msg("failed to start daemon")
		return
	}
}

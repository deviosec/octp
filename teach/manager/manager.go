package manager

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/dns"
	"gitlab.com/deviosec/octp/internal/providers"
	"gitlab.com/deviosec/octp/internal/registryauth"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/pkgs/teachworkerapi"
)

// Inital design idea
// # Functions
// 	- Goroutine 1
// 		- Take down old challenges for users that have reached timeout
// 		- Remove old workers, that are no longer used (according to hours up, do not remove machines which still have time left)
// 		- Spin up new workers when it is needed
// 	- Goroutine 2
// 		- Handle soft actions such as reset of a challenge, or destroy a challenge from the server
// 	- Goroutine 3
// 		- Handle setup of new challenges, when a user request them
// 	- Goroutine 4 (oldJobRemover)
// 		- Remove old jobs that have not been checked?
// 	- Goroutine 5
// 		- Sanity check/ping workers to see if what they are running is right

// a task to ensure we have available workers
// task to ensure we stop/destroy challenges on timeout (could be the same as the above)
// task that executes restarts, etc. (soft actions, restart, stop, etc. not starting of now challenges - renew??)
// task that handles setup of new challenges for users (might be a main which has multiple goroutines)

const (
	WorkerPrefix       = "TEACH-"
	MaxWorkers         = 3
	ChallengesPrWorker = 40
	BufferChallenges   = 20

	ChallengeTimeout = 30 * time.Minute

	WorkerUpTimeout = 80

	WorkerMinUptime = 5 * time.Hour

	WorkerPayPrMin     = 60 * time.Minute
	WorkerTimeleftKill = 30 * time.Minute

	EncryptionKeyLength = 32 // AES-256

	TimeoutWorkersFillBuffer       = 2 * time.Minute
	TimeoutWorkersRemoveUnused     = 2 * time.Minute
	TimeoutChallengesRemoveExpired = 2 * time.Minute
	TimeoutFetchChallengeStates    = 2 * time.Minute
)

// TODO(eyJhb) add time mocking, to test how well
// this manager works
type Manager struct {
	Worm               *worm.WORM
	Provider           providers.Provider
	WorkerAPI          teachworkerapi.Client // teachworker api client
	RegistryTokenMaker *registryauth.Maker   // used to generate auth tokens for registry
	DNSEnable          bool
	DNS                dns.DNSProvider

	// Server is the full path to the server
	// http(s)://(domain|ip)(:port)
	Server            string
	RegistryLink      string
	SSHKeyFingerprint string // public ssh key fingerprint

	// challengeLock is used when starting any challenge
	// to ensure that multiple threads do not call it
	// at the same time, having potential race conditions
	challengeLock sync.Mutex

	// anything that might create workers
	// needs to have this lock, before they
	// start to make any deciding logic
	// this is to ensure, that we do not
	// create too many pods all of the sudden
	workerLock sync.Mutex

	// challengeStateLock is used when accesing the
	// challengeStates map for read/writes
	challengeStateLock sync.RWMutex
	challengeStates    map[string]models.ChallengeState
}

// start all the goroutines etc.
func (m *Manager) Start() error {
	// start all our goroutines that happen in the background

	// TODO(eyJhb) we should have some kind of stop
	// indicatior for all our goroutines
	go func() {
		log.Print("Starting workersFillBuffer")
		for {
			time.Sleep(time.Second)

			ctx, cancel := context.WithTimeout(context.Background(), TimeoutWorkersFillBuffer)
			if err := m.workersFillBuffer(ctx); err != nil {
				log.Error().Err(err).Msg("teach.manager.Start: failed to fill worker buffer")
			}
			cancel()
		}
	}()

	go func() {
		log.Print("Starting workersRemoveUnused")
		for {
			time.Sleep(time.Second)

			ctx, cancel := context.WithTimeout(context.Background(), TimeoutWorkersRemoveUnused)
			if err := m.workersRemoveUnused(ctx); err != nil {
				log.Error().Err(err).Msg("teach.manager.Start: failed to remove unused workers")
			}
			cancel()
		}
	}()

	go func() {
		log.Print("Starting challengesRemoveExpired")
		for {
			time.Sleep(time.Second)

			ctx, cancel := context.WithTimeout(context.Background(), TimeoutChallengesRemoveExpired)
			if err := m.challengesRemoveExpired(ctx); err != nil {
				log.Error().Err(err).Msg("teach.manager.Start: failed to remove expired workers")
			}
			cancel()

		}
	}()

	go func() {
		log.Print("Starting fetchChallengeStates")
		for {
			time.Sleep(time.Second)

			ctx, cancel := context.WithTimeout(context.Background(), TimeoutFetchChallengeStates)
			if err := m.fetchChallengeStates(ctx); err != nil {
				log.Error().Err(err).Msg("teach.manager.Start: failed to fetch challenge states")
			}
			cancel()

		}
	}()

	return nil
}

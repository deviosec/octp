package manager

import (
	"errors"
	"math/rand"

	"gitlab.com/deviosec/octp/internal/badports"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

// findAvailablePort finds available ports for the challenge that is not currently used by the worker,
// using random and then seeing if they are available
func FindAvailablePorts(worker models.TeachWorker, challenge models.Challenge) (map[string]map[string]int, error) {
	// make map of currently used ports
	usedPorts := make(map[int]bool)
	for _, chall := range worker.UserChallenges {
		for _, virtPorts := range chall.Ports {
			for _, port := range virtPorts {
				usedPorts[port] = true
			}
		}
	}

	// find some new ports
	foundPorts := make(map[string]map[string]int)
	for _, virt := range challenge.Lab.Virtuals {
		for portName := range virt.Ports {
			var tryPort int
			for tryNum := 0; tryNum <= 1000; tryNum++ {
				tryPort = rand.Intn(65535-1000) + 1000 // always start at 1000

				// check if the port is a bad port
				// (port that cannot be used)
				if badports.IsBad(uint16(tryPort)) {
					continue
				}

				// if not in use, assign this
				if !usedPorts[tryPort] {
					break
				}
			}

			if tryPort == 0 {
				return nil, errors.New("could not find any available port for challenge")
			}

			if foundPorts[virt.Name] == nil {
				foundPorts[virt.Name] = make(map[string]int)
			}

			foundPorts[virt.Name][portName] = tryPort
		}
	}

	return foundPorts, nil
}

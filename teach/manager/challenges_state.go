package manager

import (
	"context"

	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

func (m *Manager) fetchChallengeStates(ctx context.Context) error {
	// fetch all active teachworkers
	var workers []models.TeachWorker
	if err := m.Worm.Select(&workers).Run(ctx); err != nil {
		return err
	}

	// get current user challenges
	var userChallenges []models.UserChallenge
	if err := m.Worm.Select(&userChallenges).Run(ctx); err != nil {
		return err
	}

	// loop over all teachworkers, and get the states
	workersStates := make(map[string]models.ChallengeState)
	workerFetchError := make(map[string]bool)
	for _, tw := range workers {
		// get the ChallengeState for all challenges using the API
		states, err := m.WorkerAPI.ListChallengeStates(ctx, tw)
		if err != nil {
			workerFetchError[tw.ID.String()] = true
			return err
		}

		// add to our allStates
		for stateKey, state := range states {
			workersStates[stateKey] = state
		}
	}

	// create our final states
	finalStates := make(map[string]models.ChallengeState)

	// match them up with the user challenges currently running
	for _, userChall := range userChallenges {
		userChallID := userChall.ID.String()
		// check if an error occured, when trying to get the state
		// of the current challenge.
		if workerFetchError[userChall.TeachWorkerID.String()] {
			finalStates[userChallID] = models.ChallengeStateWorkerUnavilable
			continue
		}

		// if it exists in our worker states, assign state from there
		if v, ok := workersStates[userChallID]; ok {
			finalStates[userChallID] = v
			continue
		}

		// otherwise some default logic
		if userChall.TeachWorkerID.String() != "" {
			finalStates[userChallID] = models.ChallengeStateAssigned
		} else {
			finalStates[userChallID] = models.ChallengeStateCreated
		}
	}

	// update the challengeStates
	m.challengeStateLock.Lock()
	defer m.challengeStateLock.Unlock()
	m.challengeStates = finalStates

	return nil
}

func (m *Manager) ChallengeState(userChallengeID string) models.ChallengeState {
	m.challengeStateLock.RLock()
	defer m.challengeStateLock.RUnlock()

	if v, ok := m.challengeStates[userChallengeID]; ok {
		return v
	}

	// we do not have any state on the challenge
	// return unknown state then
	return models.ChallengeStateUnknown
}

package manager

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/deviosec/octp/internal/registryauth"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

var (
	ErrAlreadyActiveChallenge = errors.New("user already has active challenge")
	// no workers mean there are NO workers at all that have any slots left for challenges
	ErrNoWorkersAvailable = errors.New("no workers available to run the challenge")
	// means that there was no viable workers, ie. there was workers with slots available, but
	// they did not pass the tests
	ErrNoViableWorkersAvailable = errors.New("no viable workers available to run the challenge")

	ErrNoSuchUser = errors.New("user does not exists")
)

func (m *Manager) challengesRemoveExpired(ctx context.Context) error {
	var userChallenges []models.UserChallenge
	if err := m.Worm.Select(&userChallenges).Run(ctx); err != nil {
		return err
	}

	for _, userChallenge := range userChallenges {
		userID := userChallenge.UserID.String()

		// if it has expired delete userChallenge from worker + store
		if time.Since(userChallenge.CreatedAt) > ChallengeTimeout { // remmove from the worker
			if err := m.StopChallenge(ctx, userID); err != nil {
				log.Printf("removeExpiredChallenges: Failed to delete challenge from worker for user %s with workerID %s", userID, userChallenge.TeachWorkerID)
			}

			log.Printf("challengesRemoveExpired: removed challenge %s for user %s from worker %s", userChallenge.ChallengeID, userID, userChallenge.TeachWorkerID)
		}
	}

	return nil
}

// TODO(eyJhb) this assume there cannot be multiple active challenges for a user
func (m *Manager) ActiveChallenge(ctx context.Context, userID string) (models.TeachWorker, models.UserChallenge, bool, error) {
	var userChallenge models.UserChallenge
	if err := m.Worm.Select(&userChallenge).Where(worm.Eq{"user_id": userID}).Limit(1).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.TeachWorker{}, models.UserChallenge{}, false, nil
		}

		return models.TeachWorker{}, models.UserChallenge{}, false, err
	}

	var teachWorker models.TeachWorker
	// at this point, if we get any errors, it is because some error on the DB happened
	// or because the teach_worker_id does not exists
	if err := m.Worm.Select(&teachWorker).Where(worm.Eq{"id": userChallenge.TeachWorkerID}).Run(ctx); err != nil {
		return models.TeachWorker{}, models.UserChallenge{}, false, err
	}

	return teachWorker, userChallenge, true, nil
}

func (m *Manager) StopChallenge(ctx context.Context, userID string) error {
	tx, err := m.Worm.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	var userChallege models.UserChallenge
	if err := m.Worm.Select(&userChallege).Where(worm.Eq{"user_id": userID}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil
		}

		return err
	}

	var worker models.TeachWorker
	if err := m.Worm.Select(&worker).Where(worm.Eq{"id": userChallege.TeachWorkerID}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil
		}
		return err
	}

	if err := m.WorkerAPI.DeleteChallenge(ctx, userChallege.ID.String(), worker); err != nil {
		return err
	}

	if err := tx.Delete(&userChallege).Run(ctx); err != nil {
		return err
	}

	tx.Commit()
	return nil
}

func (m *Manager) StartChallenge(ctx context.Context, userID, challengeID string) (string, models.UserChallenge, error) {
	// lock first, as we do not want multiple to do this at once
	m.challengeLock.Lock()
	defer m.challengeLock.Unlock()

	// check if user exists
	var user models.User
	if err := m.Worm.Select(&user).Where(worm.Eq{"id": userID}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", models.UserChallenge{}, ErrNoSuchUser
		}
		return "", models.UserChallenge{}, err
	}

	// check if user already have challenges running
	var noActiveChallenge bool
	if err := m.Worm.Select(&models.UserChallenge{}).Where(worm.Eq{"user_id": userID}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			noActiveChallenge = true
		}
	}

	if !noActiveChallenge {
		return "", models.UserChallenge{}, ErrAlreadyActiveChallenge
	}

	// get challenge and ensure that it exists, etc.
	var challenge models.Challenge
	if err := m.Worm.Select(&challenge).Where(worm.Eq{"id": challengeID}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", models.UserChallenge{}, fmt.Errorf("the challenge with the id '%s' does not exist on the server", challengeID)
		}
		return "", models.UserChallenge{}, err
	}

	// find viable teachworker
	worker, err := m.findViableTeachWorker(ctx)
	if err != nil {
		return "", models.UserChallenge{}, err
	}

	// find ports to use for that worker, with the challenge
	portsAvailable, err := FindAvailablePorts(worker, challenge)
	if err != nil {
		return "", models.UserChallenge{}, err
	}

	// populate challenge here
	if err := PopulateChallenge(worker, &challenge, portsAvailable); err != nil {
		return "", models.UserChallenge{}, err
	}

	// begin a transaction here, for adding the challenge to database + worker + attempt
	tx, err := m.Worm.Begin(ctx)
	if err != nil {
		return "", models.UserChallenge{}, err
	}
	defer tx.Rollback()

	userChallenge := models.UserChallenge{
		Ports: portsAvailable,

		ChallengeID:   challenge.ID,
		UserID:        user.ID,
		TeachWorkerID: worker.ID,
	}

	if err := tx.Insert(&userChallenge).Run(ctx); err != nil {
		return "", models.UserChallenge{}, err
	}

	// generate auth tokens so the teachworker can pull the challenge
	auth, err := m.generateChallengeAuthToken(ctx, userChallenge.ID.String(), challenge)
	if err != nil {
		return "", models.UserChallenge{}, err
	}

	// now we can push to our worker
	if err := m.WorkerAPI.AddChallenge(ctx, userChallenge.ID.String(), worker, challenge, auth); err != nil {
		return "", models.UserChallenge{}, err
	}

	challengeAttempt := models.ChallengeAttempt{
		UserID:      user.ID,
		ChallengeID: challenge.ID,
	}

	if err := tx.Insert(&challengeAttempt).Run(ctx); err != nil {
		log.Printf("StartChallenge: failed to add statistics triedChallenge for user '%s' with error %v", userID, err)
	}

	tx.Commit()
	return worker.ID.String(), userChallenge, nil
}

func (m *Manager) generateChallengeAuthToken(ctx context.Context, tokenID string, challenge models.Challenge) (virtmodels.Auth, error) {
	// get list of images, which we can use to generate auth token,
	// if RegistryTokenMaker is not nil (auth is enabled).
	// prepend our RegistryLink to each image as well, this ensures
	// that the workers can fetch the image from our registry.
	var virtImages []string
	for i, virt := range challenge.Lab.Virtuals {
		virtImages = append(virtImages, virt.Image)
		challenge.Lab.Virtuals[i].Image = fmt.Sprintf("%s/%s", m.RegistryLink, virt.Image)

	}

	// generate auth token for registry
	var auth virtmodels.Auth

	// generate auth token, if auth is enabled and there are any images
	// to generate a auth token for.
	if m.RegistryTokenMaker != nil && len(virtImages) > 0 {
		token, err := m.RegistryTokenMaker.GenerateToken(tokenID, []string{registryauth.ActionPull}, virtImages, ChallengeTimeout)
		if err != nil {
			return virtmodels.Auth{}, err
		}

		auth.Token = token
	}

	return auth, nil
}

func (m *Manager) findViableTeachWorker(ctx context.Context) (models.TeachWorker, error) {
	// first we need to ensure, that we actually have space for a challenge
	excess, err := m.bufferExcess(ctx)
	if err != nil {
		return models.TeachWorker{}, err
	}

	if excess <= 0 {
		return models.TeachWorker{}, ErrNoWorkersAvailable
	}

	// get workers, so that we can score them
	var workers []models.TeachWorker
	if err := m.Worm.Select(&workers).Run(ctx); err != nil {
		return models.TeachWorker{}, err
	}

	calculateScore := func(tw models.TeachWorker) float64 {
		// x is our input value
		// min and max are the min and max values the input x value can take
		// a and b is the range it should mapped to (0,10)
		mapValueToRange := func(x, min, max, a, b float64) float64 {
			return ((b-a)*(x-min))/(max-min) + a
		}

		// if WORKER_MIN_UPTIME is too close, then we do not add anymore challenges to it
		// we can no longer use this worker
		if time.Since(tw.CreatedAt) >= (WorkerMinUptime + ChallengeTimeout) {
			return 0
		}

		// it no longer has space for more
		if len(tw.UserChallenges) >= ChallengesPrWorker {
			return 0
		}

		scoreTable := map[string]float64{
			"timecreated":   8, // it is quite important, that we use the oldest first
			"numchallenges": 5, // not as important, that the amout of challenges are large
		}

		// map our values between 0 and 10, for time and challenges
		timeScore := scoreTable["timecreated"] * mapValueToRange(time.Since(tw.CreatedAt).Minutes(), 0, WorkerMinUptime.Minutes(), 0, 10)
		challengeScore := scoreTable["numchallenges"] * mapValueToRange(float64(len(tw.UserChallenges)), 0, ChallengesPrWorker, 0, 10)

		return timeScore + challengeScore
	}

	// calculate all the scores, and get the best candidate
	var bestScore float64
	var worker models.TeachWorker
	for _, w := range workers {
		if score := calculateScore(worker); score > bestScore || bestScore == 0.0 {
			bestScore = score
			worker = w
		}
	}

	// decide which worker to use
	if worker.ID == uuid.Nil {
		return models.TeachWorker{}, ErrNoViableWorkersAvailable
	}

	return worker, nil
}

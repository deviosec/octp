package manager

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

const (
	cnstEnvPublicAddress = "OCTP_PUBLIC_ADDRESS"
)

var (
	reEnvPublicVirtualPort = regexp.MustCompile(`^(OCTP_PUBLIC_PORT_([A-Z0-9\-]+)_([A-Z0-9]+))=.*`)
)

func PopulateChallenge(worker models.TeachWorker, challenge *models.Challenge, portsAvailable map[string]map[string]int) error {
	// populate the ports and flags
	populateChallengeVirtualPorts(challenge, portsAvailable)
	populateChallengeVirtualFlags(challenge)
	populateChallengeVirtualEnvsPublicAddressPorts(challenge, worker, portsAvailable)

	return nil
}

func populateChallengeVirtualPorts(challenge *models.Challenge, availablePorts map[string]map[string]int) {
	for _, virt := range challenge.Lab.Virtuals {
		ports := availablePorts[virt.Name]
		for portName, port := range virt.Ports {
			port.Host = ports[portName]
			virt.Ports[portName] = port
		}
	}
}

func populateChallengeVirtualFlags(challenge *models.Challenge) {
	// populate the flags
	for virtI, virt := range challenge.Lab.Virtuals {
		for envI, env := range virt.Envs {
			if !strings.Contains(env, "FLAG_") {
				continue
			}

			for _, flag := range challenge.Flags {
				challenge.Lab.Virtuals[virtI].Envs[envI] = strings.Replace(env, fmt.Sprintf("##FLAG_%s##", flag.Name), flag.Flag, -1)
			}
		}
	}
}

func populateChallengeVirtualEnvsPublicAddressPorts(challenge *models.Challenge, worker models.TeachWorker, origPorts map[string]map[string]int) {
	// convert our ports map virtname to uppercase
	ports := make(map[string]map[string]int)
	for virtName, virtPorts := range origPorts {
		ports[strings.ToUpper(virtName)] = virtPorts
	}

	// replace the correct envs, if the challenge requires it
	for _, virt := range challenge.Lab.Virtuals {
		for envI, env := range virt.Envs {
			// check if env wants public address of teach worker
			if strings.HasPrefix(env, cnstEnvPublicAddress) {
				virt.Envs[envI] = fmt.Sprintf("%s=%s", cnstEnvPublicAddress, worker.IP)
			} else if reEnvPublicVirtualPort.MatchString(env) {
				// no need to check for length of matches, as the `MatchString`
				// should ensure a match.
				matches := reEnvPublicVirtualPort.FindStringSubmatch(env)

				envName := matches[1]
				virtName := matches[2]
				portName := matches[3]

				// ensure that virtName + port exists
				if virtPorts, ok := ports[virtName]; ok {
					if portNumber, ok := virtPorts[portName]; ok {
						virt.Envs[envI] = fmt.Sprintf("%s=%d", envName, portNumber)
					}
				}
			}
		}
	}
}

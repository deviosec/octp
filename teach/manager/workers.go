package manager

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/deviosec/octp/internal/dns"
	"gitlab.com/deviosec/octp/internal/simplecrypto"
	"gitlab.com/deviosec/octp/internal/wordid"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

const (
	// - #'curl https://gitlab.com/deviosec/scripts/raw/master/octp/install.sh -o /tmp/octp-install.sh'
	WorkerTemplate = `#cloud-config
runcmd:
  - apt update
  - apt install -y curl sudo jq
  - 'curl ##url_agent## -o /usr/local/bin/octp-worker'
  - chmod +x /usr/local/bin/octp-worker
  - 'curl "https://get.docker.com" | sh'
  - chown odaemon:odaemon -R /home/odaemon/
  - systemctl daemon-reload
  - systemctl enable octp-worker.service
  - systemctl start octp-worker.service
  - apt install -y apparmor
  - reboot
users:
  - name: odaemon
    groups: [docker, sudo]
    shell: /bin/bash
write_files:
  - path: /home/odaemon/.config/octp/worker.yml
    content: |
      id: ##worker_id##
      encryption_key: ##encryption_key##
  - path: /etc/systemd/system/octp-worker.service
    content: |
      [Unit]
      Description=OCTP Teach Worker Unit

      [Service]
      User=odaemon
      Group=odaemon
      Type=simple
      ExecStart=/usr/local/bin/octp-worker
      Restart=always
      RestartSec=10

      [Install]
      WantedBy=multi-user.target
`
)

// TODO(eyJhb) reenable debug at some point
func (m *Manager) workersRemoveUnused(ctx context.Context) error {
	m.workerLock.Lock()
	defer m.workerLock.Unlock()

	// get the workers we have atm.
	var workers []models.TeachWorker
	if err := m.Worm.Select(&workers).Run(ctx); err != nil {
		return err
	}

	// 1. Is there any challenge currently on the worker? continue
	// 2. How old is the VPS, is there more than 35 minutes before it expires? continue
	// 2.1. Is the worker younger than 5 hours? continue
	// 3. If we delete this worker, and we will no longer forfill the buffer requirement - continue
	// 4. Delete the worker and try next
	for _, worker := range workers {
		// log.Printf("workersRemoveUnused: checking worker %s", worker.ID)

		bufferExcess, err := m.bufferExcess(ctx)
		if err != nil {
			log.Printf("workersRemoveUnused: failed to get bufferExcess - %v", err)
			return err
		}

		if len(worker.UserChallenges) > 0 {
			// log.Print("workersRemoveUnused: worker currently have active challenges, skipping")
			continue
		}

		// log.Printf("workersRemoveUnused: TIMELEFT_KILL - %d <= %d", WORKER_PAY_PR_MIN-(time.Since(worker.Created)%WORKER_PAY_PR_MIN), WORKER_TIMELEFT_KILL)
		if WorkerPayPrMin-(time.Since(worker.CreatedAt)%WorkerPayPrMin) <= WorkerTimeleftKill {
			// log.Print("workersRemoveUnused: skipping because of TIMELEFT_KILL")
			continue
		}

		// log.Printf("workersRemoveUnused: WORKER_MIN_UPTIME - %d < %d", time.Since(worker.Created), WORKER_MIN_UPTIME)
		if time.Since(worker.CreatedAt) < WorkerMinUptime {
			// log.Print("workersRemoveUnused: WORKER_MIN_UPTIME skipping ")
			continue
		}

		// log.Printf("workersRemoveUnused: BUFFER_CHALLENGES - %d <= %d", (bufferExcess - CHALLENGES_PR_WORKER), BUFFER_CHALLENGES)
		if (bufferExcess - ChallengesPrWorker) <= BufferChallenges {
			log.Printf("workersRemoveUnused: BUFFER_CHALLENGES skipping")
			continue
		}

		// delete it (remember to give the full name)
		if err := m.workerDestroy(ctx, worker); err != nil {
			log.Printf("removeUnusedWorkers: failed to destroy worker %s with error %v", worker.ID, err)
		} else {
			log.Printf("workersRemoveUnused: removed worker %s", worker.ID)
		}
	}

	// TODO(eyJhb) maybe this should be placed elsewhere?

	// we need to put in the edgecase, of when we only have a single
	// server, which is currently not used or not at capacity, because
	// at some point, it will be too old to run any new challenges, but
	// we will not delete it, as we will then not forfill our requirements!
	if len(workers) == 1 {
		// it we are close to having the worker expire
		// (WORKER_TIMELEFT_KILL + 10 min for spinup), then
		// we need to create a additional worker that can
		// be used, this enabled us to always have new servers

		// we use a for loop, as there will only be ONE worker in
		// our map, and because it is a map we do not know the key
		// to access it directly
		for _, worker := range workers {
			if time.Since(worker.CreatedAt) >= (WorkerMinUptime - (WorkerTimeleftKill + 10*time.Minute)) {
				log.Printf("We are currently getting a new worker, as the old one is about to expire: Uptime in minutes - %f >= %f", time.Since(worker.CreatedAt).Minutes(), (WorkerMinUptime - (WorkerTimeleftKill + 10*time.Minute)).Minutes())
				// add a worker
				if err := m.workerCreate(ctx); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

// workersFillBuffer will create a single worker each time called, if the current
// amount of workers does not forfill our buffer
func (m *Manager) workersFillBuffer(ctx context.Context) error {
	m.workerLock.Lock()
	defer m.workerLock.Unlock()

	// check if we need to do anything, if we forfill the buffer do nothing
	excess, err := m.bufferExcess(ctx)
	if err != nil {
		return err
	}

	// if we have more than or equal to our excess, return nil
	if excess >= BufferChallenges {
		return nil
	}

	// get workers so we can see if we have the max workers
	var workers []models.TeachWorker
	if err := m.Worm.Select(&workers).Run(ctx); err != nil {
		return err
	}

	if len(workers) == MaxWorkers {
		log.Print("workersFillBuffer: we have max number of workers (MAX_WORKERS), but we need more")
		return nil
	}

	log.Print("workersFillBuffer: did not forfill buffer, creating a teachworker")

	return m.workerCreate(ctx)
}

func (m *Manager) workerDestroy(ctx context.Context, worker models.TeachWorker) error {
	// destroy pod from provider
	err := m.Provider.DestroyPod(ctx, WorkerPrefix+worker.ID.String())
	if err != nil {
		return err
	}

	// remove dns pointing to server
	if m.DNSEnable {
		if err := m.DNS.Delete(ctx, worker.IP); err != nil {
			log.Printf("workerDestroy: failed to delete DNS record for worker %s, got error: %v", worker.ID, err)
		}
	}

	// delete from database
	if err := m.Worm.Delete(&worker).Run(ctx); err != nil {
		return err
	}

	return nil
}

func (m *Manager) workerCreate(ctx context.Context) error {
	tx, err := m.Worm.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// get encryptionKey
	encryptionKey := simplecrypto.Token(EncryptionKeyLength)

	// create teachworker to get ID
	teachworker := models.TeachWorker{
		EncryptionKey: encryptionKey,
	}
	if err := m.Worm.Insert(&teachworker).Run(ctx); err != nil {
		return err
	}
	ID := teachworker.ID.String()

	// prepare our template
	tmpl, err := workerGenerateTemplate(WorkerTemplate, ID, m.Server, encryptionKey)
	if err != nil {
		return err
	}

	// create pod/event
	if err := m.Provider.CreatePod(ctx, WorkerPrefix+ID, tmpl, m.Provider.DefaultImage(), []string{m.SSHKeyFingerprint}); err != nil {
		return err
	}

	// wait for the worker to become online, to get the IP
	var workerIP string
	for i := 0; i < WorkerUpTimeout; i++ {
		// sleep before we do anything!
		time.Sleep(time.Second)

		ip, err := m.Provider.PublicIPPod(ctx, WorkerPrefix+ID, false)
		if err != nil {
			log.Printf("Failed to get public ip of pod, %d retries out of %d remaining\n", i, WorkerUpTimeout)
			continue
		}

		if ip != "" {
			workerIP = ip
			break
		}
	}

	if workerIP == "" {
		return errors.New("timeout while waiting for our worker to get a IP")
	}

	// default to workerIP if DNS is not enabled
	teachworker.IP = workerIP

	// if DNS is enabled, then generate ahostname, get the IPv6 and create the subdomains
	if m.DNSEnable {
		// we got an IP for our pod, generate an hostname for it
		hostname := wordid.Generate(4)

		// add our IPv4 first with the given hostname
		dnsRecord := dns.Record{
			Name:    hostname,
			Type:    dns.RecordTypeA,
			Content: workerIP,
		}
		if err := m.DNS.Create(ctx, dnsRecord); err != nil {
			log.Printf("failed to create IPv4 hostname '%s' with record '%v' for teachworker %s, using IP instead, got error: %v", hostname, dnsRecord, ID, err)
			teachworker.IP = workerIP
		} else {
			// get IPv6 address if possible
			ip, err := m.Provider.PublicIPPod(ctx, WorkerPrefix+ID, true)
			if err != nil {
				log.Printf("failed to get IPv6 for teachworker %s, got error: %v", ID, err)
			} else {
				dnsRecord := dns.Record{
					Name:    hostname,
					Type:    dns.RecordTypeAAAA,
					Content: ip,
				}
				if err := m.DNS.Create(ctx, dnsRecord); err != nil {
					log.Printf("failed to create IPv6 hostname '%s' with record '%v' for teachworker %s, got error: %v", hostname, dnsRecord, ID, err)
				}
			}

			teachworker.IP = fmt.Sprintf("%s.%s", hostname, m.DNS.Domain())
		}
	}

	if err := m.Worm.Update(&teachworker).Run(ctx); err != nil {
		return err
	}

	tx.Commit()

	return nil
}

func workerGenerateTemplate(tmpl, workerID, server, encryptionKey string) (string, error) {
	// things that we replace
	replacements := make(map[string]string)

	// insert into our map
	// TODO(eyJhb): this is hardcoded, which it maybe should not be
	replacements["url_agent"] = fmt.Sprintf("%s/binaries/worker-linux-amd64", server)
	replacements["worker_id"] = workerID
	replacements["encryption_key"] = encryptionKey

	for k, v := range replacements {
		tmpl = strings.Replace(tmpl, fmt.Sprintf("##%s##", k), v, -1)
	}

	return tmpl, nil
}

// returns the number of available slots we have, on our workers.
// e.g. a number of 20 will express that we have 20 free slots that
// can be used for challenges. It should not be able to go negative,
// as thet means there is a error somewhere
func (m *Manager) bufferExcess(ctx context.Context) (int, error) {
	var workers []models.TeachWorker
	if err := m.Worm.Select(&workers).Run(ctx); err != nil {
		return 0, err
	}

	var challengeCounter int
	for _, worker := range workers {
		challengeCounter += len(worker.UserChallenges)
	}

	// we do not have enough workers for our buffer to be forfilled
	return (len(workers) * ChallengesPrWorker) - challengeCounter, nil
}

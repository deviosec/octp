package models

import (
	"fmt"
	"regexp"

	"github.com/hashicorp/go-multierror"
	"gitlab.com/deviosec/octp/internal/validate"
)

var (
	RegExpChalID      = regexp.MustCompile(`^[a-z0-9-]+$`)
	RegExpChalTitle   = regexp.MustCompile(`^[A-z0-9-! ,\.?':\-\(\)]+$`)
	RegExpDescription = regexp.MustCompile(`^[A-z0-9-!?\n:;/"':# ,\.\(\)+={}]+$`)
	RegExpCategories  = regexp.MustCompile(`^(web|pwn|bin|misc|reverse|forensics|crypto)$`)
	RegExpFlagKey     = regexp.MustCompile(`^[A-Z]+$`)
	RegExpFlag        = regexp.MustCompile(`^[A-z0-9{}!\(\):?'\|<>_]+$`)
	RegExpTag         = regexp.MustCompile(`^[a-z0-9-]+$`)

	RegExpFilename = regexp.MustCompile(`^[A-z0-9-_\.]+$`)

	RegExpSha256 = regexp.MustCompile(`^[A-Fa-f0-9]{64}$`)
	RegExpHint   = RegExpDescription
)

func (chal Challenge) Validate() error {
	var err *multierror.Error

	// validate base information
	err = multierror.Append(err, validate.String("id", chal.ID, RegExpChalID))
	err = multierror.Append(err, validate.String("title", chal.Title, RegExpChalTitle))
	err = multierror.Append(err, validate.String("description", chal.Description, RegExpDescription))
	err = multierror.Append(err, validate.String("category", chal.Category, RegExpCategories))

	// validate flags
	for _, flag := range chal.Flags {
		err = multierror.Append(err, validate.String(fmt.Sprintf("Flag name '%s'", flag.Name), flag.Name, RegExpFlagKey))
		err = multierror.Append(err, validate.String(fmt.Sprintf("Flag key '%s', value '%s'", flag.Name, flag.Flag), flag.Flag, RegExpFlag))
	}

	// challenge hints
	for _, chalHint := range chal.Hints {
		if challErr := chalHint.Validate(); challErr != nil {
			err = multierror.Append(err, challErr)
		}
	}

	// tags
	for tag := range chal.Tags {
		err = multierror.Append(err, validate.String(fmt.Sprintf("Tag key '%s'", tag), tag, RegExpTag))
	}

	// validate files
	for _, file := range chal.Files {
		if fileErrs := file.Validate(); fileErrs != nil {
			err = multierror.Append(err, fileErrs)
		}
	}

	// difficulty
	if chal.Difficulty > 100 || chal.Difficulty == 0 {
		err = multierror.Append(err, &validate.Error{Key: "difficulty", Value: chal.Difficulty, Msg: "difficulty must be between 1 and 100"})
	}

	// validate lab
	err = multierror.Append(err, chal.Lab.Validate())

	return err.ErrorOrNil()
}

func (chalHint ChallengeHint) Validate() error {
	var err *multierror.Error

	err = multierror.Append(err, validate.String(fmt.Sprintf("Hint '%s'", chalHint.Hint), chalHint.Hint, RegExpHint))

	if chalHint.Cost < 0 || chalHint.Cost > 5000 {
		err = multierror.Append(err, &validate.Error{Key: "hint", Value: chalHint.Cost, Msg: "challenge hint cost must be between 0 and 5000"})
	}

	return err.ErrorOrNil()
}

func (f File) Validate() error {
	var err *multierror.Error

	if f.ID != 0 {
		err = multierror.Append(err, &validate.Error{Key: "file", Value: f.ID, Msg: "id cannot be assigned to file"})
	}

	if f.Filename == "" {
		err = multierror.Append(err, validate.String("file", f.Filename, RegExpSha256))
	}

	if f.Link == "" {
		err = multierror.Append(err, &validate.Error{Key: "file", Value: f.Link, Msg: "link cannot be empty"})
	}

	if f.Sha256 != "" {
		err = multierror.Append(err, validate.String(fmt.Sprintf("file with name '%s', sha256 '%s'", f.Filename, f.Sha256), f.Sha256, RegExpSha256))
	}

	return err.ErrorOrNil()
}

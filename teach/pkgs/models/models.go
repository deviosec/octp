package models

import (
	"time"

	"github.com/gofrs/uuid"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
)

type ChallengeState uint8

const (
	ChallengeStateUnknown ChallengeState = iota
	ChallengeStateCreated
	ChallengeStateAssigned
	ChallengeStateWorkerUnavilable
	ChallengeStateWorkerError
	ChallengeStateWorkerFetching
	ChallengeStateWorkerCreating
	ChallengeStateWorkerStarting
	ChallengeStateWorkerRunning
)

func (cs ChallengeState) String() string {
	switch cs {
	case ChallengeStateCreated:
		return "created"
	case ChallengeStateAssigned:
		return "assigned"
	case ChallengeStateWorkerUnavilable:
		return "challenge server unavilable"
	case ChallengeStateWorkerError:
		return "challenge server error occured"
	case ChallengeStateWorkerFetching:
		return "fetching resources"
	case ChallengeStateWorkerCreating:
		return "creating environment"
	case ChallengeStateWorkerStarting:
		return "starting"
	case ChallengeStateWorkerRunning:
		return "running"
	default:
		return "unknown"
	}
}

// our base model, which uses UUID4 instead of int
type Base struct {
	ID        uuid.UUID  `sql:",primarykey,uuid"`
	CreatedAt time.Time  `sql:",createdAt"`
	UpdatedAt time.Time  `sql:",updatedAt"`
	DeletedAt *time.Time `sql:",deletedAt"`
}

// BaseNoID exists for structs that has
// its own primarykey
type BaseNoID struct {
	CreatedAt time.Time  `sql:",createdAt"`
	UpdatedAt time.Time  `sql:",updatedAt"`
	DeletedAt *time.Time `sql:",deletedAt"`
}

// User
type User struct {
	Base

	Email                 string
	Username              string
	HashedPassword        string
	ParticipateScoreboard bool

	TimeZone string

	UserChallenges      []UserChallenge               `sql:",embedded"`
	CompletedChallenges map[string]CompletedChallenge `sql:",mapkey:challengeid,embedded"`

	// Statistics
	FlagAttempts      []FlagAttempt      `sql:",embedded"`
	ChallengeAttempts []ChallengeAttempt `sql:",embedded"`
}

type TeachWorker struct {
	Base

	IP            string
	EncryptionKey string

	UserChallenges []UserChallenge `sql:",embedded"`
}

// TODO(eyJhb): change this in the future maybe?
type Config struct {
	BaseNoID

	Key   string `sql:",primarykey"`
	Value string
}

type Flag struct {
	Name         string `json:"name"`
	Flag         string `json:"flag"`
	DynamicEnv   bool   `json:"dynamic_env"`
	DynamicBuild bool   `json:"dynamic_build"`
}

type FlagAttempt struct {
	Base

	Name    string
	Value   string
	Correct bool

	ChallengeID string

	// foreign key/relation
	UserID uuid.UUID
}

type File struct {
	ID       int    `json:"id"`
	Filename string `json:"filename"`
	Link     string `json:"link"`
	Sha256   string `json:"sha256"`
}

type Challenge struct {
	BaseNoID
	ID          string                  `json:"id" sql:",primarykey"` // id of the challenge
	Title       string                  `json:"title"`
	Description string                  `json:"description"`
	Category    string                  `json:"category"`
	Flags       []Flag                  `json:"flags" sql:",json"`
	Hints       []ChallengeHint         `json:"hints" sql:",json"`
	Tags        map[string]ChallengeTag `json:"tags" sql:",mapkey:tag,embedded"`
	Files       []File                  `json:"files" sql:",json"`
	Active      bool                    `json:"active"` // indicates if the challenge is currently active
	Difficulty  uint8                   `json:"difficulty"`
	Lab         virtmodels.Lab          `json:"lab" sql:",json"`
}

type ChallengeTag struct {
	Tag         string `json:"tag"`
	ChallengeID string `json:"challege_id,omitempty"`
}

type ChallengeHint struct {
	Hint string `json:"hint"`
	Cost int    `json:"cost"`
}

type ChallengeAttempt struct {
	Base

	ChallengeID string

	// foreign key/relation
	UserID uuid.UUID
}

type UserChallenge struct {
	Base

	ChallengeID string
	// map[virtname]map[portname]<portnumber>
	Ports map[string]map[string]int `sql:",json"`

	// foreign key/relation
	UserID        uuid.UUID
	TeachWorkerID uuid.UUID
}

type CompletedChallenge struct {
	BaseNoID

	ChallengeID string
	UserID      uuid.UUID
}

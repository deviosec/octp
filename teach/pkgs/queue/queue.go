package queue

import (
	"sync"

	"gitlab.com/deviosec/octp/internal/simplecrypto"
)

type Queue interface {
	Pop() (interface{}, bool)
	Push(item interface{}) string
	IndexOf(token string) (int, bool)
	InQueue() int
}

type job struct {
	token string
	data  interface{}
}

type SimpleQueue struct {
	jobs []job

	emptyQueueCond sync.Cond
}

func (q *SimpleQueue) Pop() interface{} {
	// lock before we check our condition
	q.emptyQueueCond.L.Lock()

	for len(q.jobs) == 0 {
		// wait until this is free
		q.emptyQueueCond.Wait()
	}

	// remember to unlock after
	defer q.emptyQueueCond.L.Unlock()

	// get item
	itemData := q.jobs[0].data

	// pop from our jobs
	q.jobs = q.jobs[1:]

	return itemData
}

func (q *SimpleQueue) Push(data interface{}) string {
	// generate token first
	token := simplecrypto.ID()

	// init thi job
	job := job{
		token: token,
		data:  data,
	}

	// lock before anything else
	q.emptyQueueCond.L.Lock()
	defer q.emptyQueueCond.L.Unlock()

	// append to jobs
	q.jobs = append(q.jobs, job)

	q.emptyQueueCond.Signal()

	return token
}

func (q *SimpleQueue) IndexOf(token string) (int, bool) {
	q.emptyQueueCond.L.Lock()
	defer q.emptyQueueCond.L.Unlock()

	for i, job := range q.jobs {
		if job.token == token {
			return i, true
		}
	}

	return 0, false
}

func (q *SimpleQueue) InQueue() int {
	q.emptyQueueCond.L.Lock()
	defer q.emptyQueueCond.L.Unlock()

	return len(q.jobs)
}

package permissions

import "gitlab.com/deviosec/octp/internal/authentication"

const (
	NoAccess authentication.Permisson = iota
	Slave
	Admin
)

package teachworkerapi

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/deviosec/octp/internal/simplecrypto"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

var (
	ErrNoNounce = errors.New("could not find the nounce in the encrypted payload")
)

func (c *Client) messageWorker(ctx context.Context, method string, worker models.TeachWorker, msg Message) (*http.Response, error) {
	// setup and craft our message
	encMsg, err := EncryptMessage(msg, worker.EncryptionKey)
	if err != nil {
		return nil, err
	}

	// request to worker
	return makeRequest(ctx, fmt.Sprintf("http://%s:%s/challenges", worker.IP, TeachworkerAPIPort), method, encMsg)
}

func makeRequest(ctx context.Context, url, method string, data []byte) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	if data != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func EncryptMessage(msg Message, enckey string) ([]byte, error) {
	// marshal msg
	msgBytes, err := json.Marshal(msg)
	if err != nil {
		return nil, err
	}

	// encrypt
	encrypted, err := simplecrypto.Encrypt([]byte(enckey), msgBytes)
	if err != nil {
		return nil, err
	}

	return encrypted, nil
}

func DecryptMessage(payload []byte, encKey string) (Message, error) {
	var msg Message

	// get the body now
	body, err := simplecrypto.Decrypt(payload, []byte(encKey))
	if err != nil {
		log.Printf("decryptMessage: Failed to decode the body: %v", err)
		return msg, err
	}

	// decode body
	if err := json.Unmarshal(body, &msg); err != nil {
		log.Printf("decryptMessage: failed to unmarshal body: %v", err)
		return msg, err
	}

	return msg, nil
}

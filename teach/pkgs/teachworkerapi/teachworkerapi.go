package teachworkerapi

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

const (
	TeachworkerAPIPort = "8081"
)

// responses
type Response struct {
	Code   string
	Detail string
}

func (r Response) Error() string {
	return fmt.Sprintf("%s: %s", r.Code, r.Detail)
}

var (
	ResSuccess    = Response{"S-001", "successfully executed the request"}
	ResFailure    = Response{"F-001", "unable to forfill request"}
	ResInvalidMsg = Response{"F-002", "the message received was invalid"}
	ResDuplicate  = Response{"F-003", "challenge with ID already exists"}
)

// message
type MessageType int

const (
	TypeAdd MessageType = iota
	TypeDelete
	TypeList
	TypeStates
)

type Message struct {
	// ID that the server will use, to know the difference between
	// all of the challenges, and hence it is not a unique ID for
	// each message sent to the server
	ID   string      `json:"id"`
	Type MessageType `json:"type"`

	// only  populated when adding a challenge
	Challenge *models.Challenge `json:"challenge,omitempty"`
	Auth      *virtmodels.Auth  `json:"auth,omitempty"`
}

// client for interacting with a teachworker
type Client struct {
}

// AddChallenge will send a request to the teachWorker, regarding a new challenge to start.
// The ID provided is unique and is used to identify the challenge.
// challenge should be fully populated, with ports, flags, etc. and able to run as is.
func (c *Client) AddChallenge(ctx context.Context, ID string, worker models.TeachWorker, challenge models.Challenge, auth virtmodels.Auth) error {
	// construct message for worker
	msg := Message{
		Type:      TypeAdd,
		ID:        ID,
		Challenge: &challenge,
		Auth:      &auth,
	}

	// message our worker
	res, err := c.messageWorker(ctx, "POST", worker, msg)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	var resp Response
	if err := json.Unmarshal(body, &resp); err != nil {
		return ResFailure
	}

	if resp.Code == ResSuccess.Code {
		return nil
	}

	return resp
}

// ListChallenges sends a message to the provided teachWorker, saying it should return
// the list of currently added challenges.
func (c *Client) ListChallenges(ctx context.Context, worker models.TeachWorker) (map[string]models.Challenge, error) {
	msg := Message{
		Type: TypeList,
	}

	res, err := c.messageWorker(ctx, "GET", worker, msg)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	// decode reply
	var challenges map[string]models.Challenge
	if err := json.Unmarshal(body, &challenges); err != nil {
		var resp Response
		if err2 := json.Unmarshal(body, &resp); err2 != nil {
			return nil, err2
		}

		return nil, resp
	}

	return challenges, nil
}

// DeleteChallenge sends a message to the provided TeachWorker, indicating a specific challenge
// should be removed from the TeachWorker, which will stop the challenge from running.
func (c *Client) DeleteChallenge(ctx context.Context, ID string, worker models.TeachWorker) error {
	msg := Message{
		Type: TypeDelete,
		ID:   ID,
	}

	res, err := c.messageWorker(ctx, "DELETE", worker, msg)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	var resp Response
	if err := json.Unmarshal(body, &resp); err != nil {
		return ResFailure
	}

	if resp.Code == ResSuccess.Code {
		return nil
	}

	return resp
}

// ListChallengeStates contacts the TeachWorker and gets the current ChallengeState
// of all challenges that it is currently assigned.
func (c *Client) ListChallengeStates(ctx context.Context, worker models.TeachWorker) (map[string]models.ChallengeState, error) {
	msg := Message{
		Type: TypeStates,
	}

	res, err := c.messageWorker(ctx, "GET", worker, msg)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	// decode reply
	var challengeStates map[string]models.ChallengeState
	if err := json.Unmarshal(body, &challengeStates); err != nil {
		var resp Response
		if err2 := json.Unmarshal(body, &resp); err2 != nil {
			return nil, err2
		}

		return nil, resp
	}

	return challengeStates, nil
}

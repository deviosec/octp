package daemon

import (
	"bytes"
	"database/sql"
	_ "embed"
	"errors"
	"strings"
	"text/template"

	sq "github.com/Masterminds/squirrel"
	_ "github.com/lib/pq"
	"gitlab.com/deviosec/octp/internal/worm"
)

//go:embed schema-postgres.sql.tmpl
var schemaPostgresTmpl string

func execSchemaTemplate(name string) (string, error) {
	tmpl, err := template.New(name).Parse(schemaPostgresTmpl)
	if err != nil {
		return "", err
	}

	var buf []byte
	buffer := bytes.NewBuffer(buf)
	if err := tmpl.Execute(buffer, nil); err != nil {
		return "", err
	}

	return buffer.String(), nil
}

func (d *Daemon) InitWorm(link string, maxConnections int) (*worm.WORM, error) {
	// parse template first
	tmplData, err := execSchemaTemplate("schema-postgres.sql")
	if err != nil {
		return nil, err
	}

	var db *sql.DB
	var phf sq.PlaceholderFormat

	if strings.HasPrefix(link, "postgres://") {
		// set correct placeholder format
		phf = sq.Dollar

		db, err = sql.Open("postgres", link)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, errors.New("invalid database link provided")
	}

	if _, err = db.Exec(tmplData); err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(maxConnections)

	worm, err := worm.New(db, phf)
	if err != nil {
		return nil, err
	}

	return worm, nil
}

package daemon

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/authentication"
	"gitlab.com/deviosec/octp/internal/dns"
	"gitlab.com/deviosec/octp/internal/providers"
	"gitlab.com/deviosec/octp/internal/registryauth"
	"gitlab.com/deviosec/octp/internal/simplecrypto"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/manager"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/pkgs/permissions"
	"gitlab.com/deviosec/octp/teach/pkgs/teachworkerapi"
	"gitlab.com/deviosec/octp/teach/web"
	"gitlab.com/deviosec/octp/teach/web/api"
	"gitlab.com/deviosec/octp/teach/web/mw"
)

type Daemon struct {
	ServerIP string

	Config Config
}

func (d *Daemon) Start() error {
	ctx := context.TODO()

	// init worm
	orm, err := d.InitWorm(d.Config.DBLink, *d.Config.DBMaxConnections)
	if err != nil {
		return fmt.Errorf("failed to initialise WORM: %w", err)
	}

	// INIT TEMP IMPLEMENTATION TO MAKE STUFF WORK
	// TODO(eyJhb): NUKE THIS !
	tmpImpl := &TempImpl{
		Worm: orm,
	}

	// set initial registrylink in our config
	if err := tmpImpl.SetConfigString("registry_link", d.Config.RegistryLink); err != nil {
		return err
	}

	if d.Config.Debug {
		if err := d.enableDebug(ctx, orm); err != nil {
			return err
		}
	}

	// init our auth service
	authService := authentication.AuthService{
		Store: tmpImpl,
	}

	if d.Config.InitialAuth != "" {
		if err := d.setupInitialAuth(ctx, authService); err != nil {
			return fmt.Errorf("failed to setup initial auth: %w", err)
		}
	}

	// init provider
	provider, err := providers.New(d.Config.ProviderLink)
	if err != nil {
		return fmt.Errorf("failed to init provider: %w", err)
	}

	// init our registry token generator, if auth is enabled
	var registryTokenMaker *registryauth.Maker
	if d.Config.RegistryAuthEnable {
		// declare error, as to not override registryTokenMaker
		var err error

		registryTokenMaker, err = registryauth.New(d.Config.RegistryAuthPrivateKey, d.Config.RegistryAuthPublicKey)
		if err != nil {
			return err
		}
	}

	// init our dns provider
	var dnsProvider dns.DNSProvider
	if d.Config.DNSEnable {
		dnsProvider, err = dns.New(d.Config.DNSToken, d.Config.DNSDomain)
		if err != nil {
			return err
		}
	}

	// init our manager and start it
	log.Info().Msg("starting manager")
	manager := manager.Manager{
		Worm:               orm,
		Provider:           provider,
		WorkerAPI:          teachworkerapi.Client{},
		RegistryTokenMaker: registryTokenMaker,
		DNSEnable:          d.Config.DNSEnable,
		DNS:                dnsProvider,

		Server:            d.Config.Server,
		RegistryLink:      d.Config.RegistryLink,
		SSHKeyFingerprint: d.Config.SSHKeyFingerprint,
	}
	if err := manager.Start(); err != nil {
		return fmt.Errorf("failed to start manager: %w", err)
	}

	// init web and start web
	log.Info().Msg("starting web")
	web := web.Web{
		Worm:        orm,
		AuthService: authService,
		Manager:     &manager,
		APIServer: &api.Server{
			Worm:               orm,
			RegistryTokenMaker: registryTokenMaker,
		},

		// session
		SessionKeys: d.Config.WebSessionKeys,
		SessionsDir: d.Config.WebSessionsDir,

		// fileservice
		FileserviceDir:     d.Config.FileservicePath,
		FileserviceMaxSize: d.Config.FileserviceMaxSize,
		FileserviceMaxMem:  d.Config.FileserviceMaxMem,

		// binaries path
		BinariesPath: d.Config.BinariesPath,

		// rate limits
		RateLimitSignUp:         mw.RateLimit{Duration: time.Hour, Limit: 100, Methods: []string{http.MethodPost}},
		RateLimitSignIn:         mw.RateLimit{Duration: time.Minute, Limit: 100, Methods: []string{http.MethodPost}},
		RateLimitChallengeStart: mw.RateLimit{Duration: 10 * time.Minute, Limit: 10},
		RateLimitChallengeSolve: mw.RateLimit{Duration: time.Minute, Limit: 10},

		// bind information
		Bind: d.Config.WebBind,
		Port: d.Config.WebPort,
	}

	web.New() // sets initial values
	if err := web.Start(); err != nil {
		return fmt.Errorf("failed to start web server: %w", err)
	}

	return nil
}

func (d *Daemon) enableDebug(ctx context.Context, orm *worm.WORM) error {
	var users []models.User
	if err := orm.Select(&users).Limit(1).Run(ctx); err != nil {
		return err
	}

	if len(users) == 0 {
		// add teachworker
		workerID := "debugTeachWorker"
		encryptionKey := strings.Repeat("A", 32)

		log.Info().
			Str("workerid", workerID).
			Str("encryptionkey", encryptionKey).
			Msg("adding teachworker for debugging")

		// add teach worker
		tw := models.TeachWorker{
			IP:            "127.0.0.1",
			EncryptionKey: encryptionKey,
		}
		if err := orm.Insert(&tw).Run(ctx); err != nil {
			return err
		}

		// add user
		var user models.User
		if err := orm.Insert(&user).Run(ctx); err != nil {
			return err
		}

		log.Info().
			Str("userid", user.ID.String()).
			Msg("added user")
	}

	return nil
}

func (d *Daemon) setupInitialAuth(ctx context.Context, authService authentication.AuthService) error {
	// split into id and token
	authIDToken := strings.Split(d.Config.InitialAuth, ",")

	if len(authIDToken) != 2 {
		return errors.New("the InitialAuth is not provided as `id,token` format")
	}

	authID, authToken := authIDToken[0], authIDToken[1]

	// check if it already exists
	_, err := authService.Validate(ctx, authToken)
	if err != nil && err != authentication.ErrInvalidToken {
		return err
	}

	if err == authentication.ErrInvalidToken {
		log.Info().
			Str("authid", authID).
			Str("authtoken", authToken).
			Msg("adding initialauth")

		hashedToken, err := simplecrypto.HashToken(authToken)
		if err != nil {
			return err
		}

		if err := authService.Store.AddToken(ctx, hashedToken, authentication.TokenInfo{ID: authID, Perm: permissions.Admin}); err != nil {
			return err
		}
	}

	return nil
}

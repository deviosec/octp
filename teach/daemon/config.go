package daemon

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/deviosec/octp/internal/fileserver"
)

var (
	ErrAuthCertsNotSpecified = errors.New("registry auth is enabled, but the key files are not specified")
)

type Config struct {
	// the base path for the server, which follows the format of
	// http(s)://<domain>/<path> e.g. https://play.deviosec.dk
	Server string `mapstructure:"server"`

	// the format is as follows
	// [username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
	// prefix with either postgres:// or mysql://
	// postgres has `sslmode=true` as default param
	DBLink string `mapstructure:"db_link"`
	// DBMaxConnections defaults to 100
	DBMaxConnections *int `mapstructure:"db_max_connections"`

	// username:password@address:port
	// Can be used together with RegistryPrivateAddress.
	// If RegistryPrivateAddress is set, but RegistryLink
	// is unset, then RegistryLink will be set to the
	// RegistryPrivateAddress
	RegistryLink string `mapstructure:"registry_link"`

	// RegistryAuthPrivateKey and RegistryAuthPublicKey is used,
	// to setup authentication for the Docker Registry.
	// These should be point to a file, containing either the private key
	// or the public key (both are required).
	//
	// The certs can be generated using openssl
	// `openssl req -x509 -days 365 -nodes -newkey rsa:4096 -keyout registry-auth-key.pem -out registry-auth-cert.pem`
	//
	// Remember to configure the registry to use this, if you're managing
	// the registry outside of OCTP.
	// https://docs.docker.com/registry/configuration/#token
	// Service should be `Authentication` and issuer should be `OCTP`.
	RegistryAuthPrivateKey string `mapstructure:"registry_auth_private_key"`
	RegistryAuthPublicKey  string `mapstructure:"registry_auth_public_key"`

	// RegistryAuthEnable  authentication on the registry,
	// if RegistryAuthPrivateKey and RegistryAuthPublicKey is provided
	// then those are used. Otherwise they will be generated.
	// Keep in mind, that if a external registry is run with authentication
	// then this should be enabled, but EnableRegistry should be disabled.
	RegistryAuthEnable bool `mapstructure:"registry_auth_enable"`

	// TODO(eyJhb) need to add some kind of domain
	// which can be used instead of the IP.
	// ie. if we use the EnableRegistry, combined
	// with a domain, then we want to use that domain
	// and not just the public IP we pick up.
	// Also we do not want to auto get a public IP,
	// if we have specified this.
	// It could be named, host, domain, etc...

	// WebSessionKeys is a list of encryption keys
	// used to store the sessions. A list can be
	// specified for key rotation, where the first
	// key will be tried first, then the next key
	// and so on. It will then re-encrypt the session
	// using the first key.
	//
	// Note: all keys must be provided as hex encoded
	// strings.
	WebSessionKeys []string `mapstructure:"web_session_keys"`

	// WebSessionsDir specifies where the session data
	// will be stored on the local disk, if nothing
	// is specified, the default config dir will
	// be used.
	WebSessionsDir string `mapstructure:"web_sessions_dir"`

	// WebBind optionally provides which IP to bind
	// webserver on, if left blank it will
	// bind to any IP.
	//
	// Note: ignored if WebAutoCert is true
	WebBind string `mapstructure:"bind"`

	// WebPort optionally specifies the port to bind
	// the webserver to. If left blank it will default
	// to port 8080. if UseHTTPS is true, it will
	// default to port 443.
	//
	// Note: Web WebAutoCert is true, UseHTTPS is also
	// set to true.
	WebPort int `mapstructure:"port"`

	// provider things
	ProviderLink      string `mapstructure:"provider_link"`
	SSHKeyFingerprint string `mapstructure:"ssh_key_fingerprint"`

	// for API + file server
	InitialAuth string `mapstructure:"initial_auth"`

	// path for fileservice, which is where all files
	// will be stored
	FileservicePath string `mapstructure:"fileservice_path"`

	// FileserviceMaxSize is the max size of files, that can
	// be uploaded to the service. Set to `-1` to disable
	// the fileservice.
	FileserviceMaxSize int64 `mapstructure:"fileservice_max_size"`

	// FileserviceMaxMem specifies the maximum memory of
	// the file that can be kept in memory, before it gets
	// written to disk.
	FileserviceMaxMem int64 `mapstructure:"fileservice_max_mem"`

	// BinariesDir specifies the location of the binaries,
	// that will be served to the workers. Keep in mind
	// these binaries are public available, so do not keep
	// secrets in them
	BinariesPath string `mapstructure:"binaries_path"`

	// DNS settings, for using hostnames instead of IP
	DNSEnable bool   `mapstructure:"dns_enable"`
	DNSDomain string `mapstructure:"dns_domain"`
	DNSToken  string `mapstructure:"dns_token"`

	// debug
	Debug bool `mapstructure:"debug"`
}

func (c *Config) ValidateAndDefaults() error {
	// get default user config dir
	configDir, err := os.UserConfigDir()
	if err != nil {
		return err
	}

	if c.Server == "" {
		return errors.New("server is not set")
	}

	if c.RegistryLink == "" {
		return errors.New("registry_link is not set")
	}

	if c.DBLink == "" {
		return errors.New("db_link is not set")
	}

	if c.RegistryAuthEnable {
		if c.RegistryAuthPrivateKey == "" || c.RegistryAuthPublicKey == "" {
			return ErrAuthCertsNotSpecified
		}

		// ensure auth certs exists
		if _, err := os.Stat(c.RegistryAuthPrivateKey); os.IsNotExist(err) {
			return fmt.Errorf("RegistryAuthPrivateKey does not exists: %w", err)
		}

		if _, err := os.Stat(c.RegistryAuthPublicKey); os.IsNotExist(err) {
			return fmt.Errorf("RegistryAuthPublicKey does not exists: %w", err)
		}
	}

	// web sessions dir
	if c.WebSessionsDir == "" {
		c.WebSessionsDir = fmt.Sprintf("%s/%s", configDir, "sessions")
	}

	// fileserver defaults
	if c.FileservicePath == "" {
		c.WebSessionsDir = fmt.Sprintf("%s/%s", configDir, "teach-fileserver")
	}
	if c.FileserviceMaxSize == 0 {
		c.FileserviceMaxSize = fileserver.DefaultMaxSize
	}
	if c.FileserviceMaxMem == 0 {
		c.FileserviceMaxMem = fileserver.DefaultMaxMemSize
	}

	// database
	if c.DBMaxConnections == nil {
		DBMaxConnectionsDefaultValue := 100
		c.DBMaxConnections = &DBMaxConnectionsDefaultValue
	}

	// webserver port
	if c.WebPort == 0 {
		c.WebPort = 8080
	}

	if c.BinariesPath == "" {
		return errors.New("binaries_path is not set")
	} else {
		if _, err := os.Stat(c.BinariesPath); os.IsNotExist(err) {
			return fmt.Errorf("binaries_path does not exists: %w", err)
		}
	}

	return nil
}

package daemon

import (
	"context"
	"database/sql"
	"errors"

	"gitlab.com/deviosec/octp/internal/authentication"
	"gitlab.com/deviosec/octp/internal/worm"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

type TempImpl struct {
	Worm *worm.WORM
}

type AuthToken struct {
	Token string `sql:",primarykey"`
	authentication.TokenInfo
}

func (ti *TempImpl) GetConfigString(key string) (string, bool, error) {
	ctx := context.TODO()

	var confValue models.Config
	if err := ti.Worm.Select(&confValue).Where(worm.Eq{"key": key}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", false, nil
		}
		return "", false, err
	}

	return confValue.Value, true, nil
}

func (ti *TempImpl) SetConfigString(key string, value string) error {
	ctx := context.TODO()

	confValue := models.Config{
		Key:   key,
		Value: value,
	}

	// check if currently exists
	exists := true
	if err := ti.Worm.Select(&models.Config{}).Where(worm.Eq{"key": confValue.Key}).Run(ctx); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			exists = false
		} else {
			return err
		}
	}

	if exists {
		if err := ti.Worm.Update(&confValue).Where(worm.Eq{"key": confValue.Key}).Run(ctx); err != nil {
			return err
		}
	} else {
		if err := ti.Worm.Insert(&confValue).Run(ctx); err != nil {
			return err
		}
	}

	return nil
}

func (ti *TempImpl) GetTokens(ctx context.Context) (map[string]authentication.TokenInfo, error) {
	// var tokens map[string]authentication.TokenInfo
	var sqltokens []AuthToken
	// var sqltokens map[string]AuthToken
	if err := ti.Worm.Select(&sqltokens).Run(ctx); err != nil {
		return map[string]authentication.TokenInfo{}, err
	}

	tokens := make(map[string]authentication.TokenInfo)
	for _, ti := range sqltokens {
		tokens[ti.Token] = ti.TokenInfo
	}

	return tokens, nil
}

func (ti *TempImpl) AddToken(ctx context.Context, token string, tokenInfo authentication.TokenInfo) error {
	tokenToInsert := AuthToken{
		Token:     token,
		TokenInfo: tokenInfo,
	}
	if err := ti.Worm.Insert(&tokenToInsert).Run(ctx); err != nil {
		return err
	}

	return nil
}

func (ti *TempImpl) RemoveToken(ctx context.Context, token string) error {
	if err := ti.Worm.Delete(&AuthToken{}).Where(worm.Eq{"token": token}).Run(ctx); err != nil {
		return err
	}

	return nil
}

package cmd

import (
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/deviosec/octp/client/logic"
	"gitlab.com/deviosec/octp/internal/virtual"
)

var (
	flagEvent = &cli.StringFlag{
		Name:    "event",
		Aliases: []string{"e"},
		Usage:   "specifies which event to use",
	}
	flagChallengeNoBuild = &cli.BoolFlag{
		Name:  "no-build",
		Usage: "if set, it will only load challenge from file",
	}
	flagChallengeNoRegistry = &cli.BoolFlag{
		Name:  "no-registry",
		Usage: "if set, challenge will not be uploaded to registry",
	}
	flagChallengeNoFiles = &cli.BoolFlag{
		Name:  "no-files",
		Usage: "if set, challenge files will not be uploaded to server",
	}
	flagChallengeNoUpload = &cli.BoolFlag{
		Name:  "no-upload",
		Usage: "if set, challenge will not be uploaded to server",
	}
	flagPublicAddress = &cli.StringFlag{
		Name:  "public-address",
		Value: "127.0.0.1",
		Usage: "this will override the default public address when starting a challenge",
	}
)

type cmd struct {
	logic logic.Logic
	store logic.Store
}

func New() *cmd {
	s := newStore()
	return &cmd{
		logic: logic.Logic{
			Store:   s,
			VirtMan: virtual.New(),
		},

		store: s,
	}
}

func (ac *cmd) Execute() error {

	app := &cli.App{
		Flags: []cli.Flag{},
		Commands: []*cli.Command{

			// server commands
			&cli.Command{
				Name: "server",
				Flags: []cli.Flag{
					flagEvent,
				},
				Subcommands: []*cli.Command{
					&cli.Command{
						Name:    "upload",
						Aliases: []string{"u"},
						Flags: []cli.Flag{
							flagChallengeNoBuild,
							flagChallengeNoRegistry,
							flagChallengeNoFiles,
							flagChallengeNoUpload,
						},
						ArgsUsage: "[challenges set]",
						Usage:     "upload all challenges in the specified sets to the server, and create them",
						Action:    ac.cmdServerUploadChallenges,
					},
				},
			},

			// challenge commands
			&cli.Command{
				Name:   "challenges",
				Action: ac.cmdChallenges,
			},
			&cli.Command{
				Name: "challenge",
				Subcommands: []*cli.Command{
					&cli.Command{
						Name:      "list",
						Aliases:   []string{"l"},
						ArgsUsage: "[challenges sets...]",
						Usage:     "list all challenges in the specified sets",
						Action:    ac.cmdChallenges,
					},
					&cli.Command{
						Name:      "validate",
						Aliases:   []string{"val"},
						ArgsUsage: "[challenges sets...]",
						Usage:     "validate challenges configuration in the specified sets",
						Action:    ac.cmdChallengeValidate,
					},
					&cli.Command{
						Name:      "make",
						Aliases:   []string{"build"},
						ArgsUsage: "[challenges sets...]",
						Usage:     "generates files and builds challenges for the specifed sets",
						Action:    ac.cmdChallengeMake,
					},
					&cli.Command{
						Name:      "stop",
						ArgsUsage: "[challenges id...]",
						Usage:     "stop the local instances of challenges started using the client",
						Action:    ac.cmdChallengeStop,
					},
					&cli.Command{
						Name:    "start",
						Aliases: []string{"run"},
						Flags: []cli.Flag{
							flagPublicAddress,
						},
						ArgsUsage: "[challenges id...]",
						Usage:     "Run multiple challenges locally on this computer",
						Action:    ac.cmdChallengeRun,
					},
				},
			},

			// teach commands
		},
	}

	return app.Run(os.Args)
	// // define root command
	// root := cli.Command{
	// 	Name:     "octp",
	// 	LongDesc: "This commandline is used, to manage various parts of OCTP",
	// }

	// // add commands to root
	// root.AddCommands(
	// 	// ac.cmdEvent(),
	// 	// ac.cmdEvents(),
	// 	// cmdPod(),
	// 	// addCacheFlag(cmdPods()),
	// 	ac.cmdChallenge(),
	// 	ac.cmdChallenges(),
	// 	// ac.cmdConfig(),
	// )

	// // execute root with args[1] as start (0 being the program name itself)
	// return root.Execute(1)
}

package cmd

import (
	"errors"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	"gitlab.com/deviosec/octp/client/api"
	"gitlab.com/deviosec/octp/client/logic"
	"gitlab.com/deviosec/octp/client/pkgs/challenge"
	"gitlab.com/deviosec/octp/internal/registryauth"
	"gitlab.com/deviosec/octp/internal/virtual"
	"gitlab.com/deviosec/octp/internal/virtual/models"
)

var (
	ErrEventNotExists = errors.New("the selected event does not exists")
)

func (ac *cmd) cmdServerUploadChallenges(c *cli.Context) error {
	if c.Args().Len() != 1 {
		return errors.New("please specify exactly one challenge set to upload")
	}

	challengeSet := c.Args().First()

	// get challenge sets
	challSets := ac.getChallengeSets([]string{challengeSet})

	if len(challSets) == 0 {
		return errors.New("challenge set does not exists")
	}

	// get event
	event, err := ac.getEvent(c)
	if err != nil {
		return err
	}

	fmt.Printf("Uploading challengeset: %s\nServer: %s\n", challengeSet, event.Server)
	fmt.Println("Waiting 5 sec before uploading")
	time.Sleep(5 * time.Second)

	// init api client
	apiClient := api.Client{
		APIToken: event.Auth.Token,
		Server:   event.Server,
	}

	registryLinkTmp, err := apiClient.GetConfig(c.Context, "registry_link")
	if err != nil {
		return err
	}

	registryLink, couldCast := registryLinkTmp.(string)
	if !couldCast {
		return errors.New("unable to cast registry link to string")
	}

	// setup upload options
	uploadOptions := uploadChallengeOptions{
		apiClient:    apiClient,
		event:        event,
		registryLink: registryLink,

		makeOptions: ac.flagBuildOptions(c),

		noRegistry: flagChallengeNoRegistry.Get(c),
		noFiles:    flagChallengeNoFiles.Get(c),
		noUpload:   flagChallengeNoUpload.Get(c),
	}

	// loop over all challenge sets
	for _, challSet := range challSets {
		// get challenges for that challenge set
		challs, err := ac.logic.GetChallenges(challSet)
		if err != nil {
			return err
		}

		// loop over all challenges and add them
		chalErrs := make(map[string]error)
		for _, chall := range challs {
			uploadOptions.challenge = chall
			fmt.Println(chall.Challenge.ID)

			if err := ac.uploadChallenge(c, uploadOptions); err != nil {
				log.Error().Err(err).Msg("failed to upload challenge")
				chalErrs[chall.Challenge.ID] = err
			}
		}

		if len(chalErrs) > 0 {
			fmt.Println("Following challenges failed")
			for chalName, chalError := range chalErrs {
				fmt.Printf("- %s: %v\n", chalName, chalError)
			}

		}
	}

	return nil
}

// options when uploading a challenge
type uploadChallengeOptions struct {
	apiClient    api.Client
	event        logic.Event
	registryLink string
	challenge    challenge.Challenge

	makeOptions challenge.MakeOptions

	noRegistry bool
	noFiles    bool
	noUpload   bool
}

func (ac *cmd) uploadChallenge(cli *cli.Context, opts uploadChallengeOptions) error {
	// define our context
	ctx := cli.Context

	// set the challenge to be active, so it can be used on the server
	c := opts.challenge
	c.Challenge.Active = true

	// steps for uploading a challenge
	// step 1. ensure that the challenge has been build, and is ready to upload
	// step 2. ensure that the images to the registry have been pushed
	// step 3. ensure that the challenge files have been pushed to the server
	// step 4. add the actual challenge

	// step 1
	log.Info().Str("challenge_id", c.Challenge.ID).Msg("uploadChallenge: step 1")

	if opts.makeOptions.BuildVirtuals {
		if err := c.Validate(challenge.DefaultValidatePreMakeOptions); err != nil {
			return err
		}
	} else {
		if err := c.Validate(challenge.DefaultValidatePreMakeNoBuildOptions); err != nil {
			return err
		}
	}

	// do not build the image, as it will not be uploaded
	if !opts.noRegistry {
		if err := c.Make(ctx, opts.makeOptions); err != nil {
			return err
		}
	}

	// step 2
	log.Info().Str("challenge_id", c.Challenge.ID).Msg("uploadChallenge: step 2")
	for virtI, virt := range c.Challenge.Lab.Virtuals {
		name := virt.Name
		if name == "" {
			name = "main"
		}

		virtImageNameNoTag := c.VirtualImageNameNoTag(name)

		// do not actually upload the image
		if !opts.noRegistry {
			// generate auth token for all images
			tokenID := fmt.Sprintf("cli-admin-upload-%s", c.Challenge.ID)
			tokenActions := []string{registryauth.ActionPush, registryauth.ActionPull}
			registryAuthToken, err := opts.apiClient.RegistryTokenCreate(ctx, tokenID, tokenActions, []string{virtImageNameNoTag}, time.Hour)
			if err != nil {
				return err
			}

			if err := ac.logic.VirtMan.PushImage(ctx, virtual.BackendDocker, c.VirtualImageName(name), opts.registryLink, models.Auth{Token: registryAuthToken}); err != nil {
				return fmt.Errorf("failed to push %s got error %w", name, err)
			}
		}

		// set the final image name
		c.Challenge.Lab.Virtuals[virtI].Image = virtImageNameNoTag
	}

	// step 3
	if !opts.noFiles {
		log.Info().Str("challenge_id", c.Challenge.ID).Msg("uploadChallenge: step 3")
		files, err := opts.apiClient.Files(ctx)
		if err != nil {
			return err
		}

		//// check if file already exists
		for fileIndex, file := range c.Challenge.Files {
			// skip if link exists
			if file.Link != "" {
				continue
			}

			// get the last part of the filename, and use that instead, keep the original
			// originalFilename := file.Filename
			// file.Filename = file.Filename[strings.LastIndex(file.Filename, "/")+1:]

			// loop over each file on the server
			// break if it is found, and set equal existingFileID
			// to indicate a file already exists
			var existingFileID string
		fileServerLoop:
			for fileServerID, fileServer := range files {
				for _, fileHash := range fileServer.Hashes {
					if file.Sha256 == fileHash {
						existingFileID = fileServerID
						break fileServerLoop
					}
				}
			}

			// use the existing fileid if any
			if existingFileID != "" {
				// TODO(eyJhb): maybe not hardcode this?
				c.Challenge.Files[fileIndex].Link = fmt.Sprintf("%s/files/view/%s", opts.event.Server, existingFileID)
				continue
			}

			// get sha256
			sha256, err := c.CalculateSHA256(file)
			if err != nil {
				return err
			}
			file.Sha256 = sha256

			f, err := c.File(file)
			if err != nil {
				return err
			}

			fileID, err := opts.apiClient.FileAdd(ctx, file, f)
			if err != nil {
				return err
			}

			c.Challenge.Files[fileIndex].Link = fmt.Sprintf("%s/files/view/%s", opts.event.Server, fileID)
		}
	}

	// step 4
	if !opts.noUpload {
		log.Info().Str("challenge_id", c.Challenge.ID).Msg("uploadChallenge: step 4")

		if err := opts.apiClient.ChallengeAdd(ctx, c.Challenge); err != nil {
			return err
		}
	}

	return nil
}

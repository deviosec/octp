package cmd

import (
	"fmt"

	"github.com/urfave/cli/v2"
	"gitlab.com/deviosec/octp/client/pkgs/challenge"
	"gitlab.com/deviosec/octp/internal/virtual"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
	"gitlab.com/deviosec/octp/teach/manager"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

func (ac *cmd) cmdChallenges(c *cli.Context) error {
	return ac.challengesList(c.Args().Slice())
}

func (ac *cmd) cmdChallengeValidate(c *cli.Context) error {
	return ac.challengesValidate(c.Args().Slice())
}

func (ac *cmd) cmdChallengeMake(c *cli.Context) error {
	// get challenge sets
	challSets := ac.getChallengeSets(c.Args().Slice())

	// validate challenges
	ac.challengesValidate(challSets)

	// stores all build errors
	errs := make(map[string]string)

	// loop challenge sets to get challenge set
	for _, challSet := range challSets {
		fmt.Printf("\t%s\n", challSet)

		// get all challenges
		challenges, err := ac.logic.GetChallenges(challSet)
		if err != nil {
			fmt.Printf("Failed to get challenges for '%s'\n", challSet)
			continue
		}

		// loop all challenges
		for _, chal := range challenges {
			fmt.Printf("\t\tBuilding %s (%s)\n", chal.Challenge.Title, chal.Challenge.ID)

			// run the appropiate actions
			if err := chal.Make(c.Context, ac.flagBuildOptions(c)); err != nil {
				errs[chal.Challenge.ID] = err.Error()
				fmt.Printf("\t\t\t- Failed to build - %v\n", err)
			}

		}
	}

	if len(errs) > 0 {
		for chalID, err := range errs {
			fmt.Printf("Error on challenge '%s' with error - %s\n", chalID, err)
		}
	}

	return nil
}

func (ac *cmd) cmdChallengeStop(c *cli.Context) error {
	// get all challenge sets
	challengeSets, err := ac.store.GetChallengeSets()
	if err != nil {
		return err
	}

	// get all challenges
	challenges := make(map[string]*challenge.Challenge)
	for challSet := range challengeSets {
		setChallenges, err := ac.logic.GetChallenges(challSet)
		if err != nil {
			return err
		}

		for i, chal := range setChallenges {
			if challenges[chal.Challenge.ID] != nil {
				return fmt.Errorf("the challenge '%s' exists in multiple datasets, or is duplicated in one of the datasets", chal.Challenge.ID)
			}

			challenges[chal.Challenge.ID] = &setChallenges[i]
		}
	}

	virtman := virtual.New()
	for _, challengeID := range c.Args().Slice() {
		if challenges[challengeID] == nil {
			return fmt.Errorf("could not find the challenge with the given id '%s'", challengeID)
		}

		fmt.Printf("Found challenge '%s', stopping it\n", challengeID)

		chall := challenges[challengeID].Challenge
		chall.Lab.Name = "testlab-" + challengeID

		if err := virtman.DestroyLab(c.Context, &chall.Lab); err != nil {
			return err
		}
	}

	return nil
}

// TODO(eyJhb) does not clean up networking correctly
func (ac *cmd) cmdChallengeRun(c *cli.Context) error {
	// get all challenge sets
	challengeSets, err := ac.store.GetChallengeSets()
	if err != nil {
		return err
	}

	// get all challenges
	challenges := make(map[string]*challenge.Challenge)
	for challSet := range challengeSets {
		setChallenges, err := ac.logic.GetChallenges(challSet)
		if err != nil {
			return err
		}

		for i, chal := range setChallenges {
			if challenges[chal.Challenge.ID] != nil {
				return fmt.Errorf("the challenge '%s' exists in multiple datasets, or is duplicated in one of the datasets", chal.Challenge.ID)
			}

			challenges[chal.Challenge.ID] = &setChallenges[i]
		}
	}

	ctx := c.Context
	virtman := virtual.New()
	for _, challengeID := range c.Args().Slice() {
		if challenges[challengeID] == nil {
			return fmt.Errorf("could not find the challenge with the given id '%s'", challengeID)
		}

		fmt.Printf("Found challenge '%s', running make on it\n", challengeID)

		if err := challenges[challengeID].Make(ctx, ac.flagBuildOptions(c)); err != nil {
			return err
		}

		chall := challenges[challengeID]

		// default virtual
		for i, virt := range chall.Challenge.Lab.Virtuals {
			chall.Challenge.Lab.Virtuals[i] = virtmodels.SetDefaultsVirtual(virt)
		}

		// make a mork teachworker
		worker := models.TeachWorker{
			IP: flagPublicAddress.Get(c),
		}

		// mock find available ports
		availblePorts, err := manager.FindAvailablePorts(worker, chall.Challenge)
		if err != nil {
			return err
		}

		// populate challenge
		manager.PopulateChallenge(worker, &chall.Challenge, availblePorts)

		// set image and name if not populated
		for virtI, virt := range chall.Challenge.Lab.Virtuals {
			if virt.Name == "" {
				virt.Name = "main"
				chall.Challenge.Lab.Virtuals[virtI].Name = virt.Name
			}

			// set correct image
			chall.Challenge.Lab.Virtuals[virtI].Image = chall.VirtualImageName(virt.Name)
		}

		chall.Challenge.Lab.Name = "testlab-" + challengeID
		if err := virtman.CreateLab(ctx, &chall.Challenge.Lab); err != nil {
			return err
		}
		if err := virtman.StartLab(ctx, chall.Challenge.Lab); err != nil {
			return err
		}

		for _, virt := range chall.Challenge.Lab.Virtuals {
			for portName, port := range virt.Ports {
				fmt.Printf("virt: %s port: %s - %s:%d\n", virt.Name, portName, worker.IP, port.Host)
			}
		}
	}

	return nil
}

func (ac *cmd) challengesList(challengeSets []string) error {
	challSets := ac.getChallengeSets(challengeSets)

	for _, challSet := range challSets {
		challenges, err := ac.logic.GetChallenges(challSet)
		if err != nil {
			fmt.Printf("\tFailed to get challenges for '%s', %v\n", challSet, err)
			continue
		}

		fmt.Printf("%s:\n", challSet)
		for _, chal := range challenges {

			fmt.Printf("\t- %s (%s)\n", chal.Challenge.Title, chal.Challenge.ID)
		}
	}

	return nil
}

// gets the challenge sets specified from the command line
func (ac *cmd) getChallengeSets(challSets []string) []string {
	// if we have one ore more args, then only list them
	// else list them all
	// if no argument is specified, then list all
	if len(challSets) == 0 {
		storeChallSets, _ := ac.store.GetChallengeSets()

		for challSet := range storeChallSets {
			challSets = append(challSets, challSet)
		}
	}

	return challSets
}

func (ac *cmd) challengesValidate(challengeSets []string) error {
	challSets := ac.getChallengeSets(challengeSets)

	fmt.Println("Validation:")
	for _, challSet := range challSets {
		fmt.Printf("\t%s\n", challSet)

		challenges, err := ac.logic.GetChallenges(challSet)
		if err != nil {
			fmt.Printf("Failed to get challenges for '%s'\n", challSet)
			continue
		}

		for _, chal := range challenges {
			fmt.Printf("\t\t- %s (%s)\n", chal.Challenge.Title, chal.Challenge.ID)

			if err := chal.Validate(challenge.DefaultValidatePreMakeOptions); err != nil {
				fmt.Println(err)
			}
		}

	}

	return nil
}

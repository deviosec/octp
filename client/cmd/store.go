package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/deviosec/octp/client/logic"
	"gopkg.in/yaml.v2"
)

// no mutex, as this is a single person cli
type store struct {
	ChallengeSets   map[string]string      `mapstructure:"challenge_sets"`
	Events          map[string]logic.Event `mapstructure:"events"`
	Providers       map[string]string      `mapstructure:"providers"`
	DefaultProvider string                 `mapstructure:"default_provider"`

	ServerTemplate    string `mapstructure:"server_template"`
	WorkerTemplate    string `mapstructure:"worker_template"`
	SSHKeyFingerprint string `mapstructure:"ssh_key_fingerprint"`
}

func newStore() *store {
	s := store{
		ChallengeSets: make(map[string]string),
		Providers:     make(map[string]string),
	}
	confStore, err := s.load()
	if err != nil {
		log.Printf("newStore: failed to load store: %v", err)
	}
	return &confStore
}

func (s *store) load() (store, error) {
	// get user config dir
	configDir, err := os.UserConfigDir()
	if err != nil {
		return store{}, err
	}

	viper.SetConfigName("client")
	viper.AddConfigPath(fmt.Sprintf("%s/octp", configDir))
	viper.SetEnvPrefix("octp_client")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return store{}, err
	}

	var conf store
	if err := viper.Unmarshal(&conf); err != nil {
		return store{}, err
	}

	return conf, nil
}

func (s *store) save() error {
	// yaml our configuration
	storeBytes, err := yaml.Marshal(s)
	if err != nil {
		return err
	}

	// get config file path
	confileFilePath := viper.ConfigFileUsed()

	// write it out
	if err := ioutil.WriteFile(confileFilePath, storeBytes, 0750); err != nil {
		return err
	}

	return nil
}

// cloud providers (map[name]token)
func (s *store) GetProviders() (map[string]string, error) {
	return s.Providers, nil
}

func (s *store) AddProvider(name, token string) error {
	defer s.save()
	s.Providers[name] = token
	return nil
}

func (s *store) GetDefaultProvider() (string, error) {
	return s.DefaultProvider, nil
}

func (s *store) SetDefaultProvider(name string) error {
	defer s.save()
	s.DefaultProvider = name
	return nil
}

func (s *store) DeleteProvider(name string) error {
	defer s.save()
	delete(s.Providers, name)
	return nil
}

// events
func (s *store) GetEvents() (map[string]logic.Event, error) {
	return s.Events, nil
}

func (s *store) AddEvent(name string, event logic.Event) error {
	defer s.save()
	s.Events[name] = event
	return nil
}

func (s *store) DeleteEvent(name string) error {
	defer s.save()
	delete(s.Events, name)
	return nil
}

// challenge sets (map[name]path)
func (s *store) GetChallengeSets() (map[string]string, error) {
	return s.ChallengeSets, nil
}

func (s *store) AddChallengeSet(name, path string) error {
	defer s.save()
	s.ChallengeSets[name] = path
	return nil
}

func (s *store) DeleteChallengeSet(name string) error {
	defer s.save()
	delete(s.ChallengeSets, name)
	return nil
}

// server template (local version)
func (s *store) GetServerTemplate() (string, bool, error) {
	return s.ServerTemplate, s.ServerTemplate != "", nil
}

func (s *store) SetServerTemplate(template string) error {
	defer s.save()
	s.ServerTemplate = template
	return nil
}

// worker template (local version)
func (s *store) GetWorkerTemplate() (string, bool, error) {
	return s.WorkerTemplate, s.WorkerTemplate != "", nil
}

func (s *store) SetWorkerTemplate(template string) error {
	defer s.save()
	s.WorkerTemplate = template
	return nil
}

// ssh key fingerpring
func (s *store) GetSSHKeyFingerprint() (string, error) {
	return s.SSHKeyFingerprint, nil
}

func (s *store) SetSSHKeyFingerprint(fingerprint string) error {
	defer s.save()
	s.SSHKeyFingerprint = fingerprint
	return nil
}

package cmd

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/deviosec/octp/client/logic"
	"gitlab.com/deviosec/octp/client/pkgs/challenge"
)

func (ac *cmd) getEvent(c *cli.Context) (logic.Event, error) {
	eventName := flagEvent.Get(c)
	storeEvents, err := ac.store.GetEvents()
	if err != nil {
		return logic.Event{}, err
	}

	for storeEventName, storeEvent := range storeEvents {
		if eventName == storeEventName {
			return storeEvent, nil
		}
	}

	return logic.Event{}, ErrEventNotExists

}

func (ac *cmd) flagBuildOptions(c *cli.Context) challenge.MakeOptions {
	noBuild := flagChallengeNoBuild.Get(c)

	if noBuild {
		return challenge.MakeOptions{
			LoadImageFromFile:        true,
			RequireLoadImageFromFile: true,
		}
	}

	return challenge.DefaultMakeOptions
}

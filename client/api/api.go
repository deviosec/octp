package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/authentication"
	"gitlab.com/deviosec/octp/internal/fileserver/catalog"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/web/api"
)

var (
	ErrChallenges         = errors.New("could not get challenges")
	ErrRegistryInfo       = errors.New("could not get registry information")
	ErrRegistryToken      = errors.New("could not create registry token")
	ErrChallengeCreate    = errors.New("failed to create challenge")
	ErrChallengeDelete    = errors.New("failed to delete challenge")
	ErrChallengeRequireID = errors.New("no id specified for challenge")
	ErrFileDeleteFailed   = errors.New("failed to delete file")
	ErrFiles              = errors.New("failed to get files")
	ErrConfigNotFound     = errors.New("config key not found")
)

type requestMethod string

const (
	methodGet    requestMethod = http.MethodGet
	methodPost   requestMethod = http.MethodPost
	methodPut    requestMethod = http.MethodPut
	methodDelete requestMethod = http.MethodDelete

	endpointConfig        = "api/configs/%s"
	endpointRegistryToken = "api/registry/token"
	endpointChallenges    = "api/challenges"
	endpointChallenge     = "api/challenges/%s"
	endpointFiles         = "files"
	endpointFile          = "files/%s"
)

type Client struct {
	HTTPClient http.Client
	APIToken   string

	// full path to server, ie.
	// https://play.deviosec.dk:8080
	Server string
}

// tmp response KEEP IN SYNC WITH `teach/web/api/response.go`
type APIError struct {
	Msg string `json:"msg"`
}

type APIRes struct {
	Collection json.RawMessage `json:"collection"`
	Error      APIError        `json:"error"`
	Code       int             `json:"code"`
}

// makeRequest will make a request to the given endpoint, and return the response and an error.
// ensure that on non-nil errors, that the response body is read to EOF and closed according to -
// https://pkg.go.dev/net/http#Client.Do
func (c *Client) makeRequest(ctx context.Context, endpoint string, method requestMethod, body io.Reader) (APIRes, error) {
	url := fmt.Sprintf("%s/%s", c.Server, endpoint)
	req, err := http.NewRequestWithContext(ctx, string(method), url, body)
	if err != nil {
		return APIRes{}, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.APIToken))

	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return APIRes{}, err
	}
	defer res.Body.Close()

	// check if the request was unauthorized
	if res.StatusCode == http.StatusUnauthorized {
		return APIRes{}, authentication.ErrInvalidToken
	}

	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return APIRes{}, err
	}

	if len(resBody) > 0 {
		// decode json
		var apires APIRes
		if err := json.Unmarshal(resBody, &apires); err != nil {
			if err == io.EOF {
				return APIRes{
					Code: res.StatusCode,
				}, nil
			}
			log.Error().Err(err).Str("body", string(resBody)).Msg("failed to decode json")
			return APIRes{}, fmt.Errorf("makeRequest: failed to decode json: %w", err)
		}

		return apires, nil
	}

	return APIRes{Code: res.StatusCode}, nil
}

func (c *Client) GetConfig(ctx context.Context, name string) (interface{}, error) {
	res, err := c.makeRequest(ctx, fmt.Sprintf(endpointConfig, name), methodGet, nil)
	if err != nil {
		return nil, err
	}

	if res.Code == http.StatusNotFound {
		return nil, ErrConfigNotFound
	}

	if res.Code != http.StatusOK {
		log.Error().Interface("body", res).Msg("failed getting config entry")
		return nil, errors.New("failed to get config")
	}

	type intermediateRes struct {
		Key   string
		Value interface{}
	}

	var intRes intermediateRes
	if err := json.Unmarshal(res.Collection, &intRes); err != nil {
		return nil, fmt.Errorf("failed to unmarshal config: %w", err)
	}

	return intRes.Value, nil
}

func (c *Client) RegistryTokenCreate(ctx context.Context, ID string, actions []string, images []string, exp time.Duration) (string, error) {
	req := api.ReqRegistryToken{
		ID:      ID,
		Actions: actions,
		Images:  images,
		Exp:     exp,
	}

	data, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	res, err := c.makeRequest(ctx, endpointRegistryToken, methodPost, bytes.NewReader(data))
	if err != nil {
		return "", err
	}

	if res.Code != http.StatusCreated {
		log.Error().Interface("res", res).Msg("failed create registry token")
		return "", ErrRegistryToken
	}

	var registryToken string
	if err := json.Unmarshal(res.Collection, &registryToken); err != nil {
		return "", err
	}

	return registryToken, nil
}

func (c *Client) Challenges(ctx context.Context) ([]models.Challenge, error) {
	res, err := c.makeRequest(ctx, endpointChallenges, methodGet, nil)
	if err != nil {
		return nil, err
	}

	if res.Code != http.StatusOK {
		log.Error().Interface("res", res).Msg("failed getting challenges")
		return nil, ErrChallenges
	}

	var challs []models.Challenge
	if err := json.Unmarshal(res.Collection, &challs); err != nil {
		return nil, err
	}

	return challs, nil
}

func (c *Client) ChallengeAdd(ctx context.Context, chal models.Challenge) error {
	if chal.ID == "" {
		return ErrChallengeRequireID
	}

	data, err := json.Marshal(chal)
	if err != nil {
		return err
	}

	res, err := c.makeRequest(ctx, fmt.Sprintf(endpointChallenge, chal.ID), methodPut, bytes.NewReader(data))
	if err != nil {
		return err
	}

	if res.Code != http.StatusNoContent {
		log.Error().Str("challenge_id", chal.ID).Interface("res", res).Msg("failed creating challenge")
		return ErrChallengeCreate
	}

	return nil
}

func (c *Client) ChallengeDelete(ctx context.Context, chalID string) error {
	res, err := c.makeRequest(ctx, fmt.Sprintf(endpointChallenge, chalID), methodDelete, nil)
	if err != nil {
		return err
	}

	if res.Code != http.StatusNoContent {
		log.Error().Str("challenge_id", chalID).Interface("res", res).Msg("failed deleting challenge")
		return ErrChallengeDelete
	}

	return nil
}

func (c *Client) Files(ctx context.Context) (map[string]catalog.File, error) {
	res, err := c.makeRequest(ctx, endpointFiles, methodGet, nil)
	if err != nil {
		return nil, err
	}

	var files map[string]catalog.File
	if err := json.Unmarshal(res.Collection, &files); err != nil {
		return nil, err
	}

	return files, nil
}

func (c *Client) FileAdd(ctx context.Context, file models.File, f io.Reader) (string, error) {
	body, writer := io.Pipe()

	req, err := http.NewRequest(string(methodPost), fmt.Sprintf("%s/%s", c.Server, endpointFiles), body)
	if err != nil {
		return "", err
	}

	mw := multipart.NewWriter(writer)

	// add headers
	req.Header.Add("content-type", mw.FormDataContentType())
	req.Header.Add("authorization", fmt.Sprintf("Bearer %s", c.APIToken))
	// add etag for request
	if file.Sha256 != "" {
		req.Header.Add("If-None-Match", file.Sha256)
	}

	errchan := make(chan error, 1)
	go func() {
		defer writer.Close()
		defer mw.Close()

		w, err := mw.CreateFormFile("file", file.Filename)
		if err != nil {
			log.Error().Err(err).Msg("failed to create formfile")
			errchan <- err
			return
		}

		if _, err := io.Copy(w, f); err != nil {
			log.Error().Err(err).Msg("failed to create copy")
			errchan <- err
			return
		}

		// write out the last part
		if err := mw.Close(); err != nil {
			log.Error().Err(err).Msg("failed to close mw")
			errchan <- err
			return
		}

		errchan <- nil
	}()

	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return "", err
	}

	merr := <-errchan
	if err != nil {
		return "", merr
	}

	// decode response
	var apires APIRes
	if err := json.NewDecoder(res.Body).Decode(&apires); err != nil {
		return "", err
	}

	var files map[string]catalog.File
	if err := json.Unmarshal(apires.Collection, &files); err != nil {
		return "", err
	}

	for fileID := range files {
		return fileID, nil
	}

	return "", errors.New("no files returned after adding")
}

func (c *Client) FileDelete(ctx context.Context, id string) error {
	res, err := c.makeRequest(ctx, fmt.Sprintf(endpointFile, id), methodDelete, nil)
	if err != nil {
		return err
	}

	if res.Code != http.StatusNoContent {
		log.Error().Interface("res", res).Msg("failed to delete file")
		return ErrFileDeleteFailed
	}

	return nil
}

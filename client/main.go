package main

import (
	"fmt"
	"os"

	"gitlab.com/deviosec/octp/client/cmd"
)

func main() {
	c := cmd.New()
	if err := c.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

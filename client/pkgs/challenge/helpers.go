package challenge

import (
	"archive/tar"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/deviosec/octp/internal/virtual"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
)

// loadImage virtname is the name from the challenge file, ie. `main`, whereas name
// is the final name of the image.
// The image is loaded from location.
func (c *Challenge) LoadImage(ctx context.Context, name, location string) error {
	// ensure that the tar exists
	tarFile, err := os.Open(location)
	if errors.Is(err, fs.ErrNotExist) {
		return fmt.Errorf("tried to load '%s', but the file does not exists", location)
	}

	// actuall build it
	return c.VirtMan.LoadImage(ctx, virtual.BackendDocker, name, tarFile, os.Stdout)
}

func (c *Challenge) GetImageBackendInfo(ctx context.Context, name string) (virtmodels.Image, error) {
	images, err := c.VirtMan.Images(ctx, virtual.BackendDocker)
	if err != nil {
		return virtmodels.Image{}, err
	}

	for _, image := range images {
		if name == image.Name {
			return image, nil
		}
	}

	return virtmodels.Image{}, ErrBackendImageNotFound
}

// read dir helper
func readDirRec(dir string) (map[string]os.FileInfo, error) {
	files, err := readDirHelper(dir)
	if err != nil {
		return nil, err
	}

	// loop over files and remove the prefix
	nfiles := make(map[string]os.FileInfo)
	for filePath, fileInfo := range files {
		nfiles[filePath[len(dir)+1:]] = fileInfo
	}

	return nfiles, nil
}

func readDirHelper(dir string) (map[string]os.FileInfo, error) {
	returnDirs := make(map[string]os.FileInfo)

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		filePath := fmt.Sprintf("%s/%s", dir, file.Name())

		// check and evaluate if it is a symbalic link.
		// if it is then update file.
		// do not update filePath, as we do not want the symbolic
		// link path to be part of the filepaths
		if file.Mode()&os.ModeSymlink != 0 {
			actualPath, err := filepath.EvalSymlinks(filePath)
			if err != nil {
				return nil, err
			}

			// stat the evaluated symbalic link
			actualStat, err := os.Stat(actualPath)
			if err != nil {
				return nil, err
			}
			file = actualStat
		}

		// handle directory
		if file.IsDir() {
			pathFiles, err := readDirHelper(filePath)
			if err != nil {
				return nil, err
			}

			for path, osInfo := range pathFiles {
				returnDirs[path] = osInfo
			}
			continue
		}

		// handle file
		returnDirs[filePath] = file
	}

	return returnDirs, nil
}

// for rebuilding image
func (c *Challenge) rebuildImage(ctx context.Context, name, entrypoint string) error {
	// image, err := c.GetImageBackendInfo(ctx, name)
	// if err != nil {
	// 	return err
	// }

	// create a tar file
	tarFile, err := createTarball(c.Location)
	if err != nil {
		return err
	}

	// fmt.Println(name, entrypoint)
	// testBytes, err := ioutil.ReadAll(tarFile)
	// if err != nil {
	// 	return err
	// }

	// if err := ioutil.WriteFile("/tmp/test.tar.gz", testBytes, 0755); err != nil {
	// 	return err
	// }

	// return errors.New("test")

	// actuall build it
	return c.VirtMan.BuildImage(ctx, virtual.BackendDocker, name, entrypoint, tarFile, os.Stdout)
}

func createTarball(dir string) (io.Reader, error) {
	// initialize our buffer, and a new tar called tr
	ib := bytes.NewBuffer(nil)
	tr := tar.NewWriter(ib)

	// get all our files
	files, err := readDirRec(dir)
	if err != nil {
		return nil, err
	}

	for filePath, fileInfo := range files {
		// first write our header
		tr.WriteHeader(&tar.Header{
			Name:       filePath,
			Size:       fileInfo.Size(),
			Mode:       int64(fileInfo.Mode()),
			ModTime:    fileInfo.ModTime(),
			AccessTime: fileInfo.ModTime(),
			ChangeTime: fileInfo.ModTime(),
		})

		// read our file bytes, so we can write them
		b, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", dir, filePath))
		if err != nil {
			return ib, err
		}

		// write our data then
		_, err = tr.Write(b)
		if err != nil {
			return ib, err
		}
	}

	// done writing everything, return it
	tr.Close()

	return ib, nil
}

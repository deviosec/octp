package challenge

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/deviosec/octp/internal/virtual"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gopkg.in/yaml.v2"
)

const (
	CnstChallengeFileName = "challenge.yml"
)

// TODO(eyJhb): do not hardcode backend type, make the struct accept
// the backend type, or make each function accept the backend type?
// EACH FUNCTION ACCEPTING BACKEND TYPE MIGHT BE THE BEST!

var (
	ErrNoGenerator          = errors.New("no output file generator")
	ErrGeneratorNeedRebuild = errors.New("generator is out of date")
	ErrBackendImageNotFound = errors.New("backend image could not be found")
	ErrNoVirtualImport      = errors.New("virtual had no image to import")
	ErrNoChallengeFile      = errors.New("no challenge file found in the given directory")
	ErrNoBuildFile          = errors.New("could not find a build file for the given virtual")

	DefaultMakeOptions = MakeOptions{
		BuildGenerator:    true,
		BuildVirtuals:     true,
		LoadImageFromFile: true,
		BuildOutputFiles:  true,
	}
)

type MakeOptions struct {
	// images
	BuildGenerator bool
	BuildVirtuals  bool

	LoadImageFromFile bool

	// build output files
	BuildOutputFiles bool

	// fails
	RequireLoadImageFromFile bool
}

type Challenge struct {
	Challenge models.Challenge

	Location string

	VirtMan virtual.VirtualManager
}

type Manager interface {
	GetImageBackendInfo(ctx context.Context, name string) (virtmodels.Image, error)

	// generator
	HasGenerator(ctx context.Context) (bool, error)
	GeneratorNeedsRebuild(ctx context.Context) (bool, error)
	GeneratorRebuild(ctx context.Context) error
	GeneratorBuildOutput(ctx context.Context) error

	// virtual
	VirtualNeedsRebuild(ctx context.Context, name string) (bool, error)
	VirtualHasImage(ctx context.Context, name string) (bool, string, error)
	VirtualRebuild(ctx context.Context, name string) error
	VirtualLoadImage(ctx context.Context, name string) error

	// image names
	GeneratorImageName() string
	GeneratorImageNameNoTag() string
	VirtualImageName(name string) string
	VirtualImageNameNoTag(name string) string

	// main function
	Make(ctx context.Context, opts MakeOptions) error
}

// ensure that Challenge implements Manager
//lint:ignore U1000 ignored, just used to ensure interface is satisfied by challenge
func ensureImplements() Manager {
	return &Challenge{}
}

func ChallengeFinder(dir string) ([]string, error) {
	// remove any slashes at the end of the input dir
	if dir[len(dir)-1] == '/' {
		dir = dir[:len(dir)-1]
	}

	// get files in current dir
	files, err := readDirRec(dir)
	if err != nil {
		return nil, err
	}

	// first check if there is any challenge.yml here
	var challengeDirs []string
	for filePath, fileInfo := range files {
		if fileInfo.Name() == CnstChallengeFileName {
			challengeDirPath := fmt.Sprintf("%s/%s", dir, filePath)
			challengeDirs = append(challengeDirs, path.Dir(challengeDirPath))
		}
	}

	return challengeDirs, nil
}

func New(dir string) (Challenge, error) {
	// ensure that the challenge file exists and read it
	chalBytes, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", dir, CnstChallengeFileName))
	if err != nil {
		if os.IsNotExist(err) {
			return Challenge{}, ErrNoChallengeFile
		}
		return Challenge{}, err
	}

	var chal models.Challenge
	if err := yaml.Unmarshal(chalBytes, &chal); err != nil {
		return Challenge{}, err
	}

	return Challenge{
		Challenge: chal,
		Location:  dir,
		VirtMan:   virtual.New(),
	}, nil
}

func (c *Challenge) Make(ctx context.Context, opts MakeOptions) error {
	// generator and output files
	if opts.BuildGenerator || opts.BuildOutputFiles {
		// ensure that there exists a generator for this challenge
		hasGen, err := c.HasGenerator(ctx)
		if err != nil {
			return err
		}

		if hasGen {
			// check if generator needs rebuild
			needRebuild, err := c.GeneratorNeedsRebuild(ctx)
			if err != nil {
				return fmt.Errorf("failed to check if generator needs rebuild: %w", err)
			}

			// build generator if required
			if opts.BuildGenerator && needRebuild {
				if err := c.GeneratorRebuild(ctx); err != nil {
					return err
				}

				// set rebuild to false, as it was just build!
				needRebuild = false
			}

			// build output files
			if opts.BuildOutputFiles {
				if needRebuild {
					return fmt.Errorf("failed to build outputs files: %w", ErrGeneratorNeedRebuild)
				}

				if err := c.GeneratorBuildOutput(ctx); err != nil {
					return err
				}
			}
		}
	}

	// virtual and images
	if (opts.BuildVirtuals || opts.LoadImageFromFile) && len(c.Challenge.Lab.Virtuals) > 0 {
		// loop over all virtuals
		for _, virt := range c.Challenge.Lab.Virtuals {
			virtName := virt.Name

			imageName := c.VirtualImageName(virtName)

			// get the image matching the image of the virtual
			imageInfo, err := c.GetImageBackendInfo(ctx, imageName)
			if err != nil {
				// if !errors.Is(err, ErrBackendImageNotFound)
				if err != ErrBackendImageNotFound {
					return fmt.Errorf("unable to find backend image info: %w", err)
				}
			}

			var importedImage bool
			if opts.LoadImageFromFile {
				hasImage, imageDigest, err := c.VirtualHasImage(ctx, virtName)
				if err != nil {
					return err
				}

				// if the virtual has no image to import, and we require it
				// then return error
				if !hasImage && opts.RequireLoadImageFromFile {
					return fmt.Errorf("virtual %s has no image to import: %w", virtName, ErrNoVirtualImport)
				}

				// check if different, and if they are import it
				if imageDigest != imageInfo.Sha256 {
					imageLocation := c.VirtualsImageLocation(virtName)
					if err := c.LoadImage(ctx, c.VirtualImageNameNoTag(virtName), imageLocation); err != nil {
						return fmt.Errorf("failed to import image for %s got error: %w", virtName, err)
					}

					importedImage = true
				}
			}

			// if any image was imported, then there is no reason to rebuild
			// the image as well.
			if opts.BuildVirtuals && !importedImage {
				needRebuild, err := c.VirtualNeedsRebuild(ctx, virtName)
				if err != nil {
					return fmt.Errorf("failed to check if virtual %s need rebuild: %w", virtName, err)
				}

				if needRebuild {
					if err := c.VirtualRebuild(ctx, virtName); err != nil {
						return fmt.Errorf("failed to rebuild virtual %s got error: %w", virtName, err)
					}
				}
			}
		}
	}

	return nil
}

package challenge

import (
	"fmt"
	"os"

	"github.com/hashicorp/go-multierror"
)

var (
	DefaultValidatePreMakeNoBuildOptions = ValidateOptions{
		// OutputFiles:    true,
		// ChallengeModel: true,
		ChallengeFile: true,
		// DockerFiles:   true,
	}

	DefaultValidatePreMakeOptions = ValidateOptions{
		// OutputFiles:    true,
		// ChallengeModel: true,
		ChallengeFile: true,
		DockerFiles:   true,
	}

	DefaultValidatePostMake = ValidateOptions{
		OutputFiles:    true,
		ChallengeModel: true,
		ChallengeFile:  true,
		DockerFiles:    true,
	}
)

type ValidateOptions struct {
	OutputFiles bool

	ChallengeModel bool
	ChallengeFile  bool
	DockerFiles    bool
}

func (c *Challenge) Validate(opts ValidateOptions) error {
	var err *multierror.Error

	if opts.OutputFiles {
		// ensure that all output files exists
		for _, outFile := range c.Challenge.Files {
			err = multierror.Append(err, c.ValidateFile(outFile))
		}
	}

	if opts.ChallengeModel {
		// validate the challenge struct
		c.Challenge.Lab.Name = c.Challenge.ID
		err = multierror.Append(err, c.Challenge.Validate())
	}

	if opts.ChallengeFile {
		if _, err := os.Stat(fmt.Sprintf("%s/%s", c.Location, CnstChallengeFileName)); err != nil {
			if os.IsNotExist(err) {
				err = multierror.Append(err, ErrNoChallengeFile)
			} else {
				err = multierror.Append(err, err)
			}
		}
	}

	if opts.DockerFiles && len(c.Challenge.Lab.Virtuals) > 0 {
		if len(c.Challenge.Lab.Virtuals) == 1 {
			c.Challenge.Lab.Virtuals[0].Name = CnstVirtualDefaultName
		}

		for _, virt := range c.Challenge.Lab.Virtuals {
			// stat files
			dockerFiles := []string{fmt.Sprintf("%s/%s%s", c.Location, CnstVirtualBuildFilePrefix, virt.Name)}
			if virt.Name == CnstVirtualDefaultName {
				dockerFiles = append(dockerFiles, fmt.Sprintf("%s/%s", c.Location, CnstVirtualFilePrefix))
			}

			var foundFile bool
			for _, dockerFile := range dockerFiles {
				if _, err := os.Stat(dockerFile); err != nil {
					continue
				}

				foundFile = true
			}

			if !foundFile {
				err = multierror.Append(err, fmt.Errorf("no build file found for %s: %w", virt.Name, ErrNoBuildFile))
			}
		}
	}

	return err.ErrorOrNil()
}

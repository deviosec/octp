package challenge

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

const (
	CnstVirtualImagePrefix = "octpv-"

	CnstVirtualDefaultName     = "main"
	CnstVirtualsDir            = "virtuals"
	CnstVirtualFilePrefix      = "Dockerfile"
	CnstVirtualBuildFilePrefix = "Dockerfile."
)

func (c *Challenge) VirtualImageName(name string) string {
	return fmt.Sprintf("%s:latest", c.VirtualImageNameNoTag(name))
}

func (c *Challenge) VirtualImageNameNoTag(name string) string {
	return fmt.Sprintf("%s%s-%s", CnstVirtualImagePrefix, c.Challenge.ID, name)
}

// GetVirtualsImageLocation returns the location for the virtual `.tar.gz`
// ie. `/some/path/to/challenge/virtuals/<virtname>.tar.gz``
func (c *Challenge) VirtualsImageLocation(name string) string {
	return fmt.Sprintf("%s/%s/%s.tar.gz", c.Location, CnstVirtualsDir, name)
}

// GetVirtualsImageLocation returns the location for the virtual `.imageid`
// ie. `/some/path/to/challenge/virtuals/<virtname>.imageid
func (c *Challenge) VirtualsImageIDLocation(name string) string {
	return fmt.Sprintf("%s/%s/%s.imageid", c.Location, CnstVirtualsDir, name)
}

// VirtualHasImage returs if there exists an exported image for the virtual,
// the imageid of the exported image as well as an error for anyhting else
func (c *Challenge) VirtualHasImage(ctx context.Context, name string) (bool, string, error) {
	newImageID, err := ioutil.ReadFile(c.VirtualsImageIDLocation(name))
	if err != nil {
		if os.IsNotExist(err) {
			return false, "", nil
		}

		return false, "", err
	}

	return true, string(newImageID), nil
}

func (c *Challenge) VirtualRebuild(ctx context.Context, name string) error {
	entrypoint := CnstVirtualBuildFilePrefix + name

	// if this is our main, then try to build with default entrypoint
	if name == CnstVirtualDefaultName {
		if _, err := os.Open(c.Location + "/" + CnstVirtualFilePrefix); !os.IsNotExist(err) {
			entrypoint = CnstVirtualFilePrefix
		}
	}

	return c.rebuildImage(ctx, c.VirtualImageNameNoTag(name), entrypoint)
}

func (c *Challenge) VirtualLoadImage(ctx context.Context, name string) error {
	return nil
}

func (c *Challenge) VirtualNeedsRebuild(ctx context.Context, virtname string) (bool, error) {
	// determine all the virtuals we need to build
	// 1. Do the `Dockerfile.<virt-name>` exist for the virtual?
	// 2. Is there a image of `octpv-<chal-id>-<virt-name>`?
	// 3. Is there any `virtuals/<chal-id>-<virt-name>.tar.gz` in the source,
	//    and is the image already loaded? This is done by checking the
	//    `virtuals/<chal-id>-<virt-name>.imageid` and comparing it.
	//    It is assumed if imageid exits, then the rest will not be run
	//    But the image will be tried to load, if virtualNeedsRebuild returns true.
	// 4. Is the created time of the image 0? (if so, the images has not been created)
	// 5. Are there any files that are newer than our image (INCLUDING `/output` and `Dockerfile.<virt-name>`)?
	// 6. Did we rebuild our generator image? (checked outside)

	// Step 1. is ensured beforehand

	// Step 2.
	image, err := c.GetImageBackendInfo(ctx, c.VirtualImageName(virtname))
	if err != nil {
		if err != ErrBackendImageNotFound {
			return false, err
		}
	}

	// Step 3
	newImageID, err := ioutil.ReadFile(c.VirtualsImageIDLocation(virtname))
	if err == nil {
		return !strings.HasSuffix(image.Sha256, string(newImageID)), nil
	}

	// Step 4
	if image.Created == 0 {
		log.Printf("virtualNeedsRebuild: rebuilding virtual '%s' because imageCreated is 0", virtname)
		return true, nil
	}

	// Step 5
	fileInfos, _ := readDirRec(c.Location)
	for filePath, fileInfo := range fileInfos {
		if fileInfo.ModTime().Unix() > image.Created {
			log.Printf("virtualNeedsRebuild: rebuilding virtual '%s' because file '%s' is newer than image", virtname, filePath)
			return true, nil
		}
	}

	// Step 6. is ensured beforehand

	return false, nil
}

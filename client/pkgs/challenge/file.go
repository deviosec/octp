package challenge

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"os"

	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

var (
	ErrNoHash        = errors.New("no hash for file specified")
	ErrHashNotMatch  = errors.New("the hash did not match the specified hash")
	ErrFileNotExists = errors.New("the file does not exists")
)

func (c *Challenge) File(file models.File) (io.ReadCloser, error) {
	var filepath string
	for _, filename := range []string{file.Filename, CnstGeneratorOutputDir + file.Filename} {
		tmppath := fmt.Sprintf("%s/%s", c.Location, filename)
		// ensure file exists
		if _, err := os.Stat(tmppath); err != nil {
			if os.IsNotExist(err) {
				continue
			}

			return nil, err
		}

		filepath = tmppath
	}

	if filepath == "" {
		return nil, fmt.Errorf("file '%s' does not exists: %w", file.Filename, ErrFileNotExists)
	}

	// calculate checksum
	fh, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	return fh, nil
}

func (c *Challenge) CalculateSHA256(file models.File) (string, error) {
	fh, err := c.File(file)
	if err != nil {
		return "", err
	}
	defer fh.Close()

	hasher := sha256.New()
	if _, err := io.Copy(hasher, fh); err != nil {
		return "", err
	}

	return hex.EncodeToString(hasher.Sum(nil)[:]), nil
}

func (c *Challenge) ValidateFile(file models.File) error {
	// ensure that it has sha256
	if file.Sha256 == "" {
		return ErrNoHash
	}

	hash, err := c.CalculateSHA256(file)
	if err != nil {
		return err
	}

	if hash != file.Sha256 {
		return fmt.Errorf("file '%s' with hash '%s' did not match expected hash '%s': %w", file.Filename, hash, file.Sha256, ErrHashNotMatch)
	}

	return nil
}

package challenge

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/deviosec/octp/internal/virtual"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
)

const (
	CnstGeneraterName             = "generator"
	CnstGeneratorImagePrefix      = "octpg-"
	CnstGeneratorOutputDir        = "output/"
	CnstGeneratorOutputVirtualDir = "/octp-output/"
	CnstVirtualFlagPrefix         = "FLAG_"
	CnstVirtualGenerateTimeout    = 15 * 60 // timeout in secounds
)

var (
	ErrGenerateImageNotExited = errors.New("generator image did not exit within timeout")
)

func (c *Challenge) HasGenerator(ctx context.Context) (bool, error) {
	fileInfos, err := readDirRec(c.Location)
	if err != nil {
		return false, fmt.Errorf("failed to check if generator needs rebuild: %w", err)
	}

	for fileName := range fileInfos {
		if fileName == CnstVirtualBuildFilePrefix+CnstGeneraterName {
			return true, nil
		}
	}

	return false, nil
}

func (c *Challenge) GeneratorImageName() string {
	return fmt.Sprintf("%s:latest", c.GeneratorImageNameNoTag())
}

func (c *Challenge) GeneratorImageNameNoTag() string {
	return fmt.Sprintf("%s%s", CnstGeneraterName, c.Challenge.ID)
}

func (c *Challenge) GeneratorRebuild(ctx context.Context) error {
	return c.rebuildImage(ctx, c.GeneratorImageNameNoTag(), CnstVirtualBuildFilePrefix+CnstGeneraterName)
}

func (c *Challenge) GeneratorNeedsRebuild(ctx context.Context) (bool, error) {
	// determine if there is a generator, that needs to generate
	// files for us to use. This can be done by checking the following.
	// 1. Check if all files in `output/` exists - If not, rebuild
	// 2. Check all files in directory (except `/output`), get the newest file
	//    and check against output. Output is older than newest file? Rebuild
	//    This will ALWAYS return False, if it is not the case

	// step 1/step 2
	fileInfos, err := readDirRec(c.Location)
	if err != nil {
		return false, fmt.Errorf("failed to check if generator needs rebuild: %w", err)
	}

	var newestFileModTime int64
	seenOutputFiles := make(map[string]fs.FileInfo)
	for filePath, fileInfo := range fileInfos {
		// if it does not have the CnstGeneratorOutputDir prefix,
		// then it is a regular file outside the generator dir,
		// and the modtime is saved for the newest file
		if !strings.HasPrefix(filePath, CnstGeneratorOutputDir) {
			if fileInfo.ModTime().Unix() > newestFileModTime {
				newestFileModTime = fileInfo.ModTime().Unix()
			}
			continue
		}

		// otherwise it is a output file, and we add it to our list of seen
		// output files
		seenOutputFiles[fileInfo.Name()] = fileInfo
	}

	for _, file := range c.Challenge.Files {
		trimmedFilename := strings.TrimPrefix(file.Filename, CnstGeneratorOutputDir)

		fileInfo, exists := seenOutputFiles[trimmedFilename]
		if !exists {
			return true, nil
		}

		// this is where step 2 happens
		// this is only if there exists any file outside of
		// the CnstGeneratorOutputDir, that is newer than
		// the files in output (STRICTLY NEWER! Sholud work
		// with nix builds)
		if newestFileModTime > fileInfo.ModTime().Unix() {
			return true, nil
		}
	}

	return false, nil
}

func (c *Challenge) GeneratorBuildOutput(ctx context.Context) error {
	outputDir := fmt.Sprintf("%s/%s", c.Location, CnstGeneratorOutputDir)

	// make our output dir, if it does not exist
	if _, err := os.Stat(outputDir); err != nil {
		if !os.IsNotExist(err) {
			return err
		}

		// create dir then
		if err := os.Mkdir(outputDir, os.ModePerm); err != nil {
			return err
		}
	}

	mounts := []virtmodels.Mount{
		virtmodels.Mount{
			Host:    outputDir,
			Guest:   CnstGeneratorOutputVirtualDir,
			Options: "rw",
		},
	}

	// put all the flags into the generator virtual
	// it should know which to use and which not to use
	envs := []string{}
	for _, flag := range c.Challenge.Flags {
		envs = append(envs, fmt.Sprintf("%s_%s=%s", CnstVirtualFlagPrefix, flag.Name, flag.Flag))
	}

	virt := virtmodels.Virtual{
		Image:  c.GeneratorImageNameNoTag(),
		Mounts: mounts,
		Envs:   envs,
		Type:   virtual.BackendDocker.String(),
	}

	// create our container
	virtID, err := c.VirtMan.Create(ctx, virt)
	if err != nil {
		return err
	}
	virt.ID = virtID

	// start our container
	if err := c.VirtMan.Start(ctx, virt.ID); err != nil {
		return err
	}

	// wait for it to exit, we have a timeout as well
	var containerExited bool
	for i := 0; i < CnstVirtualGenerateTimeout; i++ {
		// list containers, and see if it still is in the list.
		// if it isn't in the list, mark it as exited, and then
		// we break the loop
		containers, err := c.VirtMan.List(ctx, virtual.BackendDocker, true)
		if err != nil {
			return err
		}

		for _, container := range containers {
			if container.ID == virt.ID && container.State == virtmodels.VirtualStateStopped {
				containerExited = true
				break
			}
		}

		if containerExited {
			break
		}

		// sleep for one secound, for each run
		time.Sleep(time.Second)
	}

	// after we have generated our stuff, destroy virtual
	if err := c.VirtMan.Destroy(ctx, virt.ID); err != nil {
		log.Printf("failed to destroy virtual after generating files for challenge '%s' use: %v", c.Challenge.Title, err)
		return err
	}

	// if the container never exited properly,
	// then throw a error to the user.
	if !containerExited {
		return ErrGenerateImageNotExited
	}

	return nil
}

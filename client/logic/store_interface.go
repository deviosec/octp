package logic

type Event struct {
	// full path to server, ie. https://play.deviosec.dk
	Server string
	Auth   AuthInfo
}

type AuthInfo struct {
	ID    string
	Token string
}

type Store interface {
	// cloud providers (map[name]token)
	GetProviders() (map[string]string, error)
	AddProvider(name, token string) error
	GetDefaultProvider() (string, error)
	SetDefaultProvider(name string) error
	DeleteProvider(name string) error

	// events
	GetEvents() (map[string]Event, error)
	AddEvent(name string, event Event) error
	DeleteEvent(name string) error

	// challenge sets (map[name]path)
	GetChallengeSets() (map[string]string, error)
	AddChallengeSet(name, path string) error
	DeleteChallengeSet(name string) error

	// server template (local version)
	GetServerTemplate() (string, bool, error)
	SetServerTemplate(template string) error

	// worker template (local version)
	GetWorkerTemplate() (string, bool, error)
	SetWorkerTemplate(template string) error

	// ssh key fingerpring
	GetSSHKeyFingerprint() (string, error)
	SetSSHKeyFingerprint(fingerprint string) error
}

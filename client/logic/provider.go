package logic

import (
	"errors"

	"gitlab.com/deviosec/octp/internal/providers"
)

var (
	ErrTokenNotFound = errors.New("the token with the specifed name could not be found")
)

func (l *Logic) GetProvider(name string) (providers.Provider, error) {
	if l.providers == nil {
		l.providers = make(map[string]providers.Provider)
	}

	var err error
	if name == "" {
		name, err = l.Store.GetDefaultProvider()
		if err != nil {
			return nil, err
		}
	}

	if p, ok := l.providers[name]; ok {
		return p, nil
	}

	listProviders, err := l.Store.GetProviders()
	if err != nil {
		return nil, err
	}

	var token *string
	for k, v := range listProviders {
		if k == name {
			token = &v
			break
		}
	}

	if token == nil {
		return nil, ErrTokenNotFound
	}

	// init it
	p, err := providers.New(*token)
	if err != nil {
		return nil, err
	}

	l.providers[name] = p

	return p, nil
}

package logic

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/client/pkgs/challenge"
)

var (
	ReChallengeSetName = `^[a-z0-9]+$`
)

func (l *Logic) GetChallenges(challsetName string) ([]challenge.Challenge, error) {
	// get config, and see if challenge set exists
	challengeSets, err := l.Store.GetChallengeSets()
	if err != nil {
		return nil, err
	}

	challSetPath, ok := challengeSets[challsetName]
	if !ok {
		return nil, fmt.Errorf("the specified challenge set '%s' does not exists", challsetName)
	}

	// get all challenge locations (contains challenge.yml)
	challengesDirs, err := challenge.ChallengeFinder(challSetPath)
	if err != nil {
		return nil, err
	}

	// parse all challenges
	var challenges []challenge.Challenge
	for _, challengeDir := range challengesDirs {
		chal, err := challenge.New(challengeDir)
		if err != nil {
			log.Error().Err(err).Str("challenge_dir", challengeDir).Msg("failed to parse challenge")
			continue
		}

		challenges = append(challenges, chal)
	}

	return challenges, nil
}

package logic

import (
	"gitlab.com/deviosec/octp/internal/providers"
	"gitlab.com/deviosec/octp/internal/virtual"
)

type Logic struct {
	Store   Store
	VirtMan virtual.VirtualManager

	// internal runtime state
	// different providers (+ implementations)
	providers map[string]providers.Provider
}

# OCTP (Open Cloud Training Platform)
Please note, that this is outdated and should be changed!

The goal for OCTP is to be a generic way to deploy challenges in various ways!
The most important way for OCTP to be deployed, is in cloud environments.
OCTP does NOT care about where you deploy it, but it works under the assumption that it does not have to share resources with anything BUT itself.
Therefore its ideal way of deployment is in a dedicated cloud environment, where each agent (lab environment), will be its own VPS.

OCTP will function as a basis for others to build upon, and will not be a finished and polished product with all the bells and whistles.
Instead OCTP aims to make a HTTP REST API accessible, that can be scripted up against in various ways, and therefore easily be accessible.

There will of course be modules provided which together will form a fully developed platform, that is ready for all kinds of CTFs, or whatever you throw at it.

Enjoy!


package challengemanager

import (
	"context"
	"errors"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/virtual"
	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"go.etcd.io/bbolt"
)

const (
	heartbeatInterval      = 60 * time.Second
	challengesSyncInterval = 3 * 60 * time.Second
	ensureRunningInterval  = 1 * time.Second
	runGCInterval          = 5 * time.Minute
)

var (
	ErrPodWithIDNotFound = errors.New("could not find our own pod with the given id")

	challengesBucket = []byte("challenges")
)

type ChallengeManager struct {
	// bolt holds all the challenges in a bucket
	// named `challenges`
	Bolt *bbolt.DB

	// manage virtual devices
	VirtMan virtual.VirtualManager

	// basic configuration parameters that are passed in
	Debug bool

	// challenge states
	ChallengeStateLock sync.RWMutex
	ChallengeStates    map[string]models.ChallengeState
}

func (cm *ChallengeManager) StartRoutines() {
	// GC everything every minute, to ensure that
	// we do not use space on stale images, containers, etc.
	go func() {
		// do not do garbage collection in debug mode
		if cm.Debug {
			return
		}

		for {
			ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
			if err := cm.VirtMan.GC(ctx); err != nil {
				log.Error().Err(err).Msg("failed to gc")
			}
			cancel()

			// sleep
			time.Sleep(runGCInterval)
		}
	}()

	// ensure that the challenges are running
	go func() {
		for {
			// gets all challenges, active and not active
			allChallenges, err := cm.ChallengesList()
			if err != nil {
				log.Error().Err(err).Msg("failed to list challenges")
				continue
			}

			// holds all the active challenges
			challenges := make(map[string]CMChallenge)
			for _, chall := range allChallenges {
				// if challenge is not active, or no virtuals, then skip over it
				if !chall.Challenge.Active || len(chall.Challenge.Lab.Virtuals) == 0 {
					continue
				}

				challenges[chall.ID] = chall
			}

			// get a context, so it will not continue forever
			ctx, cancel := context.WithTimeout(context.Background(), 2*60*time.Second)
			if err := cm.syncWithChallenges(ctx, challenges); err != nil {
				log.Error().Err(err).Msg("syncWithChallenges failed to complete")
			}
			cancel()

			// sleep
			time.Sleep(ensureRunningInterval)
		}
	}()
}

func (cm *ChallengeManager) syncWithChallenges(ctx context.Context, challenges map[string]CMChallenge) error {
	// remove containers we do not need anymore.
	// TODO(eyJhb) including virtuals that have changed hash.
	// Maybe we should do more?
	if err := cm.removeUnneededChallenges(ctx, challenges); err != nil {
		log.Error().Err(err).Msg("removeUnneededChallenges failed")
	}

	// create all the challenges we need
	if err := cm.createRequiredChallenges(ctx, challenges); err != nil {
		log.Error().Err(err).Msg("createRequiredChallenges failed")
	}

	// ensure all virtuals are in the correct state (running)
	if err := cm.ensureRunning(ctx, challenges); err != nil {
		log.Error().Err(err).Msg("ensureRunning failed")
	}

	return nil
}

// removeUnneededChallenges removes all vituals that is no longer needed.
func (cm *ChallengeManager) removeUnneededChallenges(ctx context.Context, challenges map[string]CMChallenge) error {
	cm.ChallengeStateLock.Lock()
	defer cm.ChallengeStateLock.Unlock()

	// loop challenge states, and removed the ones that is no longer relevant
	for id := range cm.ChallengeStates {
		if _, ok := challenges[id]; !ok {
			delete(cm.ChallengeStates, id)
		}
	}

	// get all virtuals on the machine - currently
	// just using dockerBackend
	virtuals, err := cm.VirtMan.List(ctx, virtual.BackendDocker, false)
	if err != nil {
		return err
	}

	// make it into a map[string]bool
	neededVirtuals := make(map[string]bool)
	for _, v := range virtuals {
		// if debug we will not remove the registry
		if cm.Debug && (strings.HasPrefix(v.Image, "registry:") || strings.HasPrefix(v.Image, "postgres:")) {
			neededVirtuals[v.ID] = true
			continue
		}

		// otherwise add it to the list of virtuals,
		// but do net mark it as needed yet
		neededVirtuals[v.ID] = false
	}

	// loop over our challenges, and use our state
	for _, chall := range challenges {
		// populate the lab
		nl, err := cm.VirtMan.PopulateLab(ctx, chall.Challenge.Lab)
		if err != nil {
			if err != virtual.ErrIncompleteLab {
				return err
			}
		}

		// loop all virtuals in lab - and set the id
		for _, virtual := range nl.Virtuals {
			neededVirtuals[virtual.ID] = true
		}
	}

	// loop over neededVirtuals, and everything false should be deleted
	var destroyedVirtual bool
	for virtualID, v := range neededVirtuals {
		if v {
			continue
		}

		destroyedVirtual = true
		if err := cm.VirtMan.Destroy(ctx, virtualID); err != nil {
			log.Error().Str("virtualid", virtualID).Msg("failed to destroy virtual")
		}
	}

	if destroyedVirtual {
		if err := cm.VirtMan.GCNetworks(ctx); err != nil {
			log.Error().Err(err).Msg("failed to gc networks")
		}
	}

	return nil
}

func (cm *ChallengeManager) createRequiredChallenges(ctx context.Context, challenges map[string]CMChallenge) error {
	// loop over all challenges, see if we have the required ones
	for _, challenge := range challenges {
		// populate lab
		lab, err := cm.VirtMan.PopulateLab(ctx, challenge.Challenge.Lab)
		if err != nil {
			if err != virtual.ErrIncompleteLab {
				return err
			}
		}

		// loop all virtuals, if any of them do not have a ID
		// then recrate the lab
		var requireRecrate bool
		for _, virtual := range lab.Virtuals {
			if virtual.ID == "" {
				requireRecrate = true
			}
		}

		if requireRecrate {
			if err := cm.recreateLab(ctx, challenge.ID, challenge.Challenge.Lab, challenge.Auth); err != nil {
				log.Error().Err(err).Str("labname", lab.Name).Msg("failed to recreate lab")
			}
		}
	}

	return nil
}

func (cm *ChallengeManager) ensureRunning(ctx context.Context, challenges map[string]CMChallenge) error {
	// loop all challenges, populate the lab
	for _, challenge := range challenges {
		// populate the lab
		lab, err := cm.VirtMan.PopulateLab(ctx, challenge.Challenge.Lab)
		if err != nil {
			if err != virtual.ErrIncompleteLab {
				return err
			}
		}

		// ensure
		var requireRecrate bool
		for _, virtual := range lab.Virtuals {

			virt, err := cm.VirtMan.Get(ctx, virtual.ID)
			if err != nil {
				log.Error().Err(err).Str("virtualid", virtual.ID).Msg("could not get virtual, assuming dead")
				virt.State = virtmodels.VirtualStateStopped
			}

			switch virt.State {
			case virtmodels.VirtualStateHealthy:
			case virtmodels.VirtualStateRunning:
			default:
				requireRecrate = true
			}
		}

		// if requireRecrate is true, then we need to
		// destroy the lab, create it again and start it
		if requireRecrate {
			if err := cm.recreateLab(ctx, challenge.ID, challenge.Challenge.Lab, challenge.Auth); err != nil {
				log.Error().Err(err).Str("labname", lab.Name).Msg("failed to recreate lab")
			}
		}
	}

	return nil
}

func (cm *ChallengeManager) recreateLab(ctx context.Context, userChallengeID string, lab virtmodels.Lab, auth virtmodels.Auth) error {
	// loop virtuals to populate them correctly
	// and to ensure we have the images that each virtual requires
	cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerFetching)
	for _, virt := range lab.Virtuals {
		// pull image
		if err := cm.VirtMan.PullImage(ctx, virtual.GuessBackend(virt.Type), virt.Image, auth); err != nil {
			cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerError)
			return err
		}
	}

	cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerCreating)
	if err := cm.VirtMan.DestroyLab(ctx, &lab); err != nil {
		cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerError)
		return err
	}

	if err := cm.VirtMan.CreateLab(ctx, &lab); err != nil {
		cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerError)
		return err
	}

	cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerStarting)
	if err := cm.VirtMan.StartLab(ctx, lab); err != nil {
		cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerError)
		return err
	}

	cm.setChallengeState(userChallengeID, models.ChallengeStateWorkerRunning)

	return nil
}

func (cm *ChallengeManager) setChallengeState(userChallengeID string, state models.ChallengeState) {
	cm.ChallengeStateLock.Lock()
	defer cm.ChallengeStateLock.Unlock()
	cm.ChallengeStates[userChallengeID] = state
}

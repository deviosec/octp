package challengemanager

import (
	"encoding/json"

	virtmodels "gitlab.com/deviosec/octp/internal/virtual/models"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
)

type CMChallenge struct {
	ID        string
	Challenge models.Challenge
	Auth      virtmodels.Auth
}

func (cm *ChallengeManager) ChallengeAdd(chal CMChallenge) error {
	// begin transaction
	tx, err := cm.Bolt.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Commit()

	// from documentation
	// `Returns an error if the bucket name is blank,
	// or if the bucket name is too long.`
	b, _ := tx.CreateBucketIfNotExists(challengesBucket)

	// encode our challenge into json
	data, err := json.Marshal(chal)
	if err != nil {
		return err
	}

	if err := b.Put([]byte(chal.ID), data); err != nil {
		return err
	}

	cm.ChallengeStateLock.Lock()
	defer cm.ChallengeStateLock.Unlock()
	cm.ChallengeStates[chal.ID] = models.ChallengeStateAssigned

	return nil
}

func (cm *ChallengeManager) ChallengesSet(challenges map[string]CMChallenge) error {
	tx, err := cm.Bolt.Begin(true)
	if err != nil {
		return err
	}

	b := tx.Bucket(challengesBucket)
	if b != nil {
		if err := tx.DeleteBucket(challengesBucket); err != nil {
			tx.Rollback()
			return err
		}
	}

	b, err = tx.CreateBucket(challengesBucket)
	if err != nil {
		tx.Rollback()
		return err
	}

	for k, v := range challenges {
		data, err := json.Marshal(v)
		if err != nil {
			tx.Rollback()
			return err
		}

		if err := b.Put([]byte(k), data); err != nil {
			tx.Rollback()
			return err
		}
	}

	tx.Commit()

	cm.ChallengeStateLock.Lock()
	defer cm.ChallengeStateLock.Lock()
	cm.ChallengeStates = make(map[string]models.ChallengeState)

	return nil
}

func (cm *ChallengeManager) ChallengesList() (map[string]CMChallenge, error) {
	tx, err := cm.Bolt.Begin(false)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	b := tx.Bucket(challengesBucket)
	if b == nil {
		return map[string]CMChallenge{}, nil
	}

	// iterate over our bucket
	challenges := make(map[string]CMChallenge)
	err = b.ForEach(func(k, v []byte) error {
		var ca CMChallenge
		if err := json.Unmarshal(v, &ca); err != nil {
			return err
		}

		challenges[string(k)] = ca

		return nil
	})
	if err != nil {
		return nil, err
	}

	return challenges, nil
}

func (cm *ChallengeManager) ChallengeExists(ID string) (bool, error) {
	tx, err := cm.Bolt.Begin(false)
	if err != nil {
		return false, err
	}
	defer tx.Rollback()

	b := tx.Bucket(challengesBucket)
	if b == nil {
		return false, nil
	}

	return b.Get([]byte(ID)) != nil, nil
}

func (cm *ChallengeManager) ChallengeDelete(ID string) error {
	tx, err := cm.Bolt.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Commit()

	b, _ := tx.CreateBucketIfNotExists(challengesBucket)

	if err := b.Delete([]byte(ID)); err != nil {
		return err
	}

	cm.ChallengeStateLock.Lock()
	defer cm.ChallengeStateLock.Unlock()
	delete(cm.ChallengeStates, ID)

	return nil
}

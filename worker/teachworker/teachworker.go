package teachworker

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/teach/pkgs/teachworkerapi"
	"gitlab.com/deviosec/octp/worker/challengemanager"
)

type TeachWorker struct {
	// challenge manager
	CM *challengemanager.ChallengeManager

	// configuration
	EncryptionKey string
}

func (tw *TeachWorker) ValidateConfig() error {
	if tw.EncryptionKey == "" {
		return errors.New("TeachWorker: cannot have a empty encryption key")
	}

	return nil
}

func (tw *TeachWorker) Start() error {
	mux := http.NewServeMux()

	mux.HandleFunc("/challenges", tw.HandlerChallenges)

	log.Info().Str("port", teachworkerapi.TeachworkerAPIPort).Msg("started worker webserver")
	if err := http.ListenAndServe(":"+teachworkerapi.TeachworkerAPIPort, mux); err != nil {
		return errors.Wrap(err, "Error while trying to start TeachWorker")
	}

	return nil
}

func (tw *TeachWorker) HandlerChallenges(w http.ResponseWriter, r *http.Request) {
	// read the encrypted body
	encBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Error().Err(err).Msg("ChallengeHandler: failed to read body")
		return
	}

	// process the body
	msg, err := teachworkerapi.DecryptMessage(encBody, tw.EncryptionKey)
	if err != nil {
		log.Error().Err(err).Msg("ChallengeHandler: failed to decrypt message")
		tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
		return
	}

	if msg.Type == teachworkerapi.TypeList {
		// get challenges
		challenges, err := tw.CM.ChallengesList()
		if err != nil {
			log.Error().Err(err).Msg("failed to get all challenges")
			tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
			return
		}

		// map them to map[string]models.Challenge
		resChallenges := make(map[string]models.Challenge)
		for id, chall := range challenges {
			resChallenges[id] = chall.Challenge
		}

		// marshal it
		res, err := json.Marshal(resChallenges)
		if err != nil {
			log.Error().Err(err).Msg("failed to marshal challenges")
			tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
			return
		}

		w.Write(res)
	} else if msg.Type == teachworkerapi.TypeDelete {
		if err := tw.CM.ChallengeDelete(msg.ID); err != nil {
			log.Error().Err(err).Msg("failed to delete challenge")
			tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
		} else {
			tw.response(w, http.StatusOK, teachworkerapi.ResSuccess)
		}
	} else if msg.Type == teachworkerapi.TypeAdd {
		msg.Challenge.Lab.Name = msg.ID

		// check if already have a challenge running
		exists, err := tw.CM.ChallengeExists(msg.ID)
		if err != nil {
			log.Error().Err(err).Msg("failed to check challenge exists")
			tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
			return
		}

		if exists {
			tw.response(w, http.StatusOK, teachworkerapi.ResDuplicate)
			return
		}

		// set the challenge to active, always!
		msg.Challenge.Active = true

		if err := tw.CM.ChallengeAdd(challengemanager.CMChallenge{ID: msg.ID, Challenge: *msg.Challenge, Auth: *msg.Auth}); err != nil {
			log.Error().Err(err).Msg("failed to add challenge")
			tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
			return
		}

		tw.response(w, http.StatusOK, teachworkerapi.ResSuccess)
	} else if msg.Type == teachworkerapi.TypeStates {
		tw.CM.ChallengeStateLock.RLock()
		defer tw.CM.ChallengeStateLock.RUnlock()

		// marshal challenge states
		res, err := json.Marshal(tw.CM.ChallengeStates)
		if err != nil {
			log.Error().Err(err).Msg("failed to marshal challenge states")
			tw.response(w, http.StatusInternalServerError, teachworkerapi.ResFailure)
			return
		}

		w.Write(res)
	} else {
		tw.response(w, http.StatusBadRequest, teachworkerapi.ResInvalidMsg)
	}
}

// Helper function
func (tw TeachWorker) response(w http.ResponseWriter, statusCode int, res teachworkerapi.Response) {
	b, _ := json.Marshal(res)
	w.WriteHeader(statusCode)
	w.Write(b)
}

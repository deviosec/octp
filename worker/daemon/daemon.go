package daemon

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/virtual"
	"gitlab.com/deviosec/octp/teach/pkgs/models"
	"gitlab.com/deviosec/octp/worker/challengemanager"
	"gitlab.com/deviosec/octp/worker/teachworker"
	"go.etcd.io/bbolt"
)

type Daemon struct {
	Config Config
	BoltDB *bbolt.DB
}

type Config struct {
	// ID of the host
	ID string `mapstructure:"id"`

	// encryption key that we use to decrypt
	// messages from the server
	EncryptionKey string `mapstructure:"encryption_key"`

	// used as a way to keep existing containers
	// from being deleted
	Debug bool `mapstructure:"debug"`
}

// TODO(eyJhb) should we contact the server to know
// which challenges we should be running?
func (d *Daemon) Start() error {
	log.Info().Msg("Starting daemon!")

	// init our challenge manager
	cm := &challengemanager.ChallengeManager{
		// BoltDB holds all challenges, etc.
		// in a bucket name "challenges"
		Bolt: d.BoltDB,

		VirtMan: virtual.New(),

		// config stuff
		Debug: d.Config.Debug,

		// initialise challenge state map
		ChallengeStates: make(map[string]models.ChallengeState),
	}

	log.Info().Msg("Starting challenge manager routines!")
	go func() { cm.StartRoutines() }()

	log.Info().Msg("Starting TeachWorker")

	// init teach worker
	tw := teachworker.TeachWorker{
		// challenge manager
		CM: cm,

		// config stuff
		EncryptionKey: d.Config.EncryptionKey,
	}

	if err := tw.ValidateConfig(); err != nil {
		return err
	}

	// starting teachworker
	if err := tw.Start(); err != nil {
		return err
	}

	return nil
}

package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/deviosec/octp/worker/daemon"
	"go.etcd.io/bbolt"
)

const (
	FileNameBoltDB = "worker-bolt.db"
	// timeout when opening bbolt database specified in seconds
	BoltOpenDBTimeout = 5
	// The key argument should be the AES key, either 16, 24, or 32 bytes to select AES-128, AES-192, or AES-256.
	EncKeySize = 32
)

var (
	flagConfig = flag.String("config", "", "path to configuration file, if empty reads from default location")
)

func main() {
	// parse flags
	flag.Parse()

	// get user configuration dir
	userConfigDir, err := os.UserConfigDir()
	if err != nil {
		log.Fatal().Err(err).Msg("worker: failed to get userConfigDir")
	}

	octpConfigPath := fmt.Sprintf("%s/octp", userConfigDir)

	// configure viper
	viper.SetConfigName("worker")
	viper.SetEnvPrefix("octp_worker")
	viper.AutomaticEnv()

	// prefer flag configuration, over normal paths
	if flagConfig != nil && *flagConfig != "" {
		viper.SetConfigFile(*flagConfig)
	} else {
		viper.AddConfigPath(octpConfigPath)
	}

	// read config file
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("worker: failed to read configuration file")
	}

	var conf daemon.Config
	if err := viper.Unmarshal(&conf); err != nil {
		log.Fatal().Err(err).Msg("worker: failed to unmarshal configuration file")
	}

	// open bbolt database
	finalBoltDBPath := fmt.Sprintf("%s/%s", userConfigDir, FileNameBoltDB)
	bboltDB, err := bbolt.Open(finalBoltDBPath, 0660, &bbolt.Options{Timeout: BoltOpenDBTimeout * time.Second})
	if err != nil {
		log.Fatal().Err(err).Msg("failed to open bbolt database")
	}

	// init daemon
	d := daemon.Daemon{
		Config: conf,
		BoltDB: bboltDB,
	}
	if err := d.Start(); err != nil {
		log.Fatal().Err(err).Msg("failed to start daemon")
	}
}

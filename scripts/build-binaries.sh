#!/usr/bin/env bash

# ref for building
# https://www.digitalocean.com/community/tutorials/how-to-build-go-executables-for-multiple-platforms-on-ubuntu-16-04

build_bin () {
    OS=$1 # what os (linux, windows, etc.)
    ARCH=$2 # what arch (amd64, 386, etc.)
    IN=$3 # location to use for building
    OUT=$4 # output file
    echo "Building '$IN' for '$OS-$ARCH' outputting to '$OUT'."

    # get dependencies if needed - do not need this
    # env GOOS=$OS GOARCH=$ARCH go get -v ./...

    # build it
    # env CGO_ENABLED=1 GOOS=$OS GOARCH=$ARCH go build -a -ldflags '-extldflags "-static"' -o $OUT $IN
    env CGO_ENABLED=0 GOOS=$OS GOARCH=$ARCH go build -tags seccomp -ldflags="-extldflags=static" -o $OUT $IN
    upx $OUT
}

# build server (linux only)
# build_bin "linux" "amd64" "server/main.go" "bin/server-linux-amd64"
# build_bin "linux" "386" "server/main.go" "bin/server-linux-386"

# build worker (linux only)
build_bin "linux" "amd64" "worker/main.go" "bin/worker-linux-amd64"
echo copying bin
cp bin/worker-linux-amd64 ./teach/web/bins/
# build_bin "linux" "386" "worker/main.go" "bin/worker-linux-386"

# build teach things
build_bin "linux" "amd64" "teach/main.go" "bin/teach-linux-amd64"

# build client/cmd 
# build_bin "linux" "amd64" "client/main.go" "bin/client-linux-amd64"
# build_bin "linux" "386" "client/main.go" "bin/client-linux-386"
# build_bin "windows" "amd64" "client/main.go" "bin/client-windows-amd64.exe"
# build_bin "windows" "386" "client/main.go" "bin/client-windows-386.exe"
# build_bin "darwin" "amd64" "client/main.go" "bin/client-darwin-amd64"
# build_bin "darwin" "386" "client/main.go" "bin/client-darwin-386"

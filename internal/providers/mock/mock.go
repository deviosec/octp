package mock

import (
	"context"
	"crypto/rand"
	"fmt"
	"sync"

	"gitlab.com/deviosec/octp/internal/providers/models"
)

type MockProvider struct {
	pods map[string]models.PRPod

	sync.RWMutex
}

func New(_ string) (*MockProvider, error) {
	return &MockProvider{
		pods: make(map[string]models.PRPod),
	}, nil
}

func (m *MockProvider) ListPods(ctx context.Context) ([]models.PRPod, error) {
	m.RLock()
	defer m.RUnlock()

	var pods []models.PRPod
	for _, pod := range m.pods {
		pods = append(pods, pod)
	}

	return pods, nil
}

func (m *MockProvider) DestroyPod(ctx context.Context, name string) error {
	m.Lock()
	defer m.Unlock()

	delete(m.pods, name)

	return nil
}

func (m *MockProvider) CreatePod(ctx context.Context, name, template, image string, SSHKeys []string) error {
	m.Lock()
	defer m.Unlock()

	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return err
	}

	id := fmt.Sprintf("%x", b)
	m.pods[name] = models.PRPod{
		ID:   id,
		Name: name,
	}

	return nil
}

func (m *MockProvider) PublicIPPod(ctx context.Context, name string, IPv6 bool) (string, error) {
	if IPv6 {
		return "::1", nil
	}

	return "127.0.0.1", nil
}

func (m *MockProvider) PrivateIPPod(ctx context.Context, name string) (string, error) {
	return "127.0.0.1", nil
}

func (m *MockProvider) PodExists(ctx context.Context, name string) (bool, error) {
	m.RLock()
	defer m.RUnlock()

	_, ok := m.pods[name]
	return ok, nil
}

func (m *MockProvider) DefaultImage() string {
	return "mockImage"
}

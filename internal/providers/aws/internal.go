package aws

import (
	"context"
	"encoding/base64"
	"errors"
	"log"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ec2"
	"gitlab.com/deviosec/octp/internal/providers/models"
)

const (
	securityGroupName        = "allow-all-ingress"
	securityGroupDescription = "default security group for OCTP"
)

var (
	ErrNoAMIFound = errors.New("could not find any AMIs with the provided name")
)

// createInstance will try to create a instance with the desired parameters, if it fails with invalid group, it will
// try to see, if the security group is not created. If it is not created, it will create it and retry the function call.
// if the group however does exists, then it will return the original error
func (aw *awsInternal) createInstance(ctx context.Context, name, template, image string, SSHKeys []string, retryAfterGroupAdd bool) error {
	// get image id from name
	imageID, err := aw.getAMIIDByName(ctx, image)
	if err != nil {
		return err
	}

	if len(SSHKeys) > 1 {
		return ErrOnlyOneSSHKeyAllowed
	}

	instancesInput := ec2.RunInstancesInput{
		ImageId:          aws.String(imageID),
		SecurityGroupIds: []*string{aws.String(securityGroupName)},
		InstanceType:     aws.String(Type),
		MinCount:         aws.Int64(1),
		MaxCount:         aws.Int64(1),
	}

	if len(SSHKeys) > 0 {
		instancesInput.KeyName = aws.String(SSHKeys[0])
	}

	// if there is a template (cloud init, then actually use it)
	if len(template) > 0 {
		instancesInput.UserData = aws.String(base64.StdEncoding.EncodeToString([]byte(template)))
	}

	// create instance
	runResult, err := aw.svc.RunInstancesWithContext(ctx, &instancesInput)
	if err != nil {
		if !retryAfterGroupAdd && strings.Contains(err.Error(), "for parameter groupId is invalid") {
			secGroupExists, existsErr := aw.securityGroupExists(ctx, securityGroupName)
			if existsErr != nil {
				return existsErr
			}

			// group exists, then just return the previous error
			if secGroupExists {
				return err
			}

			// create the group as it does not exists
			if err := aw.securityGroupCreate(ctx); err != nil {
				return err
			}

			// try to add it again, now that the group is created
			return aw.createInstance(ctx, name, template, image, SSHKeys, true)
		}

		return err
	}

	// add a `Name` tag to it!
	_, err = aw.svc.CreateTagsWithContext(ctx, &ec2.CreateTagsInput{
		Resources: []*string{runResult.Instances[0].InstanceId},
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(name),
			},
		},
	})

	return err
}

// getAMIIDByName returns the ID of the AMI based on the specified name
func (aw *awsInternal) getAMIIDByName(ctx context.Context, name string) (string, error) {
	output, err := aw.svc.DescribeImagesWithContext(ctx, &ec2.DescribeImagesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("name"),
				Values: []*string{aws.String(name)},
			},
		},
	})
	if err != nil {
		return "", err
	}

	if len(output.Images) == 0 {
		return "", ErrNoAMIFound
	}

	// if len is bigger than 1, give warning about multiple images
	// but we will just take the first one none-the-less
	if len(output.Images) > 1 {
		log.Printf("Multiple AMIs matech the input name '%s', using the first one", name)
	}

	return aws.StringValue(output.Images[0].ImageId), nil
}

func (aw *awsInternal) securityGroupExists(ctx context.Context, name string) (bool, error) {
	input := ec2.DescribeSecurityGroupsInput{
		GroupNames: []*string{aws.String(securityGroupName)},
	}
	output, err := aw.svc.DescribeSecurityGroupsWithContext(ctx, &input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			// noop for InvalidGroup.NotFound
			case "InvalidGroup.NotFound":
				// anything else, return false and the error
			default:
				return false, err
			}
		}
	}

	return (len(output.SecurityGroups) > 0), nil
}

func (aw *awsInternal) securityGroupCreate(ctx context.Context) error {
	_, err := aw.svc.CreateSecurityGroup(&ec2.CreateSecurityGroupInput{
		GroupName:   aws.String(securityGroupName),
		Description: aws.String(securityGroupDescription),
		// VpcId defaults to whatever is default
		// VpcId:       aws.String(*vpcIDPtr),
	})
	if err != nil {
		return err
	}

	_, err = aw.svc.AuthorizeSecurityGroupIngress(&ec2.AuthorizeSecurityGroupIngressInput{
		GroupName: aws.String(securityGroupName),
		IpPermissions: []*ec2.IpPermission{
			(&ec2.IpPermission{}).
				SetIpProtocol("-1"). // all protocols
				SetFromPort(0).      // all ports
				SetToPort(0).        // all ports
				SetIpRanges([]*ec2.IpRange{
					{CidrIp: aws.String("0.0.0.0/0")}, // all IPv4 IPs
				}).SetIpv6Ranges([]*ec2.Ipv6Range{
				{CidrIpv6: aws.String("::/0")}, // all IPv6 IPs
			}),
		},
	})

	return err
}

// getInstance returns a instance with the specified name
func (aw *awsInternal) getInstance(ctx context.Context, name string) (*ec2.Instance, error) {
	instances, err := aw.listInstances(ctx)
	if err != nil {
		return nil, err
	}

	instance, ok := instances[name]
	if !ok {
		return nil, models.ErrPodNotFound
	}
	return instance, nil
}

// listInstances list all instances on aws ec2
func (aw *awsInternal) listInstances(ctx context.Context) (map[string]*ec2.Instance, error) {
	statusInput := ec2.DescribeInstancesInput{
		// TODO(eyJhb) we should be able to do more than this,
		// but this is OK for now (follow pages)
		// TODO(eyJhb) maybe we should tag all, and only list those?
		MaxResults: aws.Int64(1000),

		// We want to get everthing except terminated, as they
		// can stay on the list for up to an hour after
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("instance-state-name"),
				Values: []*string{
					aws.String(ec2.InstanceStateNamePending),
					aws.String(ec2.InstanceStateNameRunning),
					aws.String(ec2.InstanceStateNameShuttingDown),
					aws.String(ec2.InstanceStateNameStopping),
					aws.String(ec2.InstanceStateNameStopped),
					// aws.String(ec2.InstanceStateNameTerminated),
				},
			},
		},
	}

	output, err := aw.svc.DescribeInstancesWithContext(ctx, &statusInput)
	if err != nil {
		return nil, err
	}

	instances := make(map[string]*ec2.Instance)
	for _, reservation := range output.Reservations {
		for i, instance := range reservation.Instances {
			// default to the instanceId if nothing is found
			name := aws.StringValue(instance.InstanceId)
			for _, tag := range instance.Tags {
				if aws.StringValue(tag.Key) == "Name" {
					name = aws.StringValue(tag.Value)
				}
			}

			if instances[name] != nil {
				log.Printf("aws.listInstances: multiple instances with the same name")
				continue
			}

			instances[name] = reservation.Instances[i]
		}
	}

	return instances, nil
}

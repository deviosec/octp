package aws

import "github.com/aws/aws-sdk-go/aws/credentials"

type credentailsStore struct {
	AccessKeyID string
	SecretKey   string

	retrieved bool
}

func (cs *credentailsStore) Retrieve() (credentials.Value, error) {
	// just following the other implementations
	cs.retrieved = false

	// any logic in here that is used to retrieve the information

	cs.retrieved = true
	return credentials.Value{
		AccessKeyID:     cs.AccessKeyID,
		SecretAccessKey: cs.SecretKey,
	}, nil
}

func (cs *credentailsStore) RetrieveWithContext(credentials.Context) (credentials.Value, error) {
	return cs.Retrieve()
}

func (cs *credentailsStore) IsExpired() bool {
	return !cs.retrieved
}

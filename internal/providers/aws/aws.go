package aws

import (
	"context"
	"errors"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"gitlab.com/deviosec/octp/internal/providers/models"
)

const (
	Region       = "eu-central-1"
	Type         = "t2.micro"
	defaultImage = "debian-10-amd64-20201207-477"
)

var (
	ErrInvalidLink          = errors.New("the link provided should be formatted as AWS_ACCESS_KEY_ID:AWS_SECRET_KEY")
	ErrOnlyOneSSHKeyAllowed = errors.New("you can only specify a single SSH Key")
)

type awsInternal struct {
	svc *ec2.EC2
}

func New(link string) (*awsInternal, error) {
	// remove `aws://`
	link = link[6:]

	// split at `:`
	splitLink := strings.Split(link, ":")

	if len(splitLink) != 2 {
		return nil, ErrInvalidLink
	}
	accessKeyID := splitLink[0]
	secretKey := splitLink[1]

	// init session
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(Region),
		Credentials: credentials.NewCredentials(&credentailsStore{AccessKeyID: accessKeyID, SecretKey: secretKey}),
	})
	if err != nil {
		return nil, err
	}

	return &awsInternal{svc: ec2.New(sess)}, nil
}

func (aw *awsInternal) ListPods(ctx context.Context) ([]models.PRPod, error) {
	instances, err := aw.listInstances(ctx)
	if err != nil {
		return nil, err
	}

	var pods []models.PRPod
	for name, instance := range instances {
		pods = append(pods, models.PRPod{
			ID:   aws.StringValue(instance.InstanceId),
			Name: name,
		})
	}

	return pods, nil
}

func (aw *awsInternal) DestroyPod(ctx context.Context, name string) error {
	instance, err := aw.getInstance(ctx, name)
	if err != nil {
		// if we didn't find any droplet, then
		// do nothing as it does not exist and
		// cannot be destroyed
		if err == models.ErrPodNotFound {
			return nil
		}

		return err
	}

	input := ec2.TerminateInstancesInput{
		InstanceIds: []*string{
			instance.InstanceId,
		},
	}

	_, err = aw.svc.TerminateInstancesWithContext(ctx, &input)
	return err
}

// CreatePod will try to create a instance with the specified paramaters
// NOTE: If a template is specified, then the SSHKeys will not work/be added and should be done using the template
func (aw *awsInternal) CreatePod(ctx context.Context, name, template, image string, SSHKeys []string) error {
	return aw.createInstance(ctx, name, template, image, SSHKeys, false)
}

func (aw *awsInternal) PublicIPPod(ctx context.Context, name string, IPv6 bool) (string, error) {
	instance, err := aw.getInstance(ctx, name)
	if err != nil {
		return "", err
	}

	var ip string
	if IPv6 {
		ip = aws.StringValue(instance.Ipv6Address)
	} else {
		ip = aws.StringValue(instance.PublicIpAddress)
	}
	if ip == "" {
		return "", models.ErrNoIP
	}

	return ip, nil
}

func (aw *awsInternal) PrivateIPPod(ctx context.Context, name string) (string, error) {
	instance, err := aw.getInstance(ctx, name)
	if err != nil {
		return "", err
	}

	ip := aws.StringValue(instance.PrivateIpAddress)
	if ip == "" {
		return "", models.ErrNoIP
	}

	return ip, nil
}

func (aw *awsInternal) PodExists(ctx context.Context, name string) (bool, error) {
	_, err := aw.getInstance(ctx, name)
	return (err != models.ErrPodNotFound), nil
}

func (aw *awsInternal) DefaultImage() string {
	return defaultImage
}

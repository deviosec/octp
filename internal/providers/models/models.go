package models

import "errors"

// errors
var (
	ErrPodNotFound      = errors.New("could not find the pod specified")
	ErrDuplicatePodName = errors.New("pod with the name already exists")
	ErrNoIP             = errors.New("pod did not have the requested IP")
)

type PRPod struct {
	Name string
	ID   string
}

package hetzner

import (
	"context"
	"errors"
	"strconv"

	"github.com/hetznercloud/hcloud-go/hcloud"
	"gitlab.com/deviosec/octp/internal/providers/models"
)

const (
	Region = "hel1"
	// POD_TYPE     = "s-4vcpu-8gb"
	Type         = "cpx11"
	defaultImage = "debian-11"
)

type Hetzner struct {
	Client *hcloud.Client

	networkID int
}

func New(link string) (*Hetzner, error) {
	c := hcloud.NewClient(hcloud.WithToken(link[10:]))

	// try to get datacenters, just to see if the client works
	h := &Hetzner{Client: c}
	if err := h.ping(); err != nil {
		return nil, err
	}

	return h, nil
}

func (h *Hetzner) ListPods(ctx context.Context) ([]models.PRPod, error) {
	servers, err := h.getServers(ctx)
	if err != nil {
		return nil, err
	}

	var serversList []models.PRPod
	for _, server := range servers {
		serversList = append(serversList, models.PRPod{
			Name: server.Name,
			ID:   strconv.Itoa(server.ID),
		})
	}

	return serversList, nil
}

func (h *Hetzner) DestroyPod(ctx context.Context, name string) error {
	s, err := h.getServer(ctx, name)
	if err == models.ErrPodNotFound {
		return nil
	} else if err != nil {
		return err
	}

	_, err = h.Client.Server.Delete(ctx, s)

	return err
}

func (h *Hetzner) CreatePod(ctx context.Context, name, template, image string, SSHKeys []string) error {
	// create the network first.
	// networkCreate will check if the network
	// already exists, and will cache the
	// networkID
	networkID, err := h.createNetwork(ctx)
	if err != nil {
		return err
	}

	opts := hcloud.ServerCreateOpts{
		Name:       name,
		Location:   &hcloud.Location{Name: Region},
		ServerType: &hcloud.ServerType{Name: Type},
		Image:      &hcloud.Image{Name: image},
		Networks:   []*hcloud.Network{&hcloud.Network{ID: networkID}},
		UserData:   template,
	}

	for _, sshkey := range SSHKeys {
		// find key
		keyID, found, err := h.sshKey(ctx, sshkey)
		if err != nil {
			return err
		}
		if !found {
			return errors.New("no ssh key matching the provided identifier")
		}

		opts.SSHKeys = append(opts.SSHKeys, &hcloud.SSHKey{
			ID: keyID,
		})
	}

	_, _, err = h.Client.Server.Create(ctx, opts)
	return err
}

func (h *Hetzner) PublicIPPod(ctx context.Context, name string, IPv6 bool) (string, error) {
	return h.getIP(ctx, name, IPv6, true)
}

func (h *Hetzner) PrivateIPPod(ctx context.Context, name string) (string, error) {
	return h.getIP(ctx, name, false, false)
}

func (h *Hetzner) PodExists(ctx context.Context, name string) (bool, error) {
	_, err := h.getServer(ctx, name)
	if err == models.ErrPodNotFound {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

func (h *Hetzner) DefaultImage() string {
	return defaultImage
}

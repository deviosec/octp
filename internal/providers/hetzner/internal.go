package hetzner

import (
	"context"
	"net"
	"strings"

	"github.com/hetznercloud/hcloud-go/hcloud"
	"gitlab.com/deviosec/octp/internal/providers/models"
)

const (
	networkName = "octp-private"
	networkCIDR = "10.0.0.0/16"
)

// createNetwork creates a OCTP private network, if it does not already exists
func (h *Hetzner) createNetwork(ctx context.Context) (int, error) {
	// check if network already exists, if it does, then just
	// return nil
	if networkID, err := h.networkExists(ctx); err != nil {
		return 0, err
	} else if networkID != 0 {
		return networkID, nil
	}

	// parse networkCIDR
	_, ipnet, err := net.ParseCIDR(networkCIDR)
	if err != nil {
		return 0, err
	}

	opts := hcloud.NetworkCreateOpts{
		Name:    networkName,
		IPRange: ipnet,
		Subnets: []hcloud.NetworkSubnet{
			hcloud.NetworkSubnet{
				IPRange:     ipnet,
				NetworkZone: hcloud.NetworkZoneEUCentral,
				Type:        hcloud.NetworkSubnetTypeServer,
			},
		},
	}

	network, _, err := h.Client.Network.Create(ctx, opts)
	if err != nil {
		return 0, err
	}

	h.networkID = network.ID
	return network.ID, nil
}

// networkExists checks if a private network for OCTP already exists
func (h *Hetzner) networkExists(ctx context.Context) (int, error) {
	if h.networkID != 0 {
		return h.networkID, nil
	}

	networks, _, err := h.Client.Network.List(ctx, hcloud.NetworkListOpts{Name: networkName})
	if err != nil {
		return 0, err
	}

	if len(networks) > 0 {
		h.networkID = networks[0].ID
		return h.networkID, nil
	}

	return 0, nil
}

// sshKey finds the first key that matches the identifier by name or fingerprint
func (h *Hetzner) sshKey(ctx context.Context, identifier string) (int, bool, error) {
	keys, err := h.Client.SSHKey.All(ctx)
	if err != nil {
		return 0, false, err
	}

	for _, key := range keys {
		if strings.Contains(key.Fingerprint, identifier) {
			return key.ID, true, nil
		} else if strings.Contains(key.Name, identifier) {
			return key.ID, true, nil
		}
	}

	return 0, false, nil
}

// getIP will return the IP of the server, which can be either public or private IP.
// If there are multiple private networks attached, it will just return the first private network IP
func (h *Hetzner) getIP(ctx context.Context, name string, IPv6, public bool) (string, error) {
	s, err := h.getServer(ctx, name)
	if err != nil {
		return "", err
	}

	var ip string
	if public && IPv6 {
		// TODO(eyJhb): just add an 1 after, so that it's the
		// first IP in the range
		ip = s.PublicNet.IPv6.IP.String() + "1"
	} else if public && !IPv6 {
		ip = s.PublicNet.IPv4.IP.String()
	} else if !public && !IPv6 && len(s.PrivateNet) > 0 {
		ip = s.PrivateNet[0].IP.String()
	}

	if ip == "" {
		return "", models.ErrNoIP
	}

	return ip, nil
}

// getServer returns a specific server based on name
func (h *Hetzner) getServer(ctx context.Context, name string) (*hcloud.Server, error) {

	server, _, err := h.Client.Server.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}

	if server == nil {
		return nil, models.ErrPodNotFound
	}

	return server, nil

}

// getServers returns a map of all the servers
func (h *Hetzner) getServers(ctx context.Context) (map[string]*hcloud.Server, error) {
	// create a list to hold our droplets
	serverMap := make(map[string]*hcloud.Server)

	// create options. initially, these will be blank
	servers, err := h.Client.Server.All(ctx)
	if err != nil {
		return nil, err
	}

	for _, server := range servers {
		serverMap[server.Name] = server
	}

	return serverMap, nil
}

// ping is used to check if the API key works
func (h *Hetzner) ping() error {
	_, _, err := h.Client.Datacenter.List(context.Background(), hcloud.DatacenterListOpts{})
	return err
}

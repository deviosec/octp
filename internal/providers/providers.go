package providers

import (
	"context"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/deviosec/octp/internal/providers/aws"
	"gitlab.com/deviosec/octp/internal/providers/do"
	"gitlab.com/deviosec/octp/internal/providers/hetzner"
	"gitlab.com/deviosec/octp/internal/providers/mock"
	"gitlab.com/deviosec/octp/internal/providers/models"
)

var (
	ErrNoProviderFound = errors.New("Failed to find any provider")
)

type Provider interface {
	ListPods(ctx context.Context) ([]models.PRPod, error)
	DestroyPod(ctx context.Context, name string) error
	CreatePod(ctx context.Context, name, template, image string, SSHKeys []string) error
	PublicIPPod(ctx context.Context, name string, IPv6 bool) (string, error)
	PrivateIPPod(ctx context.Context, name string) (string, error)
	PodExists(ctx context.Context, name string) (bool, error)
	DefaultImage() string
}

func New(link string) (Provider, error) {
	if strings.HasPrefix(link, "do://") {
		return do.New(link)
	} else if strings.HasPrefix(link, "aws://") {
		return aws.New(link)
	} else if strings.HasPrefix(link, "hetzner://") {
		return hetzner.New(link)
	} else if strings.HasPrefix(link, "mock://") {
		return mock.New(link)
	}

	return nil, errors.Wrapf(ErrNoProviderFound, "could not finder provider for '%s'", link)
}

package do

import (
	"context"
	"strconv"

	"github.com/digitalocean/godo"
	"github.com/pkg/errors"
	"gitlab.com/deviosec/octp/internal/providers/models"
	"golang.org/x/oauth2"
)

const (
	Region = "fra1"
	// POD_TYPE     = "s-4vcpu-8gb"
	Type         = "s-2vcpu-2gb"
	defaultImage = "debian-10-x64"
)

type DoStruct struct {
	client *godo.Client
}

// ----- INIT -----
type TokenSource struct {
	AccessToken string
}

func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

func New(doToken string) (*DoStruct, error) {
	// strip first 5 chars do://
	tokenSource := &TokenSource{
		AccessToken: doToken[5:],
	}

	oauthClient := oauth2.NewClient(context.Background(), tokenSource)
	return &DoStruct{client: godo.NewClient(oauthClient)}, nil
}

// !!!!! INIT !!!!!

func (do *DoStruct) DefaultImage() string {
	return defaultImage
}

// taken directly from DigitalOcean Godo Github
func (do *DoStruct) ListPods(ctx context.Context) ([]models.PRPod, error) {
	// create a list to hold our droplets
	list := []models.PRPod{}

	// create options. initially, these will be blank
	opt := &godo.ListOptions{PerPage: 1000}
	for {
		droplets, resp, err := do.client.Droplets.List(ctx, opt)
		if err != nil {
			return nil, errors.Wrap(err, "do.client failed to get list")
		}

		// append the current page's droplets to our list
		for _, d := range droplets {
			list = append(list, models.PRPod{Name: d.Name, ID: strconv.Itoa(d.ID)})
		}

		// if we are at the last page, break out the for loop
		if resp.Links == nil || resp.Links.IsLastPage() {
			break
		}

		page, err := resp.Links.CurrentPage()
		if err != nil {
			return nil, errors.Wrap(err, "do.client failed to get current page")
		}

		// set the page we want for the next request
		opt.Page = page + 1
	}

	return list, nil
}

func (do *DoStruct) CreatePod(ctx context.Context, name, template, image string, sshKeys []string) error {
	pod := models.PRPod{}

	// check if already exists
	if exists, err := do.PodExists(ctx, name); err != nil || exists {
		if err != nil {
			return err
		}
		return models.ErrDuplicatePodName
	}

	createRequest := &godo.DropletCreateRequest{
		Name:              name,
		Region:            Region,
		Size:              Type,
		PrivateNetworking: true,
		UserData:          template,
	}

	if len(sshKeys) > 0 {
		var doSSHKeys []godo.DropletCreateSSHKey
		for _, key := range sshKeys {
			doSSHKeys = append(doSSHKeys, godo.DropletCreateSSHKey{
				Fingerprint: key,
			})
		}

		createRequest.SSHKeys = doSSHKeys
	}

	img, err := strconv.Atoi(image)
	if err == nil {
		createRequest.Image = godo.DropletCreateImage{ID: img}
	} else {
		createRequest.Image = godo.DropletCreateImage{Slug: image}
	}

	newPod, _, err := do.client.Droplets.Create(ctx, createRequest)
	if err != nil {
		return errors.Wrap(err, "do.client failed to create droplet")
	}

	pod.Name = name
	pod.ID = strconv.Itoa(newPod.ID)

	return nil
}

func (do *DoStruct) DestroyPod(ctx context.Context, name string) error {
	d, err := do.getDroplet(ctx, name)
	if err != nil {
		// if we didn't find any droplet, then
		// do nothing as it does not exist and
		// cannot be destroyed
		if err == models.ErrPodNotFound {
			return nil
		}

		return err
	}

	// convert string to int
	dropletID, err := strconv.Atoi(d.ID)
	if err != nil {
		return errors.WithStack(err)
	}

	if _, err := do.client.Droplets.Delete(ctx, dropletID); err != nil {
		return errors.Wrap(err, "do.client failed to delete droplet")
	}

	return nil
}

func (do *DoStruct) PublicIPPod(ctx context.Context, name string, IPv6 bool) (string, error) {
	return do.getIP(ctx, name, IPv6, true)

}

func (do *DoStruct) PrivateIPPod(ctx context.Context, name string) (string, error) {
	return do.getIP(ctx, name, false, false)
}

// helper functions
func (do *DoStruct) getIP(ctx context.Context, name string, IPv6 bool, public bool) (string, error) {
	d, err := do.getDroplet(ctx, name)
	if err != nil {
		return "", err
	}

	dropletID, err := strconv.Atoi(d.ID)
	if err != nil {
		return "", errors.WithStack(err)
	}

	drop, _, err := do.client.Droplets.Get(ctx, dropletID)
	if err != nil {
		return "", errors.Wrap(err, "do.client could not get droplet")
	}

	var ip string
	if public && IPv6 {
		ip, err = drop.PublicIPv6()
	} else if public && !IPv6 {
		ip, err = drop.PublicIPv4()
	} else if !public && !IPv6 {
		ip, err = drop.PrivateIPv4()
	}

	if err != nil {
		return "", errors.Wrap(err, "do.client failed to get ip")
	}

	if ip == "" {
		return "", models.ErrNoIP
	}

	return ip, nil

}

func (do *DoStruct) getDroplet(ctx context.Context, name string) (models.PRPod, error) {
	pod := models.PRPod{}

	droplets, err := do.ListPods(ctx)
	if err != nil {
		return pod, err
	}

	for _, d := range droplets {
		if d.Name == name {
			return d, nil
		}
	}

	return pod, models.ErrPodNotFound
}

func (do *DoStruct) PodExists(ctx context.Context, name string) (bool, error) {
	_, err := do.getDroplet(ctx, name)
	if err == models.ErrPodNotFound {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

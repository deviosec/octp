package validate

import (
	"fmt"
	"regexp"
)

type Error struct {
	Key   string
	Value interface{}
	Msg   string
}

func (ve *Error) Error() string {
	return fmt.Sprintf("Key '%s' with value '%v' has error: %s", ve.Key, ve.Value, ve.Msg)
}

func String(key, v string, re *regexp.Regexp) error {
	if !re.MatchString(v) {
		return &Error{
			Key:   key,
			Value: v,
			Msg:   fmt.Sprintf("did not match regex: %s", re.String()),
		}
	}

	return nil
}

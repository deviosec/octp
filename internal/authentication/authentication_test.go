package authentication

import (
	"context"
	"regexp"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/deviosec/octp/internal/simplecrypto"
)

type InMemStorage struct {
	tokens map[string]TokenInfo
	sync.RWMutex
}

func (ims *InMemStorage) GetTokens(ctx context.Context) (map[string]TokenInfo, error) {
	ims.RLock()
	defer ims.RUnlock()

	return ims.tokens, nil
}

func (ims *InMemStorage) AddToken(ctx context.Context, token string, tokenInfo TokenInfo) error {
	ims.Lock()
	defer ims.Unlock()

	ims.tokens[token] = tokenInfo
	return nil
}

func (ims *InMemStorage) RemoveToken(ctx context.Context, token string) error {
	ims.Lock()
	defer ims.Unlock()

	delete(ims.tokens, token)
	return nil
}

// context that we just use default
var ctx = context.TODO()

func HelperHashToken(t *testing.T, token string) string {
	token, err := simplecrypto.HashToken(token)
	if err != nil {
		t.Fatalf("Could not generate hashed token for %s", token)
	}

	return token
}

func TestValidate(t *testing.T) {
	tt := []struct {
		name   string
		store  InMemStorage
		tokens map[string]error
	}{
		{
			name: "default",
			store: InMemStorage{
				tokens: map[string]TokenInfo{
					HelperHashToken(t, "token1"): TokenInfo{},
					HelperHashToken(t, "token3"): TokenInfo{},
				},
			},
			tokens: map[string]error{
				"token1": nil,
				"token2": ErrInvalidToken,
				"token3": nil,
			},
		},
	}
	for i := range tt {
		// so that we do not copy the lock
		tc := &tt[i]

		t.Run(tc.name, func(t *testing.T) {
			// init auth service
			as := AuthService{
				Store: &tc.store,
			}

			// check for expected output
			for token, expErr := range tc.tokens {
				_, err := as.Validate(ctx, token)
				assert.Equal(t, expErr, err)
			}

		})
	}
}

func TestAddTokenValidate(t *testing.T) {
	const (
		PermNoAccess Permisson = iota
		PermReadAccess
		PermWriteAccess
	)

	tt := []struct {
		name   string
		tokens []TokenInfo
	}{
		{
			name: "default",
			tokens: []TokenInfo{
				TokenInfo{
					Perm:    PermNoAccess,
					Comment: "comment1",
				},
				TokenInfo{
					Perm:    PermReadAccess,
					Comment: "comment2",
				},
				TokenInfo{
					Perm:    PermWriteAccess,
					Comment: "comment3",
				},
			},
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// init auth service
			as := AuthService{
				Store: &InMemStorage{
					tokens: make(map[string]TokenInfo),
				},
			}

			for _, ti := range tc.tokens {
				// add token
				id, token, err := as.CreateToken(ctx, ti.Perm, ti.Comment)
				assert.NoError(t, err)
				assert.True(t, len(token) > 10)

				// validate it again, to ensure correct token
				tokenInfo, err := as.Validate(ctx, token)
				assert.Equal(t, nil, err)
				assert.Equal(t, ti.Perm, tokenInfo.Perm)
				assert.Equal(t, ti.Comment, tokenInfo.Comment)
				assert.Regexp(t, regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$"), id)
			}

		})
	}
}

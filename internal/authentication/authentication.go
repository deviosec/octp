package authentication

import (
	"context"
	"errors"

	"gitlab.com/deviosec/octp/internal/simplecrypto"
)

const (
	DefaultTokenSize = 100
)

var (
	ErrInvalidToken = errors.New("the provided token is not valid")
)

// used to setup permissions
type Permisson uint8

type TokenInfo struct {
	ID      string    `json:"id"`
	Perm    Permisson `json:"perm"`
	Comment string    `json:"comment"`
}

type Storage interface {
	GetTokens(ctx context.Context) (map[string]TokenInfo, error)
	AddToken(ctx context.Context, token string, tokenInfo TokenInfo) error
	RemoveToken(ctx context.Context, token string) error
}

type AuthService struct {
	Store Storage
}

func (a *AuthService) Validate(ctx context.Context, token string) (TokenInfo, error) {
	tokens, err := a.Store.GetTokens(ctx)
	if err != nil {
		return TokenInfo{}, err
	}

	hashedToken, err := simplecrypto.HashToken(token)
	if err != nil {
		return TokenInfo{}, err
	}

	if v, ok := tokens[hashedToken]; ok {
		return v, nil
	}

	return TokenInfo{}, ErrInvalidToken
}

func (a *AuthService) Remove(ctx context.Context, tokenHashed string) error {
	return a.Store.RemoveToken(ctx, tokenHashed)
}

// CreateToken return the token ID followed by the token
func (a *AuthService) CreateToken(ctx context.Context, perm Permisson, comment string) (string, string, error) {
	id := simplecrypto.ID()
	token := simplecrypto.Token(DefaultTokenSize)

	// init token info
	tokenInfo := TokenInfo{
		ID:      id,
		Perm:    perm,
		Comment: comment,
	}

	hashedToken, err := simplecrypto.HashToken(token)
	if err != nil {
		return "", "", err
	}

	// try to add it, with the hashed value
	if err = a.Store.AddToken(ctx, hashedToken, tokenInfo); err != nil {
		return "", "", err
	}

	return tokenInfo.ID, token, nil
}

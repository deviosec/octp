package models

type VirtualState uint8

const (
	VirtualStateUnknown VirtualState = iota
	VirtualStateStopped
	VirtualStateRunning
	VirtualStateUnhealthy
	VirtualStateHealthy

	VirtualSecoptAllowDisableASLR = "allow-disable-aslr"
)

// TODO(eyJhb): ensure that all virtual names are unique
type Lab struct {
	Name     string    `json:"name"`
	Virtuals []Virtual `json:"virtuals"`
	Networks []Network `json:"networks"`
}

type Virtual struct {
	ID       string          `json:"id"`
	Name     string          `json:"name"`
	HostName string          `json:"hostname"`
	Image    string          `json:"image"`
	Sha256   string          `json:"sha256"`
	Mounts   []Mount         `json:"mounts"`
	Ports    map[string]Port `json:"ports"`
	Envs     []string        `json:"envs"`
	MemoryMB uint16          `json:"memorymb"`
	CPUs     uint8           `json:"cpus"`

	// manage capabilities
	// DropCapabilities []string `json:"drop_capabilities"`
	// AddCapabilities  []string `json:"add_capabilities"`

	// a list of string values, which will change the security options of the virtual.
	// a valid option could be [ "allow-aslr" ], to allow for changing ASLR with `setarch`.
	SecurityOptions []string `json:"security_options"`

	State VirtualState `json:"state"`

	Type string `json:"type"`
}

// TODO(eyJhb) temp sholud be replaced
func SetDefaultsVirtual(v Virtual) Virtual {
	if v.HostName == "" {
		v.HostName = v.Name
	}

	for i, p := range v.Ports {
		if p.Protocol == "" {
			p.Protocol = "tcp"
			v.Ports[i] = p
		}
	}

	return v
}

type Mount struct {
	Host    string `json:"host"`
	Guest   string `json:"guest"`
	Options string `json:"options"`
}

type Port struct {
	HostIP   string `json:"hostip"`
	Host     int    `json:"host"`
	Guest    int    `json:"guest"`
	Protocol string `json:"protocol"`
}

type Image struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Size    uint64 `json:"size"`
	Sha256  string `json:"sha256"`
	Created int64  `json:"created"`
}

type Network struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Connections []string `json:"connections"`

	Type string `json:"type"`
}

type Auth struct {
	Username string `json:"username"`
	Password string `json:"password"`

	// if token is specified, then username and Password
	// shall be ignored
	Token string `json:"token"`
}

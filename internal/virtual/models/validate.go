package models

import (
	"fmt"
	"regexp"

	"github.com/hashicorp/go-multierror"
	"gitlab.com/deviosec/octp/internal/validate"
)

var (
	regPath       = regexp.MustCompile(`^[A-z\/0-9\-\.]+$`)
	regName       = regexp.MustCompile(`^[a-z0-9\-]+$`)
	regPortName   = regexp.MustCompile(`^[A-Z0-9]+$`)
	regIPv4       = regexp.MustCompile(`^\d{1,3}(?:\.\d{1,3}){3}$`)
	regProtocol   = regexp.MustCompile(`^(tcp|udp)$`)
	regMntOptions = regexp.MustCompile(`^(r|rw)$`)
	regEnv        = regexp.MustCompile(`^[A-z0-9_-]+=.*$`)
)

var (
	securityOptions = []string{
		VirtualSecoptAllowDisableASLR,
	}
)

func (l Lab) Validate() error {
	var err *multierror.Error

	// validate name - only if set
	if l.Name != "" {
		err = multierror.Append(err, validate.String("lab.name", l.Name, regName))
	}

	// validate all Virtuals, and make map for later
	instNames := make(map[string]bool)
	for _, inst := range l.Virtuals {
		err = multierror.Append(err, inst.Validate())
		err = multierror.Append(err, validate.String("virtual.name", inst.Name, regName))

		instNames[inst.Name] = true
	}

	// validate all networks and ensure that the
	// Virtuals exists
	for _, net := range l.Networks {
		err = multierror.Append(err, net.Validate())

		for _, con := range net.Connections {
			// not declared as a Virtual, give error
			if !instNames[con] {
				err = multierror.Append(err, &validate.Error{Key: net.Name, Value: con, Msg: "network uses virtual which has not been declared"})
			}
		}

	}

	return err.ErrorOrNil()
}

func (v Virtual) Validate() error {
	var err *multierror.Error

	err = multierror.Append(err, validate.String("virtual.name", v.Name, regName))

	// images are close to paths, as they have the
	// same format, at least in docker
	err = multierror.Append(err, validate.String("image", v.Image, regPath))

	for _, mnt := range v.Mounts {
		err = multierror.Append(err, mnt.Validate())
	}

	for portName, port := range v.Ports {
		err = multierror.Append(err, validate.String(fmt.Sprintf("port.%s", portName), portName, regPortName))
		err = multierror.Append(err, port.Validate())
	}
	for i, env := range v.Envs {
		err = multierror.Append(err, validate.String(fmt.Sprintf("env.%d", i), env, regEnv))
	}

	// validate SecurityOptions
	if len(v.SecurityOptions) > 0 {
		for _, secopt := range v.SecurityOptions {
			if !stringSliceContains(secopt, securityOptions) {
				err = multierror.Append(err, &validate.Error{Key: "securityoption", Value: secopt, Msg: "option is not valid"})
			}
		}
	}

	// validate type better than this
	// if v.Type == "" {
	// 	errors = append(errors, "Type cannot be empty")
	// }

	return err.ErrorOrNil()
}

func (m Mount) Validate() error {
	var err *multierror.Error

	err = multierror.Append(err, validate.String("host", m.Host, regPath))
	err = multierror.Append(err, validate.String("guest", m.Host, regPath))
	err = multierror.Append(err, validate.String("options", m.Options, regMntOptions))

	return err.ErrorOrNil()
}

func (p Port) Validate() error {
	var err *multierror.Error

	// HostIP
	if p.HostIP != "" {
		err = multierror.Append(err, validate.String(fmt.Sprintf("p.HostIP - '%s'", p.HostIP), p.HostIP, regIPv4))
	}

	// Host (port)
	if p.Host < 0 || p.Host > 65535 {
		err = multierror.Append(err, &validate.Error{Key: "p.Host", Value: p.Host, Msg: "should be between 1 and 65535"})
	}

	// Guest (port)
	if p.Guest <= 0 || p.Guest > 65535 {
		err = multierror.Append(err, &validate.Error{Key: "p.Guest", Value: p.Guest, Msg: "should be between 1 and 65535"})
	}

	// Protocol
	err = multierror.Append(err, validate.String(fmt.Sprintf("Port.Protocol- '%s'", p.Protocol), p.Protocol, regProtocol))

	return err.ErrorOrNil()
}

func (n Network) Validate() error {
	var err *multierror.Error

	// do not validate id, as we control that anyways
	err = multierror.Append(err, validate.String("network.name", n.Name, regName))
	for i, con := range n.Connections {
		err = multierror.Append(err, validate.String(fmt.Sprintf("Connections.%d", i), con, regName))
	}

	return err.ErrorOrNil()
}

func stringSliceContains(value string, in []string) bool {
	for _, val := range in {
		if value == val {
			return true
		}
	}

	return false
}

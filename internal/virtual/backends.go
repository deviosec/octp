package virtual

import (
	"strings"
)

type BackendType int

const (
	BackendUnknown BackendType = iota
	BackendDocker
)

func (b BackendType) names() []string {
	return []string{
		"unknown",
		"docker",
	}
}

func (b BackendType) prefixes() []string {
	return []string{
		"UNK-",
		"DOCKER-",
	}
}

func (b BackendType) String() string {
	return b.names()[b]
}

func (b BackendType) Prefix() string {
	return b.prefixes()[b]
}

func (b BackendType) stripPrefix(v string) string {
	return strings.TrimPrefix(v, b.Prefix())
}

func StripPrefix(v string) string {
	b := GuessBackend(v)
	return b.stripPrefix(v)
}

func GuessBackend(value string) BackendType {
	// default to docker
	if value == "" {
		return BackendDocker
	}

	// create unknown backnend to use
	unk := BackendUnknown
	for i, prefix := range unk.prefixes() {
		if strings.HasPrefix(value, prefix) {
			return BackendType(i)
		}
	}

	for i, name := range unk.names() {
		if strings.HasPrefix(value, name) {
			return BackendType(i)
		}
	}

	return unk
}

package virtual

import (
	"context"
	"errors"

	"github.com/rs/zerolog/log"
)

var (
	ErrOneOrMoreGCErrors = errors.New("one or more backends returned a error while doing GC")
)

func (v *virtualManager) GC(ctx context.Context) error {
	var errors bool

	if err := v.GCVirtuals(ctx); err != nil {
		errors = true
	}
	if err := v.GCNetworks(ctx); err != nil {
		errors = true
	}

	if errors {
		return ErrOneOrMoreGCErrors
	}

	return nil
}

func (v *virtualManager) GCVirtuals(ctx context.Context) error {
	var errors bool
	for k, vb := range v.virtBackends {
		if err := vb.GCVirtuals(ctx); err != nil {
			errors = true
			log.Error().Err(err).Str("backend", k).Msg("failed to run GC for virtual")
		}
	}

	if errors {
		return ErrOneOrMoreGCErrors
	}

	return nil
}

func (v *virtualManager) GCNetworks(ctx context.Context) error {
	var errors bool
	for k, nb := range v.netBackends {
		if err := nb.GCNetworks(ctx); err != nil {
			errors = true
			log.Error().Err(err).Str("backend", k).Msg("failed to run GC for network")
		}
	}

	if errors {
		return ErrOneOrMoreGCErrors
	}

	return nil
}

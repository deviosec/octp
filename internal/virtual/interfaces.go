package virtual

import (
	"context"
	"io"

	"gitlab.com/deviosec/octp/internal/virtual/models"
)

type VirtualManager interface {
	// basic operations
	Create(ctx context.Context, conf models.Virtual) (string, error)
	Get(ctx context.Context, id string) (models.Virtual, error)
	Exists(ctx context.Context, id string) (bool, error)
	// deep = false allows for only setting id, name + image, if deep is true
	// then the models.Virtual should be filled. Some information might be lost,
	// like the port names etc. as that cannot be stored easily
	List(ctx context.Context, backend BackendType, deep bool) ([]models.Virtual, error)
	Destroy(ctx context.Context, id string) error
	Start(ctx context.Context, id string) error
	Stop(ctx context.Context, id string) error

	// image options
	Images(ctx context.Context, backend BackendType) ([]models.Image, error)
	LoadImage(ctx context.Context, backend BackendType, name string, tarFile io.Reader, output io.Writer) error
	BuildImage(ctx context.Context, backend BackendType, name, entrypoint string, tarFile io.Reader, output io.Writer) error
	DestroyImage(ctx context.Context, backend BackendType, name string) error
	PullImage(ctx context.Context, backend BackendType, image string, auth models.Auth) error
	PushImage(ctx context.Context, backend BackendType, image, server string, auth models.Auth) error

	// network operations
	CreateNetwork(ctx context.Context, net models.Network) (string, error)
	DestroyNetwork(ctx context.Context, name string) error
	Connect(ctx context.Context, networkID, virtualID, alias string) error
	Disconnect(ctx context.Context, networkID, virtualID string) error
	ConnectPublic(ctx context.Context, virtualID string) error
	DisconnectPublic(ctx context.Context, virtualID string) error

	// lab operations
	CreateLab(ctx context.Context, l *models.Lab) error
	DestroyLab(ctx context.Context, l *models.Lab) error
	StartLab(ctx context.Context, l models.Lab) error
	StopLab(ctx context.Context, l models.Lab) error
	PopulateLab(ctx context.Context, l models.Lab) (models.Lab, error)

	// garbage collect - everything
	GC(ctx context.Context) error
	GCVirtuals(ctx context.Context) error
	GCNetworks(ctx context.Context) error
}

type VirtualBackend interface {
	// basic operations
	Create(ctx context.Context, conf models.Virtual) (string, error)
	Get(ctx context.Context, id string) (models.Virtual, error)
	Exists(ctx context.Context, id string) (bool, error)
	// deep = false allows for only setting id, name + image, if deep is true
	// then the models.Virtual should be filled
	List(ctx context.Context, deep bool) ([]models.Virtual, error)
	Destroy(ctx context.Context, id string) error
	Start(ctx context.Context, id string) error
	Stop(ctx context.Context, id string) error

	// image operations
	Images(ctx context.Context) ([]models.Image, error)
	LoadImage(ctx context.Context, name string, tarFile io.Reader, output io.Writer) error
	BuildImage(ctx context.Context, name, entrypoint string, tarFile io.Reader, output io.Writer) error
	DestroyImage(ctx context.Context, name string) error
	PullImage(ctx context.Context, image string, auth models.Auth) error
	PushImage(ctx context.Context, image, server string, auth models.Auth) error

	// network operations
	ConnectNetwork(ctx context.Context, networkID, virtualID, name string) error
	DisconnectNetwork(ctx context.Context, networkID, virtualID string) error
	ConnectNetworkPublic(ctx context.Context, virtualID string) error
	DisconnectNetworkPublic(ctx context.Context, virtualID string) error

	// garbage collect - removed stopped things
	// virtuals, images, volumes
	GCVirtuals(ctx context.Context) error
}

type NetworkBackend interface {
	CreateNetwork(ctx context.Context, name string) (string, error)
	DestroyNetwork(ctx context.Context, id string) error
	ListNetworks(ctx context.Context) ([]models.Network, error)

	// garbage collect - remove unused networks
	GCNetworks(ctx context.Context) error
}

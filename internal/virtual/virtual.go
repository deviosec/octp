package virtual

import (
	"context"
	"errors"

	"gitlab.com/deviosec/octp/internal/virtual/docker"
)

type virtualManager struct {
	virtBackends map[string]VirtualBackend
	netBackends  map[string]NetworkBackend
}

func New() VirtualManager {
	return &virtualManager{
		virtBackends: make(map[string]VirtualBackend),
		netBackends:  make(map[string]NetworkBackend),
	}
}

func (v *virtualManager) getNetBackend(ctx context.Context, cb BackendType) (NetworkBackend, error) {
	// todo(eyJhb) fix this, and make a network backend thingy
	_ = cb

	// create docker backend
	back, err := docker.New()
	if err != nil {
		return nil, err
	}

	v.netBackends["docker"] = NetworkBackend(back)
	return v.netBackends["docker"], nil
}

func (v *virtualManager) getVirtBackend(ctx context.Context, cb BackendType) (VirtualBackend, error) {
	if cb <= BackendUnknown {
		return nil, errors.New("no backend found")
	}

	if b := v.virtBackends[cb.String()]; b != nil {
		return b, nil
	}

	if cb == BackendDocker {
		back, err := docker.New()
		if err != nil {
			return nil, err
		}

		v.virtBackends[cb.String()] = back
		return back, nil
	}

	return nil, errors.New("could not find any backend for this")
}

package virtual

import (
	"context"
	"fmt"
	"io"

	"gitlab.com/deviosec/octp/internal/virtual/models"
)

func (v *virtualManager) Images(ctx context.Context, b BackendType) ([]models.Image, error) {
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return nil, err
	}

	return vb.Images(ctx)
}

func (v *virtualManager) LoadImage(ctx context.Context, b BackendType, name string, tarFile io.Reader, output io.Writer) error {
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	return vb.LoadImage(ctx, name, tarFile, output)
}

func (v *virtualManager) BuildImage(ctx context.Context, b BackendType, name, entrypoint string, tarFile io.Reader, output io.Writer) error {
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	return vb.BuildImage(ctx, name, entrypoint, tarFile, output)
}

func (v *virtualManager) DestroyImage(ctx context.Context, b BackendType, name string) error {
	fmt.Println("Destroyngi this image virtman")
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	return vb.DestroyImage(ctx, name)
}

func (v *virtualManager) PullImage(ctx context.Context, b BackendType, image string, auth models.Auth) error {
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	return vb.PullImage(ctx, image, auth)
}

func (v *virtualManager) PushImage(ctx context.Context, b BackendType, image, server string, auth models.Auth) error {
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	return vb.PushImage(ctx, image, server, auth)
}

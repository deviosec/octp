package virtual

import (
	"context"

	"gitlab.com/deviosec/octp/internal/virtual/models"
)

func (v *virtualManager) Start(ctx context.Context, id string) error {
	b := GuessBackend(id)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	virt, err := vb.Get(ctx, b.stripPrefix(id))
	if err != nil {
		return err
	}

	if virt.State == models.VirtualStateStopped {
		return vb.Start(ctx, b.stripPrefix(virt.ID))
	}

	return nil
}

func (v *virtualManager) Stop(ctx context.Context, id string) error {
	b := GuessBackend(id)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	virt, err := vb.Get(ctx, b.stripPrefix(id))
	if err != nil {
		return err
	}

	if virt.State >= models.VirtualStateRunning {
		return vb.Stop(ctx, b.stripPrefix(virt.ID))
	}

	return nil
}

func (v *virtualManager) Exists(ctx context.Context, id string) (bool, error) {
	b := GuessBackend(id)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return false, err
	}

	return vb.Exists(ctx, id)
}

func (v *virtualManager) Destroy(ctx context.Context, id string) error {
	// if we have no id, then it is destroyed
	if id == "" {
		return nil
	}

	b := GuessBackend(id)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	return vb.Destroy(ctx, b.stripPrefix(id))
}

func (v *virtualManager) Create(ctx context.Context, conf models.Virtual) (string, error) {
	b := GuessBackend(conf.Type)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return "", err
	}

	// TODO(eyJhb) ensure sane defaults for things
	if conf.MemoryMB == 0 {
		conf.MemoryMB = 50
	}

	id, err := vb.Create(ctx, conf)
	if err != nil {
		return "", err
	}

	return b.Prefix() + id, nil
}

// List will list all the Virtuals for a given provider, where deep indicates how much information we want.
// without deep, only Id, Name and Image is guaranteed to be filled.
func (v *virtualManager) List(ctx context.Context, b BackendType, deep bool) ([]models.Virtual, error) {
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return nil, err
	}

	items, err := vb.List(ctx, deep)
	if err != nil {
		return nil, err
	}

	for k := range items {
		items[k].Type = b.String()
		items[k].ID = b.Prefix() + items[k].ID
	}

	return items, nil
}

func (v *virtualManager) Get(ctx context.Context, id string) (models.Virtual, error) {
	b := GuessBackend(id)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return models.Virtual{}, err
	}

	virtual, err := vb.Get(ctx, b.stripPrefix(id))
	if err != nil {
		return models.Virtual{}, err
	}

	virtual.ID = b.Prefix() + virtual.ID

	return virtual, nil
}

package docker

import (
	"sync"

	docker "github.com/fsouza/go-dockerclient"
)

const (
	Prefix = "DOCKER-"
)

var (
	DockerServerLink = "unix:///var/run/docker.sock"
)

type dockerBackend struct {
	c *docker.Client
}

// for creating a singleton
var (
	once     sync.Once
	instance *dockerBackend
)

// TODO(eyJhb) is this a good way? Seems not
func New() (*dockerBackend, error) {
	var err error
	var client *docker.Client
	once.Do(func() {
		client, err = docker.NewClient(DockerServerLink)
		instance = &dockerBackend{c: client}
	})

	if err != nil {
		return nil, err
	}

	return instance, nil
}

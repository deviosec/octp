package docker

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	"github.com/docker/docker/profiles/seccomp"
	"github.com/opencontainers/runtime-spec/specs-go"
	"github.com/pkg/errors"
	"gitlab.com/deviosec/octp/internal/virtual/models"
)

func (d *dockerBackend) generateSeccompProfile(ctx context.Context, conf models.Virtual) (string, error) {
	// generate our filename, which is done by first
	// sorting the securityoptions, then joining them together
	// with `_`, and finally getting the tempdir for the OS.
	secopts := conf.SecurityOptions
	sort.SliceStable(secopts, func(i, j int) bool { return secopts[i] > secopts[j] })
	optsjoined := strings.Join(secopts, "_")
	filename := fmt.Sprintf("%s/%s.json", os.TempDir(), optsjoined)

	// check if file exists, if so just ignore the next part
	if _, err := os.Stat(filename); os.IsExist(err) || err == nil {
		return filename, nil
	}

	// setup the syscalls we want to allow
	var syscalls []*seccomp.Syscall
	for _, secopt := range conf.SecurityOptions {
		switch secopt {
		case models.VirtualSecoptAllowDisableASLR:
			syscalls = append(syscalls, &seccomp.Syscall{
				Names:  []string{"personality"},
				Action: specs.ActAllow,
				Args: []*specs.LinuxSeccompArg{
					{
						Index: 0,
						Value: 0x0040000,
						Op:    specs.OpEqualTo,
					},
				},
				Comment: "allow setting ASLR personality",
			})
		default:
			return "", fmt.Errorf("securityoption '%s' is not supported by the docker backend", secopt)
		}
	}

	// generate new profile
	nsp, err := appendSeccompProfile(syscalls)
	if err != nil {
		return "", err
	}

	// marshal it
	seccompBytes, err := json.Marshal(nsp)
	if err != nil {
		return "", err
	}

	// write it to a file
	ioutil.WriteFile(filename, seccompBytes, 0644)

	return filename, nil
}

func appendSeccompProfile(newSyscalls []*seccomp.Syscall) (*seccomp.Seccomp, error) {
	// TODO(eyJhb): for this to work, the tag `seccomp` is required.
	// this is until the following commit hits the latest stable branch.
	// https://github.com/moby/moby/commit/c9e19a2aa182e06ebe1653a2c3af02563acf9e36
	dp := seccomp.DefaultProfile()
	if dp == nil {
		return nil, errors.New("virtual.docker: default profile is nil, most likely build without seccomp tag")
	}

	dp.Syscalls = append(dp.Syscalls, newSyscalls...)
	return dp, nil
}

package docker

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"

	docker "github.com/fsouza/go-dockerclient"
	"github.com/pkg/errors"
	"gitlab.com/deviosec/octp/internal/virtual/models"
)

var (
	regexpLoadedImage = regexp.MustCompile("Loaded image: (.*)")

	ErrLoadedImageName = errors.New("Unable to find the name of the newly loaded image")
)

func (d *dockerBackend) Start(ctx context.Context, id string) error {
	return errors.Wrap(d.c.StartContainerWithContext(id, nil, ctx), "could not start container")
}

func (d *dockerBackend) Stop(ctx context.Context, id string) error {
	return errors.Wrap(d.c.StopContainerWithContext(id, 1, ctx), "could not stop container")
}

func (d *dockerBackend) Exists(ctx context.Context, id string) (bool, error) {
	_, exists, err := d.getInternal(ctx, id)
	return exists, err
}

func (d *dockerBackend) Destroy(ctx context.Context, id string) error {
	err := d.c.RemoveContainer(docker.RemoveContainerOptions{
		Context:       ctx,
		ID:            id,
		RemoveVolumes: true,
		Force:         true,
	})

	if err != nil {
		if checkNoSuchContainerError(err) {
			return nil
		}
		return err
	}

	return nil
}

func (d *dockerBackend) List(ctx context.Context, deep bool) ([]models.Virtual, error) {
	opts := docker.ListContainersOptions{
		All:     true,
		Context: ctx,
	}
	containers, err := d.c.ListContainers(opts)
	if err != nil {
		return nil, err
	}

	var virtuals []models.Virtual
	for _, c := range containers {
		var vm models.Virtual
		var err error
		if deep {
			vm, err = d.Get(ctx, c.ID)
			if err != nil {
				return nil, err
			}
		} else {
			// get the name of the container
			var name string
			if len(c.Names) > 0 {
				// strip the first char as it is `/`
				name = c.Names[0][1:]
			}

			vm = models.Virtual{
				ID:    c.ID,
				Name:  name,
				Image: c.Image,
			}
		}

		virtuals = append(virtuals, vm)
	}

	return virtuals, nil
}

func (d *dockerBackend) Get(ctx context.Context, id string) (models.Virtual, error) {
	c, err := d.c.InspectContainerWithOptions(docker.InspectContainerOptions{Context: ctx, ID: id})
	if err != nil {
		return models.Virtual{}, err
	}

	vm := models.Virtual{
		ID:       c.ID,
		Name:     c.Name,
		HostName: c.Config.Hostname,
		Image:    c.Image,
		MemoryMB: uint16(c.HostConfig.Memory / 1024 / 1024),
		Ports:    make(map[string]models.Port),

		State: mapState(c.State),
		// TODO(eyJhb) CPUs not set atm, update this

		// we cannot reverse this at the moment
		// SecurityOptions: c.HostConfig.SecurityOpt,
	}

	// mounts
	for _, mnt := range c.Mounts {
		var mntOptions string
		vm.Mounts = append(vm.Mounts, models.Mount{
			Host:    mnt.Source,
			Guest:   mnt.Destination,
			Options: mntOptions,
		})
	}

	// ports
	for key, portSlice := range c.NetworkSettings.Ports {
		port := portSlice[0]

		splitPort := strings.Split(string(key), "/")
		hostPort, err := strconv.Atoi(port.HostPort)
		if err != nil {
			return vm, err
		}

		guestPort, err := strconv.Atoi(splitPort[0])
		if err != nil {
			return vm, err
		}

		// TODO(eyJhb) do we have a better way for keys?
		vm.Ports[string(key)] = models.Port{
			HostIP:   port.HostIP,
			Host:     hostPort,
			Guest:    guestPort,
			Protocol: splitPort[1],
		}
	}

	return vm, nil
}

func mapState(s docker.State) models.VirtualState {
	var virtState models.VirtualState

	if s.Running {
		if s.Health.Status != "" && s.Health.Status != "starting" {
			if s.Health.Status == "healthy" {
				virtState = models.VirtualStateHealthy
			} else {
				virtState = models.VirtualStateUnhealthy
			}
		} else if !s.Paused && !s.Restarting {
			virtState = models.VirtualStateRunning
		}
	} else {
		virtState = models.VirtualStateStopped
	}

	return virtState
}

func (d *dockerBackend) GCVirtuals(ctx context.Context) error {
	_, err := d.c.PruneContainers(docker.PruneContainersOptions{Context: ctx})
	if err != nil {
		return err
	}

	_, err = d.c.PruneImages(docker.PruneImagesOptions{Context: ctx, Filters: map[string][]string{"dangling": []string{"0"}}})
	if err != nil {
		return err
	}

	_, err = d.c.PruneVolumes(docker.PruneVolumesOptions{Context: ctx})
	if err != nil {
		return err
	}

	return nil
}

func (d *dockerBackend) Create(ctx context.Context, conf models.Virtual) (string, error) {
	// TODO(eyJhb) enforce CPU limit CPU*
	// TODO(eyJhb) enforce security profile settings, SecurityOpt
	c := docker.Config{
		Hostname:   conf.HostName,
		Domainname: conf.HostName,
		Image:      conf.Image,
		Memory:     int64(conf.MemoryMB * 1024 * 1024),
		Env:        conf.Envs,
	}

	var hc docker.HostConfig

	// if needed, generate our security profile
	if len(conf.SecurityOptions) > 0 {
		seccompProfilePath, err := d.generateSeccompProfile(ctx, conf)
		if err != nil {
			return "", err
		}

		fmt.Println("seccompprofilepath", seccompProfilePath)

		if seccompProfilePath != "" {
			hc.SecurityOpt = []string{
				fmt.Sprintf("seccomp=%s", seccompProfilePath),
			}
		}
	}

	// hc := docker.HostConfig{
	// 	// security options
	// 	SecurityOpt: conf.SecurityOptions,
	// }

	// ready our mounts
	for _, mount := range conf.Mounts {
		hc.Mounts = append(hc.Mounts, docker.HostMount{
			Source:   mount.Host,
			Target:   mount.Guest,
			ReadOnly: mount.Options == "r",
			Type:     "bind",
		})
	}

	// setup our ports, apparently we need both portBindings and exposedPorts
	portBindings := make(map[docker.Port][]docker.PortBinding)
	exposedPorts := make(map[docker.Port]struct{})
	for _, port := range conf.Ports {
		portBindings[docker.Port(fmt.Sprintf("%d/%s", port.Guest, port.Protocol))] = []docker.PortBinding{
			{
				HostIP:   port.HostIP,
				HostPort: fmt.Sprintf("%d", port.Host),
			},
		}

		exposedPorts[docker.Port(fmt.Sprintf("%d/%s", port.Guest, port.Protocol))] = struct{}{}
	}
	hc.PortBindings = portBindings
	c.ExposedPorts = exposedPorts

	cont, err := d.c.CreateContainer(docker.CreateContainerOptions{
		Name:       conf.Name,
		Config:     &c,
		HostConfig: &hc,
		Context:    ctx,
	})

	if err != nil {
		return "", err
	}

	// always remove our bridge network
	if err := d.DisconnectNetwork(ctx, "bridge", cont.ID); err != nil {
		return "", err
	}

	return cont.ID, nil
}

func (d *dockerBackend) LoadImage(ctx context.Context, name string, tarFile io.Reader, output io.Writer) error {
	// if the name contains `:`, the split it up and give everything
	// after the last occurence of it as tag.
	var tag string
	if strings.Contains(name, ":") {
		if i := strings.LastIndex(name, ":"); i != -1 {
			tag = name[i+1:]
			name = name[:i]
		}
	}

	// make a multiwriter here, as the output from OutputStream
	// is needed to get the loaded image name.
	var buf bytes.Buffer
	mw := io.MultiWriter(output, &buf)

	opts := docker.LoadImageOptions{
		InputStream:  tarFile,
		OutputStream: mw,

		Context: ctx,
	}

	if err := d.c.LoadImage(opts); err != nil {
		return err
	}

	// extract the image name
	matches := regexpLoadedImage.FindSubmatch(buf.Bytes())
	if len(matches) != 2 {
		return ErrLoadedImageName
	}
	imageName := string(matches[1])

	optsTag := docker.TagImageOptions{
		Repo: name,
		Tag:  tag,

		Context: ctx,
	}

	if err := d.c.TagImage(imageName, optsTag); err != nil {
		return err
	}

	return nil
}

func (d *dockerBackend) BuildImage(ctx context.Context, name, entrypoint string, tarFile io.Reader, output io.Writer) error {
	opts := docker.BuildImageOptions{
		Name:       name,
		Dockerfile: entrypoint,

		InputStream:  tarFile,
		OutputStream: output,

		Context: ctx,
	}

	return d.c.BuildImage(opts)
}

func (d *dockerBackend) DestroyImage(ctx context.Context, name string) error {
	opts := docker.RemoveImageOptions{
		Force:   true,
		NoPrune: false,
		Context: ctx,
	}

	if err := d.c.RemoveImageExtended(name, opts); err != nil {
		if !checkNoSuchImageError(err) {
			return err
		}
	}

	return nil
}

func (d *dockerBackend) PushImage(ctx context.Context, image, server string, auth models.Auth) error {
	// parse image, then we get latest tag if nothing
	// is specified
	repo, tag, err := parseImageString(image)
	if err != nil {
		return err
	}
	image = repo + ":" + tag

	// get all images and ensure it exists
	images, err := d.internalImages(ctx)
	if err != nil {
		return err
	}

	var dimage *docker.APIImages
	for i, img := range images {
		if dimage != nil {
			break
		}

		for _, repoTag := range img.RepoTags {
			if repoTag == image {
				dimage = &images[i]
				break
			}
		}
	}

	// if nil, then we did not find it
	if dimage == nil {
		return fmt.Errorf("image with that name does not exists: %s", image)
	}

	// add the server info to the repo
	repo = server + "/" + repo

	optsTag := docker.TagImageOptions{
		Repo:    repo,
		Tag:     tag,
		Context: ctx,
	}

	err = d.c.TagImage(dimage.ID, optsTag)
	if err != nil {
		return err
	}

	// TODO(eyJhb) figure out what to do with the outputstream
	optsPush := docker.PushImageOptions{
		Name:     repo,
		Tag:      tag,
		Registry: server,
		Context:  ctx,
	}

	return d.c.PushImage(optsPush, d.getAuthConfiguration(auth))
}

func (d *dockerBackend) Images(ctx context.Context) ([]models.Image, error) {
	dimages, err := d.internalImages(ctx)
	if err != nil {
		return nil, err
	}

	var images []models.Image
	for _, img := range dimages {
		idSplit := strings.Split(img.ID, ":")
		if len(idSplit) != 2 {
			return images, errors.New("image id - could not get sha256 from id, invalid format")
		}
		sha256 := idSplit[1]

		for _, repoTag := range img.RepoTags {
			images = append(images, models.Image{
				ID:      img.ID,
				Name:    repoTag,
				Size:    uint64(img.Size),
				Sha256:  sha256,
				Created: img.Created,
			})
		}
	}

	return images, nil
}

func (d *dockerBackend) internalImages(ctx context.Context) ([]docker.APIImages, error) {
	opts := docker.ListImagesOptions{
		All:     true,
		Context: ctx,
	}

	return d.c.ListImages(opts)
}

// getImage return the image if it exists, the image should be specified as `someimage:latest`
func (d *dockerBackend) getImage(ctx context.Context, image string) (docker.APIImages, bool, error) {
	images, err := d.internalImages(ctx)
	if err != nil {
		return docker.APIImages{}, false, err
	}

	for _, img := range images {
		for _, repoTag := range img.RepoTags {
			if repoTag == image {
				return img, true, nil
			}
		}
	}

	return docker.APIImages{}, false, nil
}

func (d *dockerBackend) imageExists(ctx context.Context, image string) (bool, error) {
	_, exists, err := d.getImage(ctx, image)
	return exists, err
}

func (d *dockerBackend) PullImage(ctx context.Context, image string, auth models.Auth) error {
	repo, tag, err := parseImageString(image)
	if err != nil {
		return err
	}

	// check if we already have the image
	if exists, err := d.imageExists(ctx, fmt.Sprintf("%s:%s", repo, tag)); exists || err != nil {
		return err
	}

	opts := docker.PullImageOptions{
		Repository: repo,
		Tag:        tag,
		Context:    ctx,
	}

	return d.c.PullImage(opts, d.getAuthConfiguration(auth))
}

func parseImageString(image string) (string, string, error) {
	repo := image
	tag := "latest"
	if strings.Contains(image, ":") {
		semIndex := strings.LastIndex(image, ":")
		tmpTag := image[semIndex+1:]
		if !strings.Contains(tmpTag, "/") {
			tag = tmpTag
			repo = repo[:semIndex]
		}
	}

	return repo, tag, nil
}

func (d *dockerBackend) getInternal(ctx context.Context, id string) (*docker.Container, bool, error) {
	c, err := d.c.InspectContainerWithOptions(docker.InspectContainerOptions{Context: ctx, ID: id})
	if err != nil {
		if checkNoSuchContainerError(err) {
			return nil, false, nil
		}

		return nil, false, errors.Wrap(err, "could not get container")
	}
	return c, true, nil
}

func (d *dockerBackend) getAuthConfiguration(auth models.Auth) docker.AuthConfiguration {
	return docker.AuthConfiguration{
		Username:      auth.Username,
		Password:      auth.Password,
		RegistryToken: auth.Token,
	}
}

func checkNoSuchContainerError(err error) bool {
	return strings.HasPrefix(err.Error(), "No such container: ")
}

func checkNoSuchImageError(err error) bool {
	return strings.HasPrefix(err.Error(), "no such image")
}

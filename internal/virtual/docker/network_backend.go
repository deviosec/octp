package docker

import (
	"context"

	docker "github.com/fsouza/go-dockerclient"
	"gitlab.com/deviosec/octp/internal/virtual/models"
)

const (
	publicNetworkName = "bridge"
)

func (d *dockerBackend) CreateNetwork(ctx context.Context, name string) (string, error) {
	opts := docker.CreateNetworkOptions{
		Name:           name,
		Driver:         "bridge",
		CheckDuplicate: false,
		Options: map[string]interface{}{
			// TODO(eyJhb): unsure if we can do this globally
			// or that we need to make this option configurable
			// disable ability to contact WWW and LAN
			"com.docker.network.bridge.enable_ip_masquerade": "false",
		},
		Context: ctx,
	}

	n, err := d.c.CreateNetwork(opts)
	if err != nil {
		return "", err
	}

	return n.ID, nil
}

func (d *dockerBackend) DestroyNetwork(ctx context.Context, notworkID string) error {
	return d.c.RemoveNetwork(notworkID)
}

func (d *dockerBackend) ListNetworks(ctx context.Context) ([]models.Network, error) {
	dnets, err := d.c.ListNetworks()
	if err != nil {
		return nil, err
	}

	var nets []models.Network
	for _, dnet := range dnets {
		nets = append(nets, models.Network{
			ID:   dnet.ID,
			Name: dnet.Name,
		})
	}

	return nets, nil
}

func (d *dockerBackend) ConnectNetwork(ctx context.Context, networkID, virtualID, alias string) error {
	opts := docker.NetworkConnectionOptions{
		Container: virtualID,
		Context:   ctx,
	}

	if alias != "" {
		opts.EndpointConfig = &docker.EndpointConfig{
			Aliases: []string{alias},
		}
	}

	return d.c.ConnectNetwork(networkID, opts)
}

func (d *dockerBackend) DisconnectNetwork(ctx context.Context, networkID, virtualID string) error {
	opts := docker.NetworkConnectionOptions{
		Container: virtualID,
		Force:     true,
		Context:   ctx,
	}

	return d.c.DisconnectNetwork(networkID, opts)
}

func (d *dockerBackend) ConnectNetworkPublic(ctx context.Context, virtualID string) error {
	opts := docker.NetworkConnectionOptions{
		Container: virtualID,
		Context:   ctx,
	}

	return d.c.ConnectNetwork(publicNetworkName, opts)
}

func (d *dockerBackend) DisconnectNetworkPublic(ctx context.Context, virtualID string) error {
	opts := docker.NetworkConnectionOptions{
		Container: virtualID,
		Force:     true,
		Context:   ctx,
	}

	return d.c.DisconnectNetwork(publicNetworkName, opts)
}

func (d *dockerBackend) GCNetworks(ctx context.Context) error {
	if _, err := d.c.PruneNetworks(docker.PruneNetworksOptions{Context: ctx}); err != nil {
		return err
	}

	return nil
}

package virtual

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/deviosec/octp/internal/virtual/models"
)

const (
	publicNetworkName = "public-network"
)

var (
	ErrVirtualNoID         = errors.New("the virtual specified does not have a ID specified")
	ErrVirtualStateUnknown = errors.New("the virtual specified is in a unknown state, requires a known state")
	ErrIncompleteLab       = errors.New("we could not get all the required information for the lab, given just names - incomplete lab")
)

// formatLabNames is just a easy way to format our names with a dash,
// so that we have a single place where this happens
func formatLabNames(labName string, name string) string {
	return fmt.Sprintf("%s-%s", labName, name)
}

// TODO(eyJhb) we should be able to force create this,
// where we will remove anything that conflicts
func (v *virtualManager) CreateLab(ctx context.Context, l *models.Lab) error {
	// always create a default network for the lab
	labNetwork := models.Network{Name: l.Name}
	defaultNetworkID, err := v.CreateNetwork(ctx, labNetwork)
	if err != nil {
		return err
	}
	labNetwork.ID = defaultNetworkID

	// make networks for them
	for k, n := range l.Networks {
		n.Name = formatLabNames(l.Name, n.Name)
		id, err := v.CreateNetwork(ctx, n)
		if err != nil {
			return err
		}

		l.Networks[k].ID = id
	}

	// create Virtuals
	for i, virt := range l.Virtuals {
		// check if there is a hostname specified, else it is set to name
		if virt.HostName == "" {
			virt.HostName = virt.Name
		}
		virt.Name = formatLabNames(l.Name, virt.Name)

		id, err := v.Create(ctx, virt)
		if err != nil {
			return err
		}
		l.Virtuals[i].ID = id

		// connect them to the networks
		var hasNetwork bool
		for _, net := range l.Networks {
			for _, connection := range net.Connections {
				connection = formatLabNames(l.Name, connection)
				if connection == virt.Name {
					// if this connection is for public
					// network access, only connect that
					// and then continue
					if net.Name == publicNetworkName {
						if err := v.ConnectPublic(ctx, id); err != nil {
							return err
						}
						continue
					}

					hasNetwork = true
					if err := v.Connect(ctx, net.ID, id, virt.HostName); err != nil {
						return err
					}
				}
			}
		}

		// if the virtual does not belong to any network,
		// the we just attach it to the default lab network created
		if !hasNetwork {
			if err := v.Connect(ctx, labNetwork.ID, id, virt.HostName); err != nil {
				return err
			}
		}
	}

	return nil
}

func (v *virtualManager) StartLab(ctx context.Context, l models.Lab) error {
	for _, virt := range l.Virtuals {
		if virt.ID == "" {
			return ErrVirtualNoID
		}

		if err := v.Start(ctx, virt.ID); err != nil {
			return err
		}
	}

	return nil
}

func (v *virtualManager) StopLab(ctx context.Context, l models.Lab) error {
	for _, virt := range l.Virtuals {
		if virt.ID == "" {
			continue
		}

		if err := v.Stop(ctx, virt.ID); err != nil {
			return err
		}
	}

	return nil
}

// DestroyLab will try to destroy all the remains of failed setups, by using populateLab.
// This is done by looping over a maximum of 99 times (this is done at least twice to ensure
// lab has been destroyed), and normally it will not be above 3-4 times.
// The IDs of the virtuals and the networks is set to "", before the next loop as
// it is used, to ensure the lab has been destroyed.
// (25 means that the lab can be failed created 99 times, and it will still cleanup)
func (v *virtualManager) DestroyLab(ctx context.Context, lab *models.Lab) error {
	for j := 0; j < 100; j++ {
		nl, err := v.PopulateLab(ctx, *lab)
		if err != nil {
			if err != ErrIncompleteLab {
				return err
			}
		}

		var isNotDestroyed bool

		// check if everything is empty (IDs)
		// this means that we have destroyed everything
		for _, virt := range nl.Virtuals {
			if virt.ID != "" {
				isNotDestroyed = true
				break
			}
		}

		if !isNotDestroyed {
			for _, net := range nl.Networks {
				if net.ID != "" {
					isNotDestroyed = true
					break
				}
			}
		}

		// we have destroyed everything, return nil
		if !isNotDestroyed {
			return nil
		}

		// destroy all virtuals first
		for i, virt := range nl.Virtuals {
			if err := v.Destroy(ctx, virt.ID); err != nil {
				return err
			}

			nl.Virtuals[i].ID = ""
		}

		// destroy all networks after
		for i, net := range nl.Networks {
			if err := v.DestroyNetwork(ctx, net.ID); err != nil {
				return err
			}
			nl.Networks[i].ID = ""
		}

		// set everything to empty IDs
		for i := range nl.Virtuals {
			nl.Virtuals[i].ID = ""
		}
		for i := range nl.Networks {
			nl.Networks[i].ID = ""
		}
	}

	return nil
}

// PopulateLab will try to populate all the IDs based an the lab name and virtual/network names
func (v *virtualManager) PopulateLab(ctx context.Context, l models.Lab) (models.Lab, error) {
	var incompleteLab bool

	// add the default lab network to our lab
	l.Networks = append(l.Networks, models.Network{
		Name: l.Name,
	})

	// try to populate our virtuals, this is done by looping over our lab
	// and getting the `Virtuals` for the provider. We keep a small cache,
	// to not spam with list commands.
	listVirtualCache := make(map[string][]models.Virtual)
	for i, virt := range l.Virtuals {
		// check if we have it in our cache, else fetch it
		if listVirtualCache[virt.Type] == nil {
			virtuals, err := v.List(ctx, GuessBackend(virt.Type), false)
			if err != nil {
				return l, err
			}

			listVirtualCache[virt.Type] = virtuals
		}

		// format the virtuals name, with the lab as well
		virtName := formatLabNames(l.Name, virt.Name)

		// try to see if we have something that matches with the name
		var foundVirt bool
		for _, backVirt := range listVirtualCache[virt.Type] {
			if virtName == backVirt.Name {
				l.Virtuals[i].ID = backVirt.ID
				foundVirt = true
				break
			}
		}

		if !foundVirt {
			incompleteLab = true
		}
	}

	// try to get our networks, using the same method as above
	listNetworkCache := make(map[string][]models.Network)
	for i, net := range l.Networks {
		// check if we have it in our cache, else fetch it
		if listNetworkCache[net.Type] == nil {
			networks, err := v.ListNetworks(ctx, GuessBackend(net.Type))
			if err != nil {
				return l, err
			}

			listNetworkCache[net.Type] = networks
		}

		// format the virtuals name, with the lab as well
		netName := formatLabNames(l.Name, net.Name)

		// if this is our default network, which has the
		// same name as the lab itself. Then we should just
		// set the name back to the original one
		if net.Name == l.Name {
			netName = net.Name
		}

		// try to see if we have something that matches with the name
		var foundNet bool
		for _, backNet := range listNetworkCache[net.Type] {
			if netName == backNet.Name {
				l.Networks[i].ID = backNet.ID
				foundNet = true
				break
			}
		}

		if !foundNet {
			incompleteLab = true
		}
	}

	if incompleteLab {
		return l, ErrIncompleteLab
	}

	return l, nil
}

package virtual

import (
	"context"

	"github.com/rs/zerolog/log"
	"gitlab.com/deviosec/octp/internal/virtual/models"
)

func (v *virtualManager) CreateNetwork(ctx context.Context, net models.Network) (string, error) {
	b := GuessBackend(net.Type)
	nb, err := v.getNetBackend(ctx, b)
	if err != nil {
		return "", err
	}

	id, err := nb.CreateNetwork(ctx, net.Name)
	if err != nil {
		return "", err
	}

	return b.Prefix() + id, nil
}

func (v *virtualManager) ListNetworks(ctx context.Context, b BackendType) ([]models.Network, error) {
	nb, err := v.getNetBackend(ctx, b)
	if err != nil {
		return nil, err
	}

	networks, err := nb.ListNetworks(ctx)
	if err != nil {
		return nil, err
	}

	for i := range networks {
		networks[i].Type = b.String()
		networks[i].ID = b.Prefix() + networks[i].ID
	}

	return networks, nil
}

func (v *virtualManager) DestroyNetwork(ctx context.Context, notworkID string) error {
	// if we have no id, then it is destroyed
	if notworkID == "" {
		return nil
	}

	b := GuessBackend(notworkID)
	nb, err := v.getNetBackend(ctx, b)
	if err != nil {
		return err
	}

	return nb.DestroyNetwork(ctx, b.stripPrefix(notworkID))
}

func (v *virtualManager) Connect(ctx context.Context, notworkID, virtualID, alias string) error {
	b := GuessBackend(virtualID)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	log.Debug().Str("container_id", virtualID).Str("network_id", notworkID).Str("alias", alias).Msg("trying to connect virtual to network")

	return vb.ConnectNetwork(ctx, b.stripPrefix(notworkID), b.stripPrefix(virtualID), alias)
}

func (v *virtualManager) Disconnect(ctx context.Context, notworkID, virtualID string) error {
	b := GuessBackend(virtualID)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	log.Debug().Str("container_id", virtualID).Str("network_id", notworkID).Msg("trying to disconnect virtual from network")

	return vb.DisconnectNetwork(ctx, b.stripPrefix(notworkID), b.stripPrefix(virtualID))
}

func (v *virtualManager) ConnectPublic(ctx context.Context, virtualID string) error {
	b := GuessBackend(virtualID)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	log.Debug().Str("container_id", virtualID).Msg("trying to connect virtual to public network")

	return vb.ConnectNetworkPublic(ctx, b.stripPrefix(virtualID))
}

func (v *virtualManager) DisconnectPublic(ctx context.Context, virtualID string) error {
	b := GuessBackend(virtualID)
	vb, err := v.getVirtBackend(ctx, b)
	if err != nil {
		return err
	}

	log.Debug().Str("container_id", virtualID).Msg("trying to disconnect virtual from public network")

	return vb.DisconnectNetworkPublic(ctx, b.stripPrefix(virtualID))
}

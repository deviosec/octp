package fileserver

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/deviosec/octp/internal/fileserver/catalog"
)

func (f *Fileserver) HandleUpload(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	if r.Method != "POST" {
		return nil, http.StatusMethodNotAllowed, errors.New("method not allowed - Use POST")
	}

	// if the user has specified a `If-None-Match` header, then only get the file if there
	// no files in the catalog with that tag
	if hash := getIfNoneMatchHash(r.Header.Get("If-None-Match")); hash != "" {
		// try to add it by hash, if not added == false
		id, fe, added, err := f.Catalog.AddByHash(hash)
		if err != nil {
			return nil, http.StatusInternalServerError, err
		}

		// if it was added, then return info to the client
		if added {
			return map[string]catalog.File{id: fe}, http.StatusCreated, nil
		}
	}

	// only allow reading request up till this
	r.Body = http.MaxBytesReader(w, r.Body, f.MaxFileSize)

	if err := r.ParseMultipartForm(f.MaxMemSize); err != nil {
		return nil, http.StatusBadRequest, err
	}

	file, fileheader, err := r.FormFile("file")
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer file.Close()

	id, fe, err := f.Catalog.AddByFile(fileheader.Filename, file)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	return map[string]catalog.File{id: fe}, http.StatusCreated, nil
}

func (f *Fileserver) HandleGetAll(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	if r.Method != "GET" {
		return nil, http.StatusMethodNotAllowed, errors.New("method not allowed - Use GET")
	}

	files, err := f.Catalog.GetAll()
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return files, http.StatusOK, nil
}

func (f *Fileserver) HandleGet(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	if r.Method != "GET" {
		return nil, http.StatusMethodNotAllowed, errors.New("method not allowed - Use GET")
	}

	uuid, err := getID(r.URL.Path)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	fe, ok, err := f.Catalog.Get(uuid)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	if !ok {
		return nil, http.StatusBadRequest, errors.New("file does not exist")
	}

	return fe, http.StatusOK, nil
}

func (f *Fileserver) HandleView(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	if r.Method != "GET" {
		return nil, http.StatusMethodNotAllowed, errors.New("method not allowed - Use GET")
	}

	uuid, err := getID(r.URL.Path)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	fe, ok, err := f.Catalog.Get(uuid)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	if !ok {
		return nil, http.StatusBadRequest, errors.New("file does not exist")
	}

	bytesToHuman := func(b int64) string {
		const unit = 1024
		if b < unit {
			return fmt.Sprintf("%d B", b)
		}
		div, exp := int64(unit), 0
		for n := b / unit; n >= unit; n /= unit {
			div *= unit
			exp++
		}
		return fmt.Sprintf("%.1f %ciB", float64(b)/float64(div), "KMGTPE"[exp])
	}

	pageData := struct {
		UUID  string
		File  catalog.File
		Size  string
		Added string
	}{
		UUID:  uuid,
		File:  fe,
		Size:  bytesToHuman(int64(fe.Size)),
		Added: fe.Added.Format(time.RFC3339),
	}

	// if viewTemplate is nil, parse it
	if f.ViewTemplate == nil {
		err := f.parseViewTemplate()
		if err != nil {
			return nil, http.StatusInternalServerError, err
		}
	}

	err = f.ViewTemplate.Execute(w, pageData)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return nil, 0, nil
}

func (f *Fileserver) HandleDownload(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	if r.Method != "GET" {
		return nil, http.StatusMethodNotAllowed, errors.New("method not allowed - Use GET")
	}

	uuid, err := getID(r.URL.Path)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	// do net check if it exists now, do that in the call below
	reader, _, err := f.Catalog.GetReader(uuid)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	fe, ok, err := f.Catalog.Get(uuid)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	if !ok {
		return nil, http.StatusBadRequest, errors.New("file does not exist")
	}

	w.Header().Set("Content-Disposition", "attachment; filename="+fe.Name)
	http.ServeContent(w, r, fe.Name, fe.Added, reader)
	return nil, 0, nil
}

func (f *Fileserver) HandleDelete(w http.ResponseWriter, r *http.Request) (interface{}, int, error) {
	if r.Method != "DELETE" {
		return nil, http.StatusMethodNotAllowed, errors.New("method not allowed - Use DELETE")
	}

	uuid, err := getID(r.URL.Path)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	err = f.Catalog.Delete(uuid)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return nil, http.StatusNoContent, nil
}

package fileserver

import (
	"encoding/json"
	"log"
	"net/http"
)

type HandleFunc func(http.ResponseWriter, *http.Request) (interface{}, int, error)

func (fn HandleFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	type ResponseError struct {
		Code int    `json:"code"`
		Msg  string `json:"msg"`
	}

	type Response struct {
		Collection interface{}   `json:"collection"`
		Error      ResponseError `json:"error"`
		Code       int           `json:"code"`
	}

	data, code, err := fn(w, r)
	if code == 0 {
		return
	}

	// log error if it is a internal error
	if code == 500 {
		log.Printf("Internal server error on API request: %v", err)
	}

	// make our response ready
	res := Response{
		Collection: data,
		Error: ResponseError{
			Code: code,
		},
		Code: code,
	}
	if err != nil {
		res.Error.Msg = err.Error()
	}

	// always specify we are using json
	w.Header().Add("Content-Type", "application/json")

	// write status-code after other
	// headers have been added
	w.WriteHeader(code)

	// write our response (body)
	json.NewEncoder(w).Encode(res)
}

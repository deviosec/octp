package fileserver

import (
	"errors"
	"strings"
)

func getID(path string) (string, error) {
	if path[len(path)-1] == '/' {
		path = path[:len(path)-1]
	}

	if len(path) == 0 {
		return "", errors.New("no file specified")
	}

	return path[strings.LastIndex(path, "/")+1:], nil
}

func getIfNoneMatchHash(h string) string {
	startQuote := strings.Index(h, `"`)
	if startQuote == -1 {
		return ""
	}
	endQuote := strings.Index(h[startQuote+1:], `"`)
	if endQuote == -1 {
		return ""
	}

	return h[startQuote+1 : startQuote+endQuote+1]
}

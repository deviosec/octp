package fileserver

import (
	"errors"
	"html/template"

	"gitlab.com/deviosec/octp/internal/fileserver/catalog"
)

const (
	DefaultMaxSize    = 100 << 20 // default 100MB
	DefaultMaxMemSize = 10 << 20  // default memory to use 10MB
)

var (
	ErrInternalServerError = errors.New("internal server error")
	ErrNotFound            = errors.New("couldn't find requested item")
)

type Fileserver struct {
	// for upload files, the maximum file
	// size that can be uploaded
	MaxFileSize int64

	// the maximum memory to use per
	// upload (file stored in memory)
	MaxMemSize int64

	// view template
	ViewTemplate *template.Template

	Catalog catalog.Catalog
}

// specify based on constants if needed
func New(path string, maxFileSize, MaxMemSize int64) (*Fileserver, error) {
	cat, err := catalog.New(path)
	if err != nil {
		return nil, err
	}

	f := &Fileserver{
		MaxFileSize: maxFileSize,
		MaxMemSize:  MaxMemSize,
		Catalog:     cat,
	}

	f.parseViewTemplate()

	return f, nil
}

func (f *Fileserver) parseViewTemplate() error {
	if f.ViewTemplate != nil {
		return nil
	}

	tmpl, err := template.New("").Parse(string(tmplView))
	if err != nil {
		return err
	}

	f.ViewTemplate = tmpl

	return nil
}

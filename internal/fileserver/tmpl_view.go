package fileserver

const (
	tmplView = `
<!DOCTYPE html>
<html>
    <head>
        <title>File View</title>
    </head>
    <body>
        <div class="fileinfo">
            <h1>{{ .File.Name }}</h1>
            <p>ID: {{ .UUID }}</p>

            <ul id="metadata">
                <li>Size: {{ .Size }}</li>
                <li>Uploaded: {{ .Added }}</li>
            </ul>

            <ul id="hashes">
                {{ range $k, $v := .File.Hashes }}
                <li>{{ $k }}: {{ $v }}</li>
                {{ end }}
            </ul>
            
            <a href="../download/{{ .UUID }}">Click here to download</a>
        </div>
    </body>
</html>
`
)

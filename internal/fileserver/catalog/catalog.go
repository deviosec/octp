package catalog

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sync"
	"time"

	"gitlab.com/deviosec/octp/internal/simplecrypto"
)

const (
	hashFilename = "sha256"
	catalogName  = "catalog.json"
)

type File struct {
	Name   string
	Hashes map[string]string
	Size   uint64
	Added  time.Time
}

type Catalog interface {
	AddByFile(name string, inputFile io.Reader) (string, File, error)
	AddByHash(hash string) (string, File, bool, error)
	GetAll() (map[string]File, error)
	Get(uuid string) (File, bool, error)
	GetReader(uuid string) (io.ReadSeeker, bool, error)
	Delete(uuid string) error
}

type catalog struct {
	files     map[string]File
	filesHash map[string]map[string]bool
	path      string

	sync.RWMutex
}

func New(path string) (*catalog, error) {
	if path == "" {
		return nil, errors.New("fileserver.catalog: empty path specified")
	}

	if path[len(path)-1] != '/' {
		path += `/`
	}

	pinfo, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if !pinfo.IsDir() {
		return nil, errors.New("the given path is not a directory")
	}

	cat := &catalog{
		files:     make(map[string]File),
		filesHash: make(map[string]map[string]bool),

		path: path,
	}

	if err := cat.loadCatalog(); err != nil {
		return nil, err
	}

	return cat, nil
}

func (c *catalog) AddByHash(hash string) (string, File, bool, error) {
	if c.entriesWithHash(hash) == 0 {
		return "", File{}, false, nil
	}

	defer c.saveCatalog()

	c.Lock()
	defer c.Unlock()

	var key string
	for k := range c.filesHash[hash] {
		key = k
		break
	}

	fFile := c.files[key]

	file := File{
		Name:   fFile.Name,
		Hashes: fFile.Hashes,
		Size:   fFile.Size,
		Added:  time.Now(),
	}

	uuid := simplecrypto.ID()

	c.files[uuid] = file
	c.filesHash[hash][uuid] = true

	return uuid, file, true, nil
}

func (c *catalog) AddByFile(name string, inputFile io.Reader) (string, File, error) {
	// hash generator
	hg := HashGenerator{}
	hw := hg.Writer()

	// file
	f := tmpFile{}
	fw, err := f.Writer()
	if err != nil {
		return "", File{}, err
	}
	// destroy file if we do not save it
	defer f.Destroy()

	// make our final multiwriter
	mw := io.MultiWriter(hw, fw)

	// copy the file into our multiwriter
	size, err := io.Copy(mw, inputFile)
	if err != nil {
		return "", File{}, err
	}

	// get our hashmap to ensure we do not save
	// the same file multiple times
	hm := hg.Hashes()

	// no one has this, solidify our file
	var fileEntry File
	numEntries := c.entriesWithHash(hm[hashFilename])

	// operations below require lock, and save on actually adding
	// a item to our files
	defer c.saveCatalog()

	c.Lock()
	defer c.Unlock()

	if numEntries == 0 {
		err := f.Save(c.path + hm[hashFilename])
		if err != nil {
			return "", File{}, err
		}
	}

	fileEntry = File{
		Name:   name,
		Hashes: hm,
		Size:   uint64(size),
		Added:  time.Now(),
	}

	// check if hashmap for this hash is nil, create it
	if c.filesHash[hm[hashFilename]] == nil {
		c.filesHash[hm[hashFilename]] = make(map[string]bool)
	}

	// add entry to our files
	uuid := simplecrypto.ID()
	c.files[uuid] = fileEntry
	c.filesHash[hm[hashFilename]][uuid] = true
	return uuid, fileEntry, nil
}

func (c *catalog) GetAll() (map[string]File, error) {
	c.RLock()
	defer c.RUnlock()

	return c.files, nil
}

func (c *catalog) Get(uuid string) (File, bool, error) {
	c.RLock()
	defer c.RUnlock()

	f, ok := c.files[uuid]
	if !ok {
		return File{}, false, nil
	}
	return f, true, nil
}

func (c *catalog) GetReader(uuid string) (io.ReadSeeker, bool, error) {
	// get file
	fileEntry, exists, err := c.Get(uuid)
	if err != nil || !exists {
		return nil, exists, err
	}

	// open the file, to get a reader
	f, err := os.Open(c.path + fileEntry.Hashes[hashFilename])
	if err != nil {
		return nil, false, err
	}

	return f, true, nil
}

func (c *catalog) Delete(uuid string) error {
	// check it exists
	f, exists, _ := c.Get(uuid)

	if !exists {
		return nil
	}

	// check if we are the last entry, so delete the file
	if c.entriesWithHash(f.Hashes[hashFilename]) == 1 {
		// delete the file
		err := os.Remove(c.path + f.Hashes[hashFilename])
		if err != nil {
			return fmt.Errorf("catalog.Delete: failed to delete file from disc: %w", err)
		}
	}

	// only save on actual deletion
	defer c.saveCatalog()
	c.Lock()
	defer c.Unlock()
	delete(c.filesHash[f.Hashes[hashFilename]], uuid)
	delete(c.files, uuid)

	return nil
}

func (c *catalog) entriesWithHash(hash string) int {
	c.RLock()
	defer c.RUnlock()

	return len(c.filesHash[hash])
}

func (c *catalog) saveCatalog() error {
	c.RLock()
	defer c.RUnlock()

	data, err := json.Marshal(c.files)
	if err != nil {
		return err
	}

	if err := ioutil.WriteFile(c.path+catalogName, data, 0666); err != nil {
		return err
	}

	return nil
}

func (c *catalog) loadCatalog() error {
	c.Lock()
	defer c.Unlock()

	catBytes, err := ioutil.ReadFile(c.path + catalogName)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	if len(catBytes) == 0 {
		return nil
	}

	files := make(map[string]File)
	if err := json.Unmarshal(catBytes, &files); err != nil {
		return err
	}

	for uuid, file := range files {
		if _, err := os.Stat(c.path + file.Hashes[hashFilename]); os.IsNotExist(err) {
			return fmt.Errorf("File not existing %s - %s", file.Name, file.Hashes[hashFilename])
		}
		if c.filesHash[file.Hashes[hashFilename]] == nil {
			c.filesHash[file.Hashes[hashFilename]] = make(map[string]bool)
		}

		c.filesHash[file.Hashes[hashFilename]][uuid] = true
	}

	c.files = files
	return nil
}

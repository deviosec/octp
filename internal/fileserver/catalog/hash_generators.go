package catalog

import (
	"crypto/sha256"
	"encoding/hex"
	"hash"
	"io"
)

type HashGenerator struct {
	hashes map[string]hash.Hash
}

func (hg *HashGenerator) Writer() io.Writer {
	if hg.hashes == nil {
		hg.hashes = map[string]hash.Hash{
			// "md5":    md5.New(),
			// "sha1":   sha1.New(),
			"sha256": sha256.New(),
			// "sha512": sha512.New(),
		}
	}

	var writers []io.Writer
	for _, hw := range hg.hashes {
		writers = append(writers, hw)
	}

	return io.MultiWriter(writers...)
}

func (hg *HashGenerator) Hashes() map[string]string {
	result := make(map[string]string)
	for key, hashWriter := range hg.hashes {
		result[key] = hex.EncodeToString(hashWriter.Sum(nil)[:])
	}
	return result
}

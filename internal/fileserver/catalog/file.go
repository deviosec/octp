package catalog

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type tmpFile struct {
	saved   bool
	tmpName string
}

func (t *tmpFile) Writer() (io.Writer, error) {
	f, err := ioutil.TempFile("", "")
	if err != nil {
		log.Printf("tmpFile.Writer: Failed to get writer: %v", err)
		return nil, err
	}

	t.tmpName = f.Name()

	return f, nil
}

func (t *tmpFile) Destroy() {
	if !t.saved && t.tmpName != "" {
		err := os.Remove(t.tmpName)
		if err != nil {
			log.Printf("tmpFile.Destroy: failed to remove file: %v", err)
		}
	}
}

func (t *tmpFile) Save(path string) error {
	if err := moveFile(t.tmpName, path); err != nil {
		return err
	}

	t.saved = true
	return nil
}

// https://gist.github.com/var23rav/23ae5d0d4d830aff886c3c970b8f6c6b
func moveFile(sourcePath, destPath string) error {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("couldn't open source file: %s", err)
	}
	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		return fmt.Errorf("couldn't open dest file: %s", err)
	}
	defer outputFile.Close()
	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		return fmt.Errorf("writing to output file failed: %s", err)
	}
	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		return fmt.Errorf("failed removing original file: %s", err)
	}
	return nil
}

package registryauth

import (
	"crypto/tls"
	"crypto/x509"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/distribution/distribution/registry/auth/token"
	"github.com/docker/libtrust"
)

const (
	ClaimIssuer   = "OCTP"
	ClaimAudience = "Authentication"

	ActionPull = "pull"
	ActionPush = "push"
)

type Maker struct {
	privKey libtrust.PrivateKey
	pubKey  libtrust.PublicKey
}

func New(privKeyFile, pubKeyFile string) (*Maker, error) {
	cert, err := tls.LoadX509KeyPair(pubKeyFile, privKeyFile)
	if err != nil {
		return nil, err
	}

	x509Cert, err := x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return nil, err
	}

	pk, err := libtrust.FromCryptoPublicKey(x509Cert.PublicKey)
	if err != nil {
		return nil, err
	}
	prk, err := libtrust.FromCryptoPrivateKey(cert.PrivateKey)
	if err != nil {
		return nil, err
	}

	return &Maker{privKey: prk, pubKey: pk}, nil
}

func (rt Maker) GenerateToken(ID string, actions []string, images []string, exp time.Duration) (string, error) {
	now := time.Now()

	// get the algoritm of the private key
	// we use a hack here, and just sign anything to get the algoritm
	_, algo, err := rt.privKey.Sign(strings.NewReader("AUTH"), 0)
	if err != nil {
		return "", err
	}

	// generate JWT header
	header := token.Header{
		Type:       "JWT",
		SigningAlg: algo,
		KeyID:      rt.pubKey.KeyID(),
	}

	// generate our claimset
	claimset := token.ClaimSet{
		Issuer:     ClaimIssuer,
		Subject:    ID,
		Audience:   ClaimAudience,
		Expiration: now.Add(exp).Unix(),
		NotBefore:  now.Unix() - 10,
		IssuedAt:   now.Unix(),
		JWTID:      fmt.Sprintf("%d", rand.Int63()),
		Access:     []*token.ResourceActions{},
	}

	// add our actions to our claimset
	for _, image := range images {
		claimset.Access = append(claimset.Access, &token.ResourceActions{
			Type:    "repository",
			Name:    image,
			Actions: actions,
		})
	}

	// marshal header and claim
	headerJSON, err := json.Marshal(header)
	if err != nil {
		return "", err
	}

	claimJSON, err := json.Marshal(claimset)
	if err != nil {
		return "", err
	}

	payload := fmt.Sprintf("%s%s%s", b64encode(headerJSON), token.TokenSeparator, b64encode(claimJSON))
	sig, sigAlgo, err := rt.privKey.Sign(strings.NewReader(payload), 0)
	if err != nil && sigAlgo != algo {
		return "", err
	}

	token := fmt.Sprintf("%s%s%s", payload, token.TokenSeparator, b64encode(sig))

	return token, nil
}

// helpers
func b64encode(inp []byte) string {
	// return strings.ReplaceAll(b64.StdEncoding.EncodeToString(inp), "=", "")
	// return b64.StdEncoding.EncodeToString(inp)
	return b64.RawURLEncoding.EncodeToString(inp)
}

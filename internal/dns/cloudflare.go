package dns

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/cloudflare/cloudflare-go"
)

var (
	ErrNoRecordFound = errors.New("record with the given name was not found")
)

func initCloudflare(token, domain string) (*CF, error) {
	api, err := cloudflare.NewWithAPIToken(token)
	if err != nil {
		return nil, err
	}

	id, err := api.ZoneIDByName(domain)
	if err != nil {
		return nil, err
	}

	return &CF{
		APIClient: api,
		ZoneID:    id,
		ZoneName:  domain,
	}, nil

}

func (cf *CF) List(ctx context.Context) ([]Record, error) {
	recs, err := cf.APIClient.DNSRecords(ctx, cf.ZoneID, cloudflare.DNSRecord{})
	if err != nil {
		return nil, err
	}

	var records []Record
	for _, r := range recs {
		records = append(records, Record{
			Name:      r.Name,
			Type:      getRecordType(r.Type),
			Content:   r.Content,
			CreatedOn: r.CreatedOn,
		})
	}

	return records, nil
}

func (cf *CF) Create(ctx context.Context, rec Record) error {
	newDNS := cloudflare.DNSRecord{
		Type:    rec.Type.String(),
		Name:    cf.fullDomainString(rec.Name),
		Content: rec.Content,
	}

	_, err := cf.APIClient.CreateDNSRecord(ctx, cf.ZoneID, newDNS)
	return err
}

func (cf *CF) Delete(ctx context.Context, name string) error {
	// find the domain id first
	rec, err := cf.getDomain(ctx, name)
	if err != nil {
		if err == ErrNoRecordFound {
			return nil
		}
		return err
	}

	if err := cf.APIClient.DeleteDNSRecord(ctx, cf.ZoneID, rec.ID); err != nil {
		return err
	}

	// delet the rest that matches, if any
	return cf.Delete(ctx, name)
}

func (cf *CF) Domain() string {
	return cf.ZoneName
}

// helpers
func (cf *CF) fullDomainString(name string) string {
	if !strings.HasSuffix(name, cf.ZoneName) {
		return fmt.Sprintf("%s.%s", name, cf.ZoneName)
	}

	return name
}
func (cf *CF) getDomain(ctx context.Context, name string) (Record, error) {
	dnsRec := cloudflare.DNSRecord{Name: cf.fullDomainString(name)}
	recs, err := cf.APIClient.DNSRecords(context.Background(), cf.ZoneID, dnsRec)
	if err != nil {
		return Record{}, err
	}

	if len(recs) == 0 {
		return Record{}, ErrNoRecordFound
	}

	r := recs[0]

	return Record{
		ID:        r.ID,
		Name:      r.Name,
		Type:      getRecordType(r.Type),
		Content:   r.Content,
		CreatedOn: r.CreatedOn,
	}, nil
}

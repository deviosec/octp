package dns

import (
	"context"
	"time"

	"github.com/cloudflare/cloudflare-go"
)

type RecordType string

const (
	RecordTypeA       RecordType = "A"
	RecordTypeAAAA    RecordType = "AAAA"
	RecordTypeUnknown RecordType = "unknown"
)

func (rt RecordType) String() string {
	return string(rt)
}

type Record struct {
	ID      string
	Name    string
	Type    RecordType
	Content string

	CreatedOn time.Time
}

type DNSProvider interface {
	List(ctx context.Context) ([]Record, error)
	Create(ctx context.Context, rec Record) error
	Delete(ctx context.Context, name string) error
	Domain() string
}

type CF struct {
	APIClient *cloudflare.API
	ZoneID    string
	ZoneName  string
}

func New(token, domain string) (DNSProvider, error) {
	cf, err := initCloudflare(token, domain)
	if err != nil {
		return nil, err
	}

	return cf, nil
}

func getRecordType(t string) RecordType {
	switch t {
	case "A":
		return RecordTypeA
	case "AAAA":
		return RecordTypeAAAA
	default:
		return RecordTypeUnknown
	}
}

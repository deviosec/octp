package worm

import (
	"context"
	"database/sql"
	"errors"

	sq "github.com/Masterminds/squirrel"
)

var (
	ErrAlreadyTransaction = errors.New("already in transaction or DB does not support BeginTx")
	ErrNotTransaction     = errors.New("not in transaction or TX does not support Commit/Rollback")
)

// txBegin is used to ensure, that our statement.DB
type txBegin interface {
	BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)
}

type txCommitRollback interface {
	Commit() error
	Rollback() error
}

func (stmt *Statement) Begin(ctx context.Context) (*Statement, error) {
	if v, ok := stmt.DB.(txBegin); ok {
		tx, err := v.BeginTx(ctx, nil)
		if err != nil {
			return nil, err
		}

		newStmt := stmt.GetInstance()
		newStmt.DB = tx
		newStmt.Builder = sq.StatementBuilderType{}.
			PlaceholderFormat(stmt.PlaceholderFormat).
			RunWith(tx)
		newStmt.InTransaction = true

		return &newStmt, nil
	}

	return nil, ErrAlreadyTransaction
}

func (stmt *Statement) Commit() error {
	if v, ok := stmt.DB.(txCommitRollback); ok {
		return v.Commit()
	}

	return ErrNoCondition
}

func (stmt *Statement) Rollback() error {
	if v, ok := stmt.DB.(txCommitRollback); ok {
		return v.Rollback()
	}

	return ErrNoCondition
}

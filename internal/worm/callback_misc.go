package worm

import (
	"context"
	"database/sql"
	"encoding/json"
	"reflect"
	"time"
)

// callbacks batch operations
func addBatchOperations(cType CallbackType) CallbackFn {
	return func(ctx context.Context, stmt *Statement) (bool, error) {
		if stmt.hasErrors() {
			return false, nil
		}

		// check if we have an array or slice
		mv := reflect.ValueOf(stmt.Model).Elem()

		// if it is not a slice or array, then just continue on like nothing
		if mv.Kind() != reflect.Slice && mv.Kind() != reflect.Array {
			return false, nil
		}

		nstmt := stmt
		if !stmt.InTransaction {
			var err error
			nstmt, err = stmt.Begin(ctx)
			if err != nil {
				return true, err
			}
		}

		// TODO(eyJhb): this should always run, but... Is this the best way?
		if !stmt.InTransaction {
			defer nstmt.Commit()
		}

		for i := 0; i < mv.Len(); i++ {
			// get the value at index X of slice or array
			v := mv.Index(i)

			// for each element we activate the callbacks
			// so reconstruct the statement, and parse fields
			substmt := nstmt.GetInstance()
			substmt.Model = v.Addr().Interface()
			substmt.Fields = stmt.Fields

			// update the field values
			if err := GetStructValues(v, substmt.Fields); err != nil {
				if !stmt.InTransaction {
					stmt.Rollback()
				}
				return true, err
			}

			// fire the callbacks !
			substmt.RunCallbacks(ctx, cType)
			if substmt.hasErrors() {
				// TODO(eyJhb): Should we rollback here?
				// Should we stop it as well?
				if !stmt.InTransaction {
					stmt.Rollback()
				}
				stmt.AddErrors(substmt.error())
				return true, nil
			}
		}

		return true, nil
	}
}

// Callbacks JSON
func callbackBeforeJSON(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	for _, field := range stmt.Fields {
		if _, ok := field.Tags[TagJSON]; !ok {
			continue
		}

		var jsonInput []byte
		field.Value = &jsonInput
	}

	return false, nil
}

func callbackAfterJSON(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	for _, field := range stmt.Fields {
		if _, ok := field.Tags[TagJSON]; !ok {
			continue
		}

		if err := json.Unmarshal(*field.Value.(*[]byte), field.Ptr); err != nil {
			return false, err
		}
	}

	return false, nil
}

// Callbacks time
func callbackAddTime(tagname string) CallbackFn {
	return func(ctx context.Context, stmt *Statement) (bool, error) {
		if stmt.hasErrors() {
			return false, nil
		}

		for _, field := range stmt.Fields {
			if _, ok := field.Tags[tagname]; !ok {
				continue
			}

			ptrTime, err := checkPtrTime(field.Ptr)
			if err != nil {
				return false, err
			}

			currentTime := time.Now()

			// field is now of *time.Time
			field.Value = &currentTime

			// update our ptr
			if ptrTime {
				reflect.ValueOf(field.Ptr).Elem().Set(reflect.ValueOf(&currentTime))
			} else {
				reflect.ValueOf(field.Ptr).Elem().Set(reflect.ValueOf(currentTime))
			}
		}

		return false, nil
	}
}

func callbackBeforeTime(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	for _, field := range stmt.Fields {
		_, ok1 := field.Tags[TagCreatedAt]
		_, ok2 := field.Tags[TagUpdatedAT]
		_, ok3 := field.Tags[TagDeletedAt]
		if !ok1 && !ok2 && !ok3 {
			continue
		}

		if _, err := checkPtrTime(field.Ptr); err != nil {
			return false, err
		}

		var timeInput sql.NullTime
		field.Value = &timeInput
	}

	return false, nil
}

func callbackAfterTime(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	for _, field := range stmt.Fields {
		_, ok1 := field.Tags[TagCreatedAt]
		_, ok2 := field.Tags[TagUpdatedAT]
		_, ok3 := field.Tags[TagDeletedAt]
		if !ok1 && !ok2 && !ok3 {
			continue
		}

		vof := reflect.ValueOf(field.Ptr)
		ptrTime, err := checkPtrTime(field.Ptr)
		if err != nil {
			return false, err
		}

		timeInput := field.Value.(*sql.NullTime)
		if !timeInput.Valid {
			// set to zero value (works for both *time.Time and time.Time
			vof.Elem().Set(reflect.Zero(vof.Type().Elem()))
			continue
		}

		// set ptr to time.Time
		if ptrTime {
			vof.Elem().Set(reflect.ValueOf(&timeInput.Time))
		} else {
			vof.Elem().Set(reflect.ValueOf(timeInput.Time))
		}
	}

	return false, nil
}

// return true if the `val` is *time.Time, false if time.Time.
// keep in mind, that `val` here will usually be `field.Ptr`, therefore
// it will always be a pointer. This is the reason double pointers are used.
// if neither *time.Time or time.Time, then ErrNotTypeTime will be returned.
func checkPtrTime(val interface{}) (bool, error) {
	switch val.(type) {
	case *(*time.Time):
		return true, nil
	case *time.Time:
		return false, nil
	}

	return false, ErrNotTypeTime
}

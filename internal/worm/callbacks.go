package worm

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

var (
	ErrCallbackPos = errors.New("callback position needs to be in the form of before:something, after:something or empty")
)

type CallbackType uint8

const (
	CallbackInsert CallbackType = iota
	CallbackSelect
	CallbackUpdate
	CallbackDelete
	CallbackRowsBefore
	CallbackRowsAfter
)

// Callbacks/middleware - if stop == true or err != nil it will stop
type CallbackFn func(ctx context.Context, stmt *Statement) (stop bool, err error)

type Callback struct {
	Name string
	// position can be, `before:callbackname` or `after:callbackname`
	// also allowed to be empty ``, which will just insert it whereever
	Position string
	Type     CallbackType
	Fn       CallbackFn
}

func (stmt *Statement) addCallbacks() error {
	// create
	stmt.AddCallback("batch", "", CallbackInsert, addBatchOperations(CallbackInsert))
	stmt.AddCallback("insert", "after:batch", CallbackInsert, callbackInsert)
	stmt.AddCallback("addUUID", "before:insert", CallbackInsert, callbackInsertAddUUID)
	stmt.AddCallback("addCreatedAt", "before:insert", CallbackInsert, callbackAddTime("createdAt"))
	stmt.AddCallback("addUpdatedAt", "before:insert", CallbackInsert, callbackAddTime("updatedAt"))
	stmt.AddCallback("toJSON", "before:insert", CallbackInsert, callbackInsertToJSON)

	// select
	stmt.AddCallback("select", "", CallbackSelect, callbackSelect)
	stmt.AddCallback("excludesoftdeleted", "before:select", CallbackSelect, callbackSelectExcludeSoftdeleted)
	stmt.AddCallback("relation", "after:select", CallbackSelect, callbackEmbedded)

	// rows before
	stmt.AddCallback("beforeTime", "", CallbackRowsBefore, callbackBeforeTime)
	stmt.AddCallback("beforeJSON", "", CallbackRowsBefore, callbackBeforeJSON)

	// rows after
	stmt.AddCallback("afterTime", "", CallbackRowsAfter, callbackAfterTime)
	stmt.AddCallback("afterJSON", "", CallbackRowsAfter, callbackAfterJSON)

	// delete
	stmt.AddCallback("delete", "", CallbackDelete, callbackDelete)

	// update stuff
	stmt.AddCallback("toJSON", "before:update", CallbackUpdate, callbackInsertToJSON)
	stmt.AddCallback("updateUpdatedAt", "before:update", CallbackUpdate, callbackAddTime("updatedAt"))
	stmt.AddCallback("update", "", CallbackUpdate, callbackUpdate)

	return nil
}

func (stmt *Statement) RunCallbacks(ctx context.Context, ctype CallbackType) error {
	for _, callback := range stmt.Callbacks {
		if callback.Type != ctype {
			continue
		}

		stop, err := callback.Fn(ctx, stmt)
		if err != nil {
			stmt.AddErrors(fmt.Errorf("callback %s failed: %w", callback.Name, err))
		}

		if stop {
			break
		}
	}

	return stmt.error()
}

// AddCallback adds a callback at a given position in the chain of callbacks, with the type specified.
// pos can be in the form of `"after:insert"`, `"before:insert"` or `""` which will place it at the end of callbacks.
func (stmt *Statement) AddCallback(name, pos string, ctype CallbackType, fn CallbackFn) error {
	callback := &Callback{
		Name:     name,
		Position: pos,
		Type:     ctype,
		Fn:       fn,
	}

	when, relativeTo, err := getCallbackPosition(callback.Position)
	if err != nil {
		return err
	}

	// log.Info().
	// 	Str("when", when).
	// 	Str("relativeTo", relativeTo).
	// 	Str("callback", name).
	// 	Msg("AddCallback")
	// Str("e", when).

	// find out where to insert the callback
	if len(stmt.Callbacks) == 0 {
		stmt.Callbacks = append(stmt.Callbacks, callback)
		return nil
	}

	var insertIndex int
	for i, c := range stmt.Callbacks {
		insertIndex = i
		if relativeTo == c.Name {
			if when == "before" {
				insertIndex = i
			} else {
				insertIndex = i + 1
			}
			break
		}
	}

	stmt.Callbacks = append(stmt.Callbacks[:insertIndex], append([]*Callback{callback}, stmt.Callbacks[insertIndex:len(stmt.Callbacks)]...)...)

	return nil
}

func getCallbackPosition(pos string) (string, string, error) {
	// if the pos is empty, it just need to go _somewhere_ relative
	// to when it was added in the code
	if pos == "" {
		return "", "", nil
	}
	// get position, and split to determine
	// where to place callback
	posSplit := strings.Split(pos, ":")

	if len(posSplit) != 2 {
		return "", "", ErrCallbackPos
	}

	// we know we have a length of two here, determine if it is before or after
	when := posSplit[0]
	if when != "before" && when != "after" {
		return "", "", ErrCallbackPos
	}

	relativeTo := posSplit[1]

	return when, relativeTo, nil
}

package worm

import (
	"context"
	"encoding/json"
	"reflect"

	"github.com/gofrs/uuid"
)

func callbackInsert(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	// create a columns and values []interface{}
	var columns []string
	var values []interface{}
	for _, field := range stmt.Fields {
		if ShouldIgnoreField(field) {
			continue
		}

		columns = append(columns, field.Column)

		if field.Value != nil {
			values = append(values, field.Value)
			continue
		}

		values = append(values, field.Ptr)
	}

	insertQuery := stmt.Builder.Insert(stmt.Table).
		Columns(columns...).
		Values(values...)

	_, err := insertQuery.ExecContext(ctx)

	return false, err
}

func callbackInsertToJSON(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	for _, field := range stmt.Fields {
		if _, ok := field.Tags[TagJSON]; !ok {
			continue
		}

		jsonBytes, err := json.Marshal(field.Ptr)
		if err != nil {
			return false, err
		}

		// field is now of *[]byte
		field.Value = &jsonBytes
	}

	return false, nil
}

func callbackInsertAddUUID(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	for _, field := range stmt.Fields {
		if _, ok := field.Tags[TagUUID]; !ok {
			continue
		}

		uuidv4, err := uuid.NewV4()
		if err != nil {
			return false, err
		}

		if reflect.TypeOf(field.Ptr).Elem() != reflect.TypeOf(uuid.Nil) {
			return false, ErrNotTypeUUID
		}

		// set ptr to the new value, as we want the code outside
		// to be able to access the ID
		reflect.ValueOf(field.Ptr).Elem().Set(reflect.ValueOf(uuidv4))
	}

	return false, nil
}

package worm

import (
	"context"
	"reflect"
	"time"

	sq "github.com/Masterminds/squirrel"
)

func callbackDelete(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	var softField *Field
	var idField *Field
	for _, field := range stmt.Fields {
		if _, ok := field.Tags[TagDeletedAt]; ok {
			softField = field
		} else if _, ok := field.Tags[TagPrimaryKey]; ok {
			idField = field
		}

		if softField != nil && idField != nil {
			break
		}
	}

	// if we are given a empty struct, then we just delete
	// based on the conditions (we require at least one wherePart).
	// however if it is not empty, we delete based on the id
	// and ignore all the whereParts (throw an error to indicate
	// that we do not accept any whereParts)
	var isEmpty bool
	if reflect.ValueOf(stmt.Model).Elem().IsZero() {
		isEmpty = true
	}

	// if there are no whereParts, then we return a error
	if isEmpty && len(stmt.WhereParts) == 0 {
		if len(stmt.WhereParts) == 0 {
			return false, ErrNoCondition
		}

		// if the struct is not empty and there are any whereParts specified, give a error
	} else if !isEmpty && len(stmt.WhereParts) > 0 {
		if len(stmt.WhereParts) > 0 {
			return false, ErrCondition
		}
		// if the struct is not empty but we could not find any idField, then return error
	} else if !isEmpty && idField == nil {
		return false, ErrNoPrimaryField
	}

	if softField != nil {
		return callbackDeleteSoft(ctx, stmt, isEmpty, idField, softField)
	}

	return callbackDeleteHard(ctx, stmt, isEmpty, idField)
}

func callbackDeleteSoft(ctx context.Context, stmt *Statement, isEmpty bool, idField, softField *Field) (bool, error) {
	currentTime := time.Now()
	softDeleteQuery := stmt.Builder.Update(stmt.Table).
		Set(softField.Column, currentTime)

	if isEmpty {
		for _, wherePart := range stmt.WhereParts {
			softDeleteQuery = softDeleteQuery.Where(wherePart)
		}
	} else {
		softDeleteQuery = softDeleteQuery.Where(sq.Eq{idField.Column: idField.Ptr})
	}

	_, err := softDeleteQuery.ExecContext(ctx)
	if err != nil {
		return false, err
	}

	ptrTime, err := checkPtrTime(softField.Ptr)
	if err != nil {
		return false, err
	}

	// update our ptr
	if ptrTime {
		reflect.ValueOf(softField.Ptr).Elem().Set(reflect.ValueOf(&currentTime))
	} else {
		reflect.ValueOf(softField.Ptr).Elem().Set(reflect.ValueOf(currentTime))
	}

	return false, nil
}

func callbackDeleteHard(ctx context.Context, stmt *Statement, isEmpty bool, idField *Field) (bool, error) {
	hardDeleteQuery := stmt.Builder.Delete(stmt.Table)

	if isEmpty {
		for _, wherePart := range stmt.WhereParts {
			hardDeleteQuery = hardDeleteQuery.Where(wherePart)
		}
	} else {
		hardDeleteQuery = hardDeleteQuery.Where(sq.Eq{idField.Column: idField.Ptr})
	}

	_, err := hardDeleteQuery.ExecContext(ctx)
	if err != nil {
		return false, err
	}

	return false, err
}

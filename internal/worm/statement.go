package worm

import (
	"context"
	"database/sql"
	"reflect"

	sq "github.com/Masterminds/squirrel"
	"github.com/hashicorp/go-multierror"
)

type DBRunner interface {
	PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

type Statement struct {
	// connection to the underlying database, which is also
	// used by `Builder` (`RunWith` Masterminds/squirrel).
	// DB *sql.DB
	DB DBRunner

	// Holds the placeholder format, which is used by
	// `Builder`
	PlaceholderFormat sq.PlaceholderFormat

	// Builder has the PlaceholderFormat and the database
	// in itself, where it can be used with QueryContext, etc.
	Builder sq.StatementBuilderType

	// table we are operating on, which will automatically
	// be set based on the name of the struct. If however
	// this does not match, it can be overriden.
	Table string

	// model contains the input that we operate on.
	// it could be of types struct{}, []struct{}, [5]struct{}.
	// fields is the parsed struct, where each field represents
	// a field inside the struct (it will not parse slices/arrays).
	Model interface{}

	Fields []*Field

	// sorted list of callbacks
	Callbacks []*Callback

	// for specifying a query
	WhereParts []interface{}
	Limit      *uint64
	Distinct   []string

	// holds all the errors that might
	// occur during the callbacks
	errors []error

	// Transaction in progress?
	InTransaction bool

	// TODO(eyJhb): we need to add some kind of
	// `Unscoped` thing, so that deleted entries (soft deleted)
	// could still be selected if wanted.
	// I don't think this should remove all the WhereParts, limits, etc.
	// but maybe only those that middleware wants to specify?
	// Unscoped bool
}

// TODO(eyJhb): this should support a hierachy of annonymous fields
// ie. if it is a embedded struct Bar, with Name ID, it should have a
// annonymous fields ["Bar"], and it should be able to nest forever down
type Field struct {
	// name of the field in the struct
	Name string

	// name of the field in the database
	Column string

	// tags in the field
	Tags map[string]string

	// the value, this might get converted, etc.
	// this value can/will be changed
	Value interface{}

	// pointer to value in model - should not be changed
	// and will remain the same throughout
	Ptr interface{}

	// check if we should make ptr in field + model in Statement be reflect.Value instead.
	// there is no need for the m NOT to be...

	// ValueOf reflect.Value
}

func (stmt *Statement) ParseFields() error {
	// Run ensures that this is a pointer
	mv := reflect.ValueOf(stmt.Model).Elem()

	// get the fields/meta information from our struct
	fields, err := ParseStruct(mv.Type())
	if err != nil {
		return err
	}

	// get values/ptrs to all meta information - only if struct
	if mv.Kind() == reflect.Struct {
		if err := GetStructValues(mv, fields); err != nil {
			return err
		}
	}

	stmt.Fields = fields

	return nil
}

// GetInstance will copy DB, Table and Callbacks.
// It will not preserve the Model, Fields and Selectiors (WhereParts, Limit).
func (stmt *Statement) GetInstance() Statement {
	return Statement{
		DB:                stmt.DB,
		PlaceholderFormat: stmt.PlaceholderFormat,
		Table:             stmt.Table,
		Callbacks:         stmt.Callbacks,
		Builder:           stmt.Builder,
		InTransaction:     stmt.InTransaction,
	}
}

// hasErrors will return true, if any error
// occured when running the statement (ie. in callbacks)
func (stmt *Statement) hasErrors() bool {
	return len(stmt.errors) > 0
}

// errors returns all errors during callbacks
func (stmt *Statement) error() error {
	return multierror.Append(nil, stmt.errors...).ErrorOrNil()
}

// AddErrors is a convenient way of adding one
// or more to the statement
func (stmt *Statement) AddErrors(errs ...error) {
	stmt.errors = append(stmt.errors, errs...)
}

func (stmt *Statement) Run(ctx context.Context, ctype CallbackType) error {
	mt := reflect.TypeOf(stmt.Model)

	// enure we have a ptr
	if mt.Kind() != reflect.Ptr {
		stmt.AddErrors(ErrNotPointer)
		return stmt.error()
	}

	if stmt.Table == "" {
		structName, err := GetStructName(reflect.TypeOf(stmt.Model))
		if err != nil {
			stmt.AddErrors(err)
			return stmt.error()
		}

		stmt.Table = SnakeCasePlural(structName)
	}

	if err := stmt.ParseFields(); err != nil {
		stmt.AddErrors(err)
		return stmt.error()
	}

	stmt.RunCallbacks(ctx, ctype)

	return stmt.error()
}

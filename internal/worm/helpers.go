package worm

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/iancoleman/strcase"
	"github.com/jinzhu/inflection"
)

// ParseStruct accets a reflect.Type, which can either be a
// struct, slice of structs, array of structs or map of structs,
// otherwise a error will be returned.
// ParseStruct will parse each field in the struct, and return the fields for it.
// It will recursively call itself on annonymous fields, and add them to the final fields.
// A none-annonymous takes procedence over a annonymous field, and will therefore overwrite
// the annonymous field.
// Keep in mind that ParseStruct will not populate "Value" and "Ptr" of the field, GetStructValues
// should be used afterwards to populate those.
func ParseStruct(mt reflect.Type) ([]*Field, error) {
	// check if ptr
	if mt.Kind() == reflect.Ptr {
		mt = mt.Elem()
	}

	// check if slice/array/map, if so we get the inner type
	if mt.Kind() == reflect.Slice || mt.Kind() == reflect.Array || mt.Kind() == reflect.Map {
		mt = mt.Elem()
	}

	// ensure we are dealing with a struct, as to not panic
	if mt.Kind() != reflect.Struct {
		return nil, fmt.Errorf("ParseStruct: %w", ErrNotStruct)
	}

	var fields []*Field
	var anonymousFields []*Field
	for i := 0; i < mt.NumField(); i++ {
		// get the field type
		ft := mt.Field(i)

		// if this is embedded (annonymous) and of kind struct, recurse
		if ft.Anonymous && ft.Type.Kind() == reflect.Struct {
			recFields, err := ParseStruct(ft.Type)
			if err != nil {
				return nil, err
			}

			anonymousFields = append(anonymousFields, recFields...)

			// continue on to next field
			continue
		}

		// declare field + set default values
		field := Field{
			Name:   ft.Name,
			Column: strcase.ToSnake(ft.Name),
			Tags:   make(map[string]string),
		}

		// lookup tags + set column name if specified
		if v, ok := ft.Tag.Lookup(FieldTagKey); ok {
			tags := strings.Split(v, ",")
			if len(tags) > 0 {
				// update the column name if specified
				if tags[0] != "" {
					field.Column = tags[0]
				}

				// ignore colunms whose name are `-`
				if field.Column == "-" {
					continue
				}

				// add all our tags
				for _, tag := range tags[1:] {
					// ignore empty tags
					if tag == "" {
						continue
					}

					// split tag on `:`
					splitTag := strings.SplitN(tag, ":", 2)

					if len(splitTag) == 1 {
						field.Tags[splitTag[0]] = ""
					} else {
						field.Tags[splitTag[0]] = splitTag[1]
					}
				}
			}
		}

		fields = append(fields, &field)
	}

	// append any anonymous field, that is
	// not currently in the struct
	for _, anfield := range anonymousFields {
		var found bool
		for _, field := range fields {
			// if field.Name == anfield.Name {
			if field.Column == anfield.Column {
				found = true
				break
			}
		}

		if !found {
			fields = append(fields, anfield)
		}
	}

	return fields, nil
}

// GetStructValues takes a reflect.Value and a slice of fields, where
// it will use FieldByName to populate the "Value" (nil) and "Ptr" in each field.
// If a mv is not of type "struct", GetStructValues vill panic.
func GetStructValues(mv reflect.Value, fields []*Field) error {
	for _, field := range fields {
		fv := mv.FieldByName(field.Name)

		field.Value = nil

		// Should be possible to addr
		// for anything except map
		if fv.CanAddr() {
			field.Ptr = fv.Addr().Interface()
		} else {
			field.Ptr = fv.Interface()
		}
	}

	return nil
}

// ShouldIgnoreField return true, if the field should be ignored.
// This is caused by the field having options "ignore" or "embedded" set,
// which most likely indicate that a callback will handle this field in its
// own way.
func ShouldIgnoreField(field *Field) bool {
	if _, ok := field.Tags[TagIgnore]; ok {
		return true
	}

	if _, ok := field.Tags[TagEmbedded]; ok {
		return true
	}

	return false
}

// GetStructName expects a reflect.Type to be given, which
// is of either struct, slice, array or map
func GetStructName(mt reflect.Type) (string, error) {
	// if pointer, get value to what it is pointing at
	if mt.Kind() == reflect.Ptr {
		mt = mt.Elem()
	}

	// if slice/array/map, then get the type it contains
	if mt.Kind() == reflect.Slice || mt.Kind() == reflect.Array || mt.Kind() == reflect.Map {
		mt = mt.Elem()
	}

	// if not of type struct by now, then give error
	if mt.Kind() != reflect.Struct {
		return "", fmt.Errorf("GetStructName: %w", ErrNotStruct)
	}

	return mt.Name(), nil
}

// SnakeCasePlural will return the input string
// as a the plural of input, then snake cased.
// SnakeCasePlural is shorthand for using "Plural" and "SnakeCase"
func SnakeCasePlural(input string) string {
	return SnakeCase(Plural(input))
}

// Plural returns the plural of the word
// on a best effort basis
func Plural(input string) string {
	return inflection.Plural(input)
}

// SnakeCase returns the input as snakeCase
func SnakeCase(input string) string {
	return strcase.ToSnake(input)
}

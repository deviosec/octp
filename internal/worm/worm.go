package worm

import (
	"database/sql"
	_ "embed"
	"errors"

	sq "github.com/Masterminds/squirrel"
)

// TODO(eyJhb) we want to add some tests at some point, with SQLite in-memory as the database

// comparetors and selectors
type And = sq.And
type Eq = sq.Eq
type Gt = sq.Gt
type GtOrEq = sq.GtOrEq
type ILike = sq.ILike
type Like = sq.Like
type Lt = sq.Lt
type LtOrEq = sq.LtOrEq
type NotEq = sq.NotEq
type NotILike = sq.NotILike
type NotLike = sq.NotLike
type Or = sq.Or

// Tags available to struct fields
// This is heavily inspired by the JSON package in Golang
// `sql:"column_name,options"` or `sql:",options"` (default column name used).
// each option is seperaed with `,`, `sql:",option1,option2,option3"`.
// some tags migt accept a value, this can be provided using `:`, ie.
// `sql:",mapkey:Name"`
const (
	// which tag key to use, when doing lookups
	FieldTagKey = "sql"

	// tags for managing time + deletion
	// following tags support time.Time and *time.Time.
	// these take no options
	TagCreatedAt = "createdAt" // adds a timestamp on creation
	TagUpdatedAT = "updatedAt" // updates timestamp on update (+ initially set on create)
	TagDeletedAt = "deletedAt" // sets a timestamp to indicate deletion, recommended to use *time.Time

	// tags for supporting embedded fields
	// supports the following types of embedding
	// [10]Permissions
	// []Permissions
	// map[T]Permissions
	// TODO(eyJhb): these may change in the future, ie. foreignkey, I am not sure of the definition
	TagEmbedded   = "embedded"   // indicates that this field embeds a struct
	TagMapKey     = "mapkey"     // indicates what key to use for mapping, in the struct being embedded
	TagForeignKey = "foreignkey" // what key to use to create the foreign relationship (if none specified, the parent struct _id will be used, ie. user_id)
	TagTable      = "table"      // which table to fetch the data from, defaults to plural of struct name

	// misc tags
	TagPrimaryKey = "primarykey" // indicates that this field is the primarykey
	TagUUID       = "uuid"       // adds a UUID upon creation
	TagJSON       = "json"       // converts the data to json, before inserted into the database. supports everything json.Marshal does
	TagIgnore     = "ignore"     // ignore this field when using insert, select, update
)

// Errors that might be returned
var (
	ErrInsertWhere     = errors.New("insert does not support where")
	ErrInsertLimit     = errors.New("insert does not support limit")
	ErrSelectDistinct  = errors.New("only select supports distinct")
	ErrNoCondition     = errors.New("no condition specified")
	ErrCondition       = errors.New("condition specified on non-empty model")
	ErrUnsupportedType = errors.New("unsupported type")
	ErrStopScan        = errors.New("returned when the scanning loop should be stopped")
	ErrNotPointer      = errors.New("the model should be provided as a pointer")
	ErrNotStruct       = errors.New("the given model is not of type struct")
	ErrNotTypeUUID     = errors.New("the field is not of type uuid.UUID")
	ErrNotTypeTime     = errors.New("the field is not of type time.Time")
	ErrNoPrimaryField  = errors.New("could not find any field with primarykey for the model")
)

// Thanks to GermainZ from #ranger on Libera
// for finding this beautiful name, Worst ORM.
type WORM struct {
	Statement *Statement
}

// New will return a DB object, where DB should be a
// connected/opened database, and placeholder should be
// the correct one corresponding to the database used
func New(DB *sql.DB, placeholder sq.PlaceholderFormat) (*WORM, error) {
	stmtBuilder := sq.StatementBuilderType{}.
		PlaceholderFormat(placeholder).
		RunWith(DB)

	stmt := &Statement{
		DB:                DB,
		PlaceholderFormat: placeholder,
		Builder:           stmtBuilder,
	}

	// add the callbacks
	if err := stmt.addCallbacks(); err != nil {
		return nil, err
	}

	return &WORM{Statement: stmt}, nil
}

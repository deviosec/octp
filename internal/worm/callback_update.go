package worm

import (
	"context"

	sq "github.com/Masterminds/squirrel"
)

func callbackUpdate(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	// holds the field that contains
	// the primarykey (if any)
	var idField *Field

	// create a setmap
	setmap := make(map[string]interface{})
	for _, field := range stmt.Fields {
		if ShouldIgnoreField(field) {
			continue
		}

		if _, ok := field.Tags[TagPrimaryKey]; ok {
			idField = field
			continue
		}

		if field.Value != nil {
			setmap[field.Column] = field.Value
			continue
		}

		setmap[field.Column] = field.Ptr
	}

	if idField == nil {
		return false, ErrNoPrimaryField
	}

	updateQuery := stmt.Builder.Update(stmt.Table).
		SetMap(setmap).
		Where(sq.Eq{idField.Column: idField.Ptr})

	_, err := updateQuery.ExecContext(ctx)
	if err != nil {
		return false, err
	}

	return false, nil
}

package worm

import (
	"context"
)

// TODO(eyJhb): add suffix/postfix
// which uses the squirrel suffix/postfix
// useful for ie. on duplicate, etc.
type StatementQuery struct {
	Stmt  *Statement
	ctype CallbackType
}

// Begin starts a database transaction, and will return a new WORM
// instance, that has a new underlying statement, which should be used.
// After either a Commit/Rollback the WORM/Statement can no longer be used.
// If already in a transaction, Begin will return ErrAlreadyTransaction.
func (w WORM) Begin(ctx context.Context) (*WORM, error) {
	stmt := w.Statement.GetInstance()
	newStmt, err := stmt.Begin(ctx)
	if err != nil {
		return nil, err
	}

	return &WORM{Statement: newStmt}, nil
}

// Commit will commit the transaction. If called outside transaction
// error ErrNotTransaction will be returned.
func (w WORM) Commit() error {
	return w.Statement.Commit()
}

// Rollback will rollback the transaction, see Commit for more
func (w WORM) Rollback() error {
	return w.Statement.Rollback()
}

// Insert will `insert` the model into the table based on
// the model name (ie. struct name).
func (w *WORM) Insert(model interface{}) StatementQuery {
	stmt := w.Statement.GetInstance()
	stmt.Model = model
	return StatementQuery{Stmt: &stmt, ctype: CallbackInsert}
}

// Select will `select` the model from the database.
// The model can be given as a map, array or slice.
// Map will need to have key type of the structs primarykey type.
func (w *WORM) Select(model interface{}) StatementQuery {
	stmt := w.Statement.GetInstance()
	stmt.Model = model
	return StatementQuery{Stmt: &stmt, ctype: CallbackSelect}
}

// Update will `update` the model, where it will be based on
// the primarykey. It is not possible to use a empty model
// to update multiple objects.
func (w *WORM) Update(model interface{}) StatementQuery {
	stmt := w.Statement.GetInstance()
	stmt.Model = model
	return StatementQuery{Stmt: &stmt, ctype: CallbackUpdate}
}

// Delete starts a Delete statement.
// If a empty model is given, then it needs to be accompanied
// by one or more `Where` conditions.
// If model is not empty, the primarykey will be used as condition
// for deletion.
func (w *WORM) Delete(model interface{}) StatementQuery {
	stmt := w.Statement.GetInstance()
	stmt.Model = model
	return StatementQuery{Stmt: &stmt, ctype: CallbackDelete}
}

// Table specifies the table to query data from
func (s StatementQuery) Table(table string) StatementQuery {
	s.Stmt.Table = table
	return s
}

// Where will append a extra condition to the `where` statement
func (s StatementQuery) Where(pred interface{}) StatementQuery {
	if s.ctype == CallbackInsert {
		s.Stmt.AddErrors(ErrInsertWhere)
	} else {
		s.Stmt.WhereParts = append(s.Stmt.WhereParts, pred)
	}
	return s
}

// Limit will append `limit` to the query, to only get a
// specific amount of results back.
func (s StatementQuery) Limit(limit uint64) StatementQuery {
	if s.ctype == CallbackInsert {
		s.Stmt.AddErrors(ErrInsertLimit)
	} else {
		s.Stmt.Limit = &limit
	}
	return s
}

func (s StatementQuery) Distinct(columns ...string) StatementQuery {
	if s.ctype != CallbackSelect {
		s.Stmt.AddErrors(ErrInsertLimit)
	} else {
		s.Stmt.Distinct = columns
	}
	return s
}

// func (s StatementQuery) Run(ctx context.Context) StatementQuery {
func (s StatementQuery) Run(ctx context.Context) error {
	return s.Stmt.Run(ctx, s.ctype)
}

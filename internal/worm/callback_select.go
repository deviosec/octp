package worm

import (
	"context"
	"database/sql"
	"reflect"

	"github.com/Masterminds/squirrel"
)

func callbackSelect(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	mt := reflect.TypeOf(stmt.Model).Elem()

	// declare a structType that will hold the type of struct we
	// are operating on
	structType := mt

	// singleRow indicates if we just have a struct, no slice/array, etc.
	// and therefore only expect a single element to be returned.
	// if no such element is returned, we will return a error
	singleRow := true

	// switch witch kind we are using, slice, array or just a single struct
	if mt.Kind() == reflect.Slice || mt.Kind() == reflect.Array || mt.Kind() == reflect.Map {
		structType = mt.Elem()
		singleRow = false
	}

	// parse our struct, to get all the columns we are interested in
	// if we are dealing with a single row, then it is a struct and we
	// don't want to parse it once more
	fields := stmt.Fields
	if singleRow {
		var err error
		fields, err = ParseStruct(structType)
		if err != nil {
			return false, err
		}
	}

	var columns []string
	for _, field := range fields {
		if ShouldIgnoreField(field) {
			continue
		}

		columns = append(columns, field.Column)
	}

	var selectQuery squirrel.SelectBuilder
	if len(stmt.Distinct) == 0 {
		selectQuery = stmt.Builder.Select().
			Columns(columns...).
			From(stmt.Table)
	} else {
		selectQuery = stmt.Builder.Select().
			Columns(stmt.Distinct...).
			From(stmt.Table)
		selectQuery = selectQuery.Distinct()

		// ignore all other fields
		for _, field := range stmt.Fields {
			var found bool
			for _, distinctColunm := range stmt.Distinct {
				if distinctColunm == field.Column {
					found = true
					break
				}
			}
			if !found {
				field.Tags[TagIgnore] = ""
			}
		}
	}

	for _, wherePart := range stmt.WhereParts {
		selectQuery = selectQuery.Where(wherePart)
	}

	if stmt.Limit != nil {
		selectQuery = selectQuery.Limit(*stmt.Limit)
	}

	rows, err := selectQuery.QueryContext(ctx)
	if err != nil {
		return false, err
	}
	defer rows.Close()

	var hasRows bool
	for i := 0; rows.Next(); i++ {
		hasRows = true

		if err := scanRows(ctx, stmt, i, rows); err != nil {
			if err != ErrStopScan {
				return false, err
			}
			break
		}
	}

	// we didn't get any rows, and we wanted
	// at least one...
	if singleRow && !hasRows {
		return false, sql.ErrNoRows
	}

	return false, nil
}

func callbackSelectExcludeSoftdeleted(ctx context.Context, stmt *Statement) (bool, error) {
	if stmt.hasErrors() {
		return false, nil
	}

	rt := reflect.TypeOf(stmt.Model).Elem()

	var err error
	fields := stmt.Fields
	if rt.Kind() == reflect.Array || rt.Kind() == reflect.Slice || rt.Kind() == reflect.Map {
		fields, err = ParseStruct(rt.Elem())
		if err != nil {
			return false, err
		}
	}

	for _, field := range fields {
		if _, ok := field.Tags[TagDeletedAt]; !ok {
			continue
		}

		stmt.WhereParts = append(stmt.WhereParts, Eq{field.Column: nil})
	}

	return false, nil
}

func scanRows(ctx context.Context, parentStmt *Statement, i int, row *sql.Rows) error {
	mv := reflect.ValueOf(parentStmt.Model).Elem()

	var currentElement reflect.Value
	if mv.Kind() == reflect.Slice {
		// create new value of the same type as our slice
		tmpValue := reflect.New(mv.Type().Elem())
		// append that to our element in our slice
		mv.Set(reflect.Append(mv, tmpValue.Elem()))
		// set currentElement to a ptr of the last element added
		currentElement = mv.Index(mv.Len() - 1).Addr()
	} else if mv.Kind() == reflect.Array {
		if i > mv.Len()-1 {
			return ErrStopScan
		}

		currentElement = mv.Index(i).Addr()
	} else if mv.Kind() == reflect.Map {
		// if nil map, we initilise it
		if mv.IsNil() {
			mv.Set(reflect.MakeMap(mv.Type()))
		}

		// create new value of the same type as our map
		// this value will be set with index row.ID, after scan
		tmpValue := reflect.New(mv.Type().Elem())
		currentElement = tmpValue
	} else if mv.Kind() == reflect.Struct {
		if i > 0 {
			return ErrStopScan
		}

		currentElement = mv.Addr()
	}

	// get instance and override the model
	stmt := parentStmt.GetInstance()
	stmt.Model = currentElement.Interface()
	stmt.Fields = parentStmt.Fields

	// get struct values - no need to reparse the struct each time
	if err := GetStructValues(currentElement.Elem(), parentStmt.Fields); err != nil {
		return err
	}

	// run callbacks before
	if err := stmt.RunCallbacks(ctx, CallbackRowsBefore); err != nil {
		parentStmt.AddErrors(stmt.error())
		return ErrStopScan
	}

	// fill with ptrs to values
	var valuePtrs []interface{}

	// get primarykey field
	var mapkeyField *Field
	for _, field := range stmt.Fields {
		if ShouldIgnoreField(field) {
			continue
		}

		// map to primary key!
		if _, ok := field.Tags[TagPrimaryKey]; ok {
			mapkeyField = field
		}

		if field.Value != nil {
			valuePtrs = append(valuePtrs, field.Value)
			continue
		}

		valuePtrs = append(valuePtrs, field.Ptr)
	}

	if err := row.Scan(valuePtrs...); err != nil {
		return err
	}

	// run callbacks after
	if err := stmt.RunCallbacks(ctx, CallbackRowsAfter); err != nil {
		parentStmt.AddErrors(stmt.error())
		return ErrStopScan
	}

	// set mapindex here
	if mv.Kind() == reflect.Map {
		if mapkeyField == nil {
			return ErrNoPrimaryField
		}

		mv.SetMapIndex(reflect.ValueOf(mapkeyField.Ptr).Elem(), currentElement.Elem())
	}

	return nil
}

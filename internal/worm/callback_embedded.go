package worm

import (
	"context"
	"errors"
	"reflect"
	"strings"
)

const (
	embeddedDefaultMapKey = "ID"
)

// callbackEmbedded ensures that any embedded struct will be populated.
// callbackEmbedded is the 1st function call of 3, and is responsible for
// finding all the structs that are in the stmt.Model (ie. it can be struct/slice/array/map).
func callbackEmbedded(ctx context.Context, stmt *Statement) (bool, error) {
	// slice for all our elements, ie. in slice, array or map.
	// if it is a struct, then we add it anyways.
	// then embeddedOnStruct can be run for each one.
	var elems []reflect.Value
	mv := reflect.ValueOf(stmt.Model).Elem()

	if mv.Kind() == reflect.Struct {
		// ensure that we actually got a result, and we are not
		// just operating on the zero value
		if !mv.IsZero() {
			elems = append(elems, mv)
		}
	} else if mv.Kind() == reflect.Slice || mv.Kind() == reflect.Array {
		for i := 0; i < mv.Len(); i++ {
			elems = append(elems, mv.Index(i))
		}
	} else if mv.Kind() == reflect.Map {
		for _, key := range mv.MapKeys() {
			// create a new value of the same type, as our map.
			// seed the new tmp value, with the values from our mapIndex.
			// this is done, so that the element in elems, is a ptr
			tmpValue := reflect.New(mv.MapIndex(key).Type()).Elem()
			tmpValue.Set(mv.MapIndex(key))
			elems = append(elems, tmpValue)
		}
	}

	for _, elem := range elems {
		if err := GetStructValues(elem, stmt.Fields); err != nil {
			return true, err
		}

		if err := runEmbeddedOnStruct(ctx, stmt, 0); err != nil {
			return true, err
		}
	}

	return false, nil
}

// runEmbeddedOnStruct is the 2nd step of 3, where it has the responsibility of finding
// the primarykey, and all the fields in the struct that requires a embedded struct.
// it will then run runEmbeddedMapping for each embedded struct, which will do the actual mapping.
func runEmbeddedOnStruct(ctx context.Context, stmt *Statement, i int) error {
	var pkField *Field
	var embeddedFields []*Field
	for _, field := range stmt.Fields {
		if _, ok := field.Tags[TagEmbedded]; ok {
			embeddedFields = append(embeddedFields, field)
			continue
		}

		// we assume there is only one?
		if _, ok := field.Tags[TagPrimaryKey]; ok {
			pkField = field
			continue
		}
	}

	for _, embeddedField := range embeddedFields {
		if err := runEmbeddedMapping(ctx, stmt, pkField, embeddedField); err != nil {
			return err
		}
	}

	return nil
}

// runEmbeddedMapping is the 3rd and final step, which will setup the correct query,
// and will provide the embeddedField field, which will be populated by the select query.
func runEmbeddedMapping(ctx context.Context, stmt *Statement, pkField, embeddedField *Field) error {
	// create a new statement, with our embeddedField Ptr
	// as the model
	newStmt := stmt.GetInstance()
	newStmt.Model = embeddedField.Ptr

	// reflect the type of our embeddedField, which is used
	// to ensure they key of they may, matches the value
	// of the struct we want to use as our key
	mt := reflect.TypeOf(embeddedField.Ptr).Elem()
	if mt.Kind() == reflect.Map {
		// embedValueType is the embedded value type
		// ie. the element we to point to, as our value.
		mapT := mt.Elem()

		// TODO(eyJhb): Default map key, to the key that
		// has the primarykey set on it, in the child
		mapkey := embeddedDefaultMapKey
		if v, ok := embeddedField.Tags[TagMapKey]; ok {
			mapkey = v
		}
		mapkey = strings.ToLower(mapkey)

		mapkeyField, ok := mapT.FieldByNameFunc(func(fieldName string) bool {
			return strings.ToLower(fieldName) == mapkey
		})
		if !ok {
			return errors.New("unable to find field to use as value for map in struct")
		}

		// ensure that the key type and the struct value matches
		if mt.Key().Kind() != mapkeyField.Type.Kind() {
			return errors.New("the map key type does not match the value type in the struct")
		}

		// change the newStmt.model to be a slice of our map values
		// which we will use later, to make our map
		newStmt.Model = reflect.New(reflect.SliceOf(mapT)).Interface()
	}

	// set the correct table, that we will get our data from
	newStmt.Table = embeddedField.Tags[TagTable]
	if newStmt.Table == "" {
		embeddedStructName, err := GetStructName(reflect.TypeOf(embeddedField.Ptr))
		if err != nil {
			return err
		}
		newStmt.Table = SnakeCasePlural(embeddedStructName)
	}

	// get the foreignkey used, if specified, else default to
	// `<structName>_id`
	foreignkey := embeddedField.Tags[TagForeignKey]
	if foreignkey == "" {
		structName, err := GetStructName(reflect.TypeOf(stmt.Model))
		if err != nil {
			return err
		}
		foreignkey = SnakeCase(structName) + "_id"
	}

	// append to our search query
	newStmt.WhereParts = append(newStmt.WhereParts, Eq{foreignkey: pkField.Ptr})

	// run our query
	if err := newStmt.Run(ctx, CallbackSelect); err != nil {
		return err
	}

	// if we have a map, we need to map the slice over to our map
	if mt.Kind() == reflect.Map {
		newMap := reflect.ValueOf(embeddedField.Ptr).Elem()

		// if map is empty, then we initilise it
		if newMap.IsNil() {
			newMap.Set(reflect.MakeMap(newMap.Type()))
		}

		// should always be a slice, as we define that above
		mv := reflect.ValueOf(newStmt.Model).Elem()

		// loop over newStmt.Model -> which is *[]interface{}
		for i := 0; i < mv.Len(); i++ {
			value := mv.Index(i)

			// TODO(eyJhb): do this at the top
			mapkey := "ID"
			if v, ok := embeddedField.Tags[TagMapKey]; ok {
				mapkey = v
			}
			mapkey = strings.ToLower(mapkey)

			key := value.FieldByNameFunc(func(fieldName string) bool {
				return strings.ToLower(fieldName) == mapkey
			})

			if key.IsZero() {
				return errors.New("unable to find field to use as key for map in struct")
			}

			newMap.SetMapIndex(key, value)
		}

		// set the new map to the one we created
		newStmt.Model = newMap.Addr().Interface()

		parentmv := reflect.ValueOf(stmt.Model).Elem()
		// if the current element is a struct, and our parent model is a map, then we should
		// run the below
		if mv.Type().Elem().Kind() == reflect.Struct && parentmv.Kind() == reflect.Map {
			// get the current value, of the map[any]struct we are working on
			v := parentmv.MapIndex(reflect.ValueOf(pkField.Ptr).Elem())

			// create a new struct, which is addressable
			newStruct := reflect.New(v.Type())
			newStruct.Elem().Set(v)

			// get the field we want to change (the embedded field in the struct)
			f := newStruct.Elem().FieldByName(embeddedField.Name)
			// set it
			f.Set(newMap)

			// finally, update the index in our parent map
			parentmv.SetMapIndex(reflect.ValueOf(pkField.Ptr).Elem(), newStruct.Elem())
		}
	}

	return nil
}

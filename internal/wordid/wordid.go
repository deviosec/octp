package wordid

import (
	"math/rand"
	"strings"
)

type Options struct {
	Length         uint16
	LowercaseWords bool
	Words          []string
}

const (
	DefaultLength = 4
)

func GenerateSlice(opts Options) []string {
	if opts.Length == 0 {
		opts.Length = DefaultLength
	}

	if len(opts.Words) == 0 {
		opts.Words = getWords()
	}

	var generatedWords []string
	for i := 0; i < int(opts.Length); i++ {
		w := opts.Words[rand.Intn(len(opts.Words))]
		if opts.LowercaseWords {
			w = strings.ToLower(w)
		}

		generatedWords = append(generatedWords, w)
	}

	return generatedWords
}

func Generate(n uint16) string {
	opts := Options{
		Length: n,
	}

	return strings.Join(GenerateSlice(opts), "")
}
